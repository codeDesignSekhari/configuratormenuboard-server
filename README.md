# API IMENUS
Solution de commande et de réservation en ligne pour restaurant.

Symfony API Imenus 10 Mars 2021.

# refacto SERVICE folder and MANAGER
The service folder will be used for the business code 
and the manager folder for persistence

# Install development environment

* Create a MySQL database
```sh
php bin/console doctrine:database:create
```
* Clone from project repository from GitLab
```sh
git clone git@gitlab.com:codeDesignSekhari/configuratormenuboard-server.git
cd configuratormenuboard-server
git checkout master
```

* Install symfony bundles using composer.

```sh
$ composer install
```

# (Re)-initialize application

The *--dev* argument generates sample data fixtures

```sh
$ php bin/console doctrine:fixtures:load
```

# Run PHP Server

If you do not specify <HOST> and <PORT>, serveur will run on 127.0.0.1:8080

```sh
$ php bin/console server:run [<HOST>:<PORT>]
```

You can also start your server as a background task (unavailable on Windows...)

```sh
$ php bin/console server:start [<HOST>:<PORT>]
```

Then you can stop it

```sh
$ php bin/console server:stop [<HOST>:<PORT>]
```
## Clear cache
```sh
$ php bin/console cache:clear --env={prod|dev}
```

## JWT CREATE Key public/private LexikJWTAuthentificationBundle
```sh
$ mkdir -p config/jwt
$ openssl genrsa -out config/jwt/private.pem -aes256 4096
$ openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
```

Add in lexik_jwt_authentication.yaml :
```sh
secret_key: '%env(resolve:JWT_SECRET_KEY)%'
public_key: '%env(resolve:JWT_PUBLIC_KEY)%'
pass_phrase: '%env(JWT_PASSPHRASE)%'
```

## Run PHPUnit Tests suite

In order to check the project properly run on your system, and before committing
changes, you should run tests and apply corrections if some of them fail.

* Download [PHPUnit](https://phpunit.de/) executable

```sh
# run the whole tests suite
phpunit
# run only one test controller
phpunit tests/MyBundle/Controller/MyControllerTest.php
```






