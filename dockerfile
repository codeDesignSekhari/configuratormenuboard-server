###> recipes ###
###< recipes ###

# Start with the official PHP 8.2 image
FROM php:8.2-fpm

# Install system dependencies and clean cache
RUN apt-get update && apt-get install -y \
 git \
 wget \
 apt-transport-https \
 curl \
 vim \
 libpng-dev unzip libicu-dev libpq-dev libmariadb-dev \
 libonig-dev \
 libxml2-dev \
 libzip-dev \
 libbz2-dev \
 libgmp-dev \
 zlib1g-dev \
 zip \
 unzip

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql intl mysqli mbstring exif pcntl bcmath gettext gd
RUN docker-php-ext-install soap sockets opcache calendar pdo

RUN pecl install apcu && docker-php-ext-enable apcu

# Copy Composer from the official Composer image
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Set working directory
WORKDIR /var/www

# Copier les fichiers de configuration et les dépendances de l'application
COPY composer.json .
COPY composer.lock .

# Copy existing application directory permissions
COPY --chown=www-data:www-data . /var/www
#COPY --chown=www-data:www-data . .git

USER www-data

# Symfony CLI
#RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony

# Configurer l'accès à la base de données MariaDB
ENV DATABASE_URL=mysql://root:@mariadb:3306/imenus


# Installer les dépendances avec Composer
#RUN composer install