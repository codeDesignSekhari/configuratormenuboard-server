<?php

[
    "boisson" => [
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Orangina",
            "price" => 1.50,
            "description" => null,
        ],
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Fanta orange",
            "price" => 1.50,
            "description" => null,
        ],
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Fanta citron",
            "price" => 1.50,
            "description" => null,
        ],
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Coca cola",
            "price" => 1.50,
            "description" => null,
        ],
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Oasis tropical",
            "price" => 1.50,
            "description" => null,
        ],
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Pepsi Max 50cl",
            "price" => 1.50,
            "description" => null,
        ],
    ],
    "dessert" => [
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Tiramisu",
            "price" => 2.50,
            "description" => null,
        ],
        [
            'fileName' => "orangina",
            'typeMedia' => "picture",
            "name" => "Cocktail de fruit",
            "price" => 2.50,
            "description" => null,
        ],
    ],
];