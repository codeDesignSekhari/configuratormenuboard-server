<?php

[
    "minimum_order" => [
        [
            'id' => 1,
            'name' => "Pas de préférence",
        ],
        [
            'id' => 2,
            'name' => "10.00",
        ],
        [
            'id' => 3,
            'name' => "15.00",
        ],
    ],
    "delivery_fee" => [
        [
            'id' => 1,
            'name' => "Pas de préférence",
        ],
        [
            'id' => 2,
            'name' => 'Gratuit',
        ],
        [
            'id' => 3,
            'name' => "1.00",
        ],
        [
            'id' => 4,
            'name' => "2.50",
        ],
    ],
    "offer_promote" => [
        [
            'id' => 1,
            'name' => "réduction",
        ],
        [
            'id' => 2,
            'name' => 'carte de fidelité',
        ],
    ],
];