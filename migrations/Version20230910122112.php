<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230910122112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE delivery_shop ADD store_id INT DEFAULT NULL, ADD delivery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery_shop ADD CONSTRAINT FK_7DF68438B092A811 FOREIGN KEY (store_id) REFERENCES store (id)');
        $this->addSql('ALTER TABLE delivery_shop ADD CONSTRAINT FK_7DF6843812136921 FOREIGN KEY (delivery_id) REFERENCES delivery (id)');
        $this->addSql('CREATE INDEX IDX_7DF68438B092A811 ON delivery_shop (store_id)');
        $this->addSql('CREATE INDEX IDX_7DF6843812136921 ON delivery_shop (delivery_id)');
        $this->addSql('ALTER TABLE store DROP FOREIGN KEY FK_FF57587747087B03');
        $this->addSql('DROP INDEX IDX_FF57587747087B03 ON store');
        $this->addSql('ALTER TABLE store DROP delivery_shop_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE delivery_shop DROP FOREIGN KEY FK_7DF68438B092A811');
        $this->addSql('ALTER TABLE delivery_shop DROP FOREIGN KEY FK_7DF6843812136921');
        $this->addSql('DROP INDEX IDX_7DF68438B092A811 ON delivery_shop');
        $this->addSql('DROP INDEX IDX_7DF6843812136921 ON delivery_shop');
        $this->addSql('ALTER TABLE delivery_shop DROP store_id, DROP delivery_id');
        $this->addSql('ALTER TABLE store ADD delivery_shop_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD CONSTRAINT FK_FF57587747087B03 FOREIGN KEY (delivery_shop_id) REFERENCES delivery_shop (id)');
        $this->addSql('CREATE INDEX IDX_FF57587747087B03 ON store (delivery_shop_id)');
    }
}
