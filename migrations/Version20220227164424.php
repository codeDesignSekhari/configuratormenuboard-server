<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220227164424 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE day_week CHANGE monday_open monday_open TIME DEFAULT NULL, CHANGE create_at create_at DATETIME DEFAULT NULL, CHANGE monday_close monday_close TIME DEFAULT NULL, CHANGE tuesday_open tuesday_open TIME DEFAULT NULL, CHANGE tuesday_close tuesday_close TIME DEFAULT NULL, CHANGE wednesday_open wednesday_open TIME DEFAULT NULL, CHANGE wednesday_close wednesday_close TIME DEFAULT NULL, CHANGE thursday_open thursday_open TIME DEFAULT NULL, CHANGE thursday_close thursday_close TIME DEFAULT NULL, CHANGE friday_open friday_open TIME DEFAULT NULL, CHANGE friday_close friday_close TIME DEFAULT NULL, CHANGE saturday_open saturday_open TIME DEFAULT NULL, CHANGE saturday_close saturday_close TIME DEFAULT NULL, CHANGE sunday_open sunday_open TIME DEFAULT NULL, CHANGE sunday_close sunday_close TIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE day_week CHANGE monday_open monday_open TIME NOT NULL, CHANGE create_at create_at DATETIME NOT NULL, CHANGE monday_close monday_close TIME NOT NULL, CHANGE tuesday_open tuesday_open TIME NOT NULL, CHANGE tuesday_close tuesday_close TIME NOT NULL, CHANGE wednesday_open wednesday_open TIME NOT NULL, CHANGE wednesday_close wednesday_close TIME NOT NULL, CHANGE thursday_open thursday_open TIME NOT NULL, CHANGE thursday_close thursday_close TIME NOT NULL, CHANGE friday_open friday_open TIME NOT NULL, CHANGE friday_close friday_close TIME NOT NULL, CHANGE saturday_open saturday_open TIME NOT NULL, CHANGE saturday_close saturday_close TIME NOT NULL, CHANGE sunday_open sunday_open TIME NOT NULL, CHANGE sunday_close sunday_close TIME NOT NULL');
    }
}
