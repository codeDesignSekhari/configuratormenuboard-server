<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231122193556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE user SET roles= \'[\"ROLE_USER\",\"ROLE_FRONT\",\"ROLE_BACK\"]\'');
        $this->addSql('UPDATE user SET roles= \'[\"ROLE_USER\",\"ROLE_FRONT\",\"ROLE_BACK\",\"ROLE_ADMIN\"]\' WHERE user.id = 1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE user SET roles= \'[\"ROLE_USER\",\"ROLE_FRONT\",\"ROLE_BACK\"]\'');
        $this->addSql('UPDATE user SET roles= \'[\"ROLE_USER\",\"ROLE_FRONT\",\"ROLE_BACK\",\"ROLE_ADMIN\"]\' WHERE user.id = 1');
    }
}
