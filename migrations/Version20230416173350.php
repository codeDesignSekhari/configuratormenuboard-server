<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230416173350 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //$this->addSql('CREATE TABLE order_line_complement (order_line_id INT NOT NULL, complement_id INT NOT NULL, INDEX IDX_8D16CE83BB01DC09 (order_line_id), INDEX IDX_8D16CE8340D9D0AA (complement_id), PRIMARY KEY(order_line_id, complement_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        //$this->addSql('ALTER TABLE order_line_complement ADD CONSTRAINT FK_8D16CE83BB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id) ON DELETE CASCADE');
        //$this->addSql('ALTER TABLE order_line_complement ADD CONSTRAINT FK_8D16CE8340D9D0AA FOREIGN KEY (complement_id) REFERENCES complement (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_line_complement DROP FOREIGN KEY FK_8D16CE83BB01DC09');
        $this->addSql('ALTER TABLE order_line_complement DROP FOREIGN KEY FK_8D16CE8340D9D0AA');
        $this->addSql('DROP TABLE order_line_complement');
    }
}
