<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220319215659 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE seo (id INT AUTO_INCREMENT NOT NULL, slug_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, meta_description VARCHAR(255) NOT NULL, meta_key_words VARCHAR(255) NOT NULL, meta_robots VARCHAR(255) NOT NULL, ld_json JSON DEFAULT NULL, UNIQUE INDEX UNIQ_6C71EC30311966CE (slug_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE seo ADD CONSTRAINT FK_6C71EC30311966CE FOREIGN KEY (slug_id) REFERENCES slug (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE seo');
    }
}
