<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230218193202 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE complement (id INT AUTO_INCREMENT NOT NULL, complement_group_id INT DEFAULT NULL, menu_configurator_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_F8A41E34805D42E9 (complement_group_id), INDEX IDX_F8A41E34E37C1D6C (menu_configurator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE complement_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, min INT DEFAULT NULL, max INT DEFAULT NULL, is_multiple TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE complement ADD CONSTRAINT FK_F8A41E34805D42E9 FOREIGN KEY (complement_group_id) REFERENCES complement_group (id)');
        $this->addSql('ALTER TABLE complement ADD CONSTRAINT FK_F8A41E34E37C1D6C FOREIGN KEY (menu_configurator_id) REFERENCES menu_configurator (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complement DROP FOREIGN KEY FK_F8A41E34805D42E9');
        $this->addSql('ALTER TABLE complement DROP FOREIGN KEY FK_F8A41E34E37C1D6C');
        $this->addSql('DROP TABLE complement');
        $this->addSql('DROP TABLE complement_group');
    }
}
