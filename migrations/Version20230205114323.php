<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230205114323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE order_line (id INT AUTO_INCREMENT NOT NULL, order_product_id INT DEFAULT NULL, media_manager_id INT DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, quantity INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, order_fields JSON DEFAULT NULL, INDEX IDX_9CE58EE1F65E9B0F (order_product_id), UNIQUE INDEX UNIQ_9CE58EE1721BFAF7 (media_manager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE1F65E9B0F FOREIGN KEY (order_product_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE1721BFAF7 FOREIGN KEY (media_manager_id) REFERENCES media_manager (id)');
        $this->addSql('ALTER TABLE `order` ADD cell_phone VARCHAR(255) NOT NULL, DROP price, DROP quantity, CHANGE name email VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE1F65E9B0F');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE1721BFAF7');
        $this->addSql('DROP TABLE order_line');
        $this->addSql('ALTER TABLE `order` ADD price DOUBLE PRECISION NOT NULL, ADD quantity INT NOT NULL, ADD name VARCHAR(255) NOT NULL, DROP email, DROP cell_phone');
    }
}
