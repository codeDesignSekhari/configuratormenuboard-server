<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231113235006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE config_shop ADD color_base VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_configurator ADD style_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_configurator ADD CONSTRAINT FK_AD24F695BACD6074 FOREIGN KEY (style_id) REFERENCES menu_configurator_style (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AD24F695BACD6074 ON menu_configurator (style_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE config_shop DROP color_base');
        $this->addSql('ALTER TABLE menu_configurator DROP FOREIGN KEY FK_AD24F695BACD6074');
        $this->addSql('DROP INDEX UNIQ_AD24F695BACD6074 ON menu_configurator');
        $this->addSql('ALTER TABLE menu_configurator DROP style_id');
    }
}
