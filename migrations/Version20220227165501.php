<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220227165501 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE config_shop DROP FOREIGN KEY FK_3CCA105230AA4BAD');
        $this->addSql('DROP INDEX UNIQ_3CCA105230AA4BAD ON config_shop');
        $this->addSql('ALTER TABLE config_shop DROP day_week_id');
        $this->addSql('ALTER TABLE day_week ADD config_shop_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE day_week ADD CONSTRAINT FK_F03284B6380A062D FOREIGN KEY (config_shop_id) REFERENCES config_shop (id)');
        $this->addSql('CREATE INDEX IDX_F03284B6380A062D ON day_week (config_shop_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE config_shop ADD day_week_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE config_shop ADD CONSTRAINT FK_3CCA105230AA4BAD FOREIGN KEY (day_week_id) REFERENCES day_week (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3CCA105230AA4BAD ON config_shop (day_week_id)');
        $this->addSql('ALTER TABLE day_week DROP FOREIGN KEY FK_F03284B6380A062D');
        $this->addSql('DROP INDEX IDX_F03284B6380A062D ON day_week');
        $this->addSql('ALTER TABLE day_week DROP config_shop_id');
    }
}
