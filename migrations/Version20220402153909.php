<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220402153909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE menu_title_store');
        $this->addSql('ALTER TABLE menu_title ADD store_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_title ADD CONSTRAINT FK_BA9A5B0CB092A811 FOREIGN KEY (store_id) REFERENCES store (id)');
        $this->addSql('CREATE INDEX IDX_BA9A5B0CB092A811 ON menu_title (store_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE menu_title_store (menu_title_id INT NOT NULL, store_id INT NOT NULL, INDEX IDX_575AF04C26CEC22B (menu_title_id), INDEX IDX_575AF04CB092A811 (store_id), PRIMARY KEY(menu_title_id, store_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE menu_title_store ADD CONSTRAINT FK_575AF04C26CEC22B FOREIGN KEY (menu_title_id) REFERENCES menu_title (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_title_store ADD CONSTRAINT FK_575AF04CB092A811 FOREIGN KEY (store_id) REFERENCES store (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_title DROP FOREIGN KEY FK_BA9A5B0CB092A811');
        $this->addSql('DROP INDEX IDX_BA9A5B0CB092A811 ON menu_title');
        $this->addSql('ALTER TABLE menu_title DROP store_id');
    }
}
