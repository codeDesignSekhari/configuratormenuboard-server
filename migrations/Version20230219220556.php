<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230219220556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complement DROP FOREIGN KEY FK_F8A41E34E37C1D6C');
        $this->addSql('DROP INDEX IDX_F8A41E34E37C1D6C ON complement');
        $this->addSql('ALTER TABLE complement DROP menu_configurator_id');
        $this->addSql('ALTER TABLE complement_group ADD menu_configurator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE complement_group ADD CONSTRAINT FK_DE1D683BE37C1D6C FOREIGN KEY (menu_configurator_id) REFERENCES menu_configurator (id)');
        $this->addSql('CREATE INDEX IDX_DE1D683BE37C1D6C ON complement_group (menu_configurator_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complement ADD menu_configurator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE complement ADD CONSTRAINT FK_F8A41E34E37C1D6C FOREIGN KEY (menu_configurator_id) REFERENCES menu_configurator (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_F8A41E34E37C1D6C ON complement (menu_configurator_id)');
        $this->addSql('ALTER TABLE complement_group DROP FOREIGN KEY FK_DE1D683BE37C1D6C');
        $this->addSql('DROP INDEX IDX_DE1D683BE37C1D6C ON complement_group');
        $this->addSql('ALTER TABLE complement_group DROP menu_configurator_id');
    }
}
