<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230116234612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE banner (id INT AUTO_INCREMENT NOT NULL, store_id INT DEFAULT NULL, update_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', end_time TIME DEFAULT NULL, start_time TIME DEFAULT NULL, UNIQUE INDEX UNIQ_6F9DB8E7B092A811 (store_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE banner ADD CONSTRAINT FK_6F9DB8E7B092A811 FOREIGN KEY (store_id) REFERENCES store (id)');
        $this->addSql('ALTER TABLE media_manager ADD banner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media_manager ADD CONSTRAINT FK_9BF3A3F0684EC833 FOREIGN KEY (banner_id) REFERENCES banner (id)');
        $this->addSql('CREATE INDEX IDX_9BF3A3F0684EC833 ON media_manager (banner_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media_manager DROP FOREIGN KEY FK_9BF3A3F0684EC833');
        $this->addSql('ALTER TABLE banner DROP FOREIGN KEY FK_6F9DB8E7B092A811');
        $this->addSql('DROP TABLE banner');
        $this->addSql('DROP INDEX IDX_9BF3A3F0684EC833 ON media_manager');
        $this->addSql('ALTER TABLE media_manager DROP banner_id');
    }
}
