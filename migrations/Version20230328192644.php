<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230328192644 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE banner ADD picture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE banner ADD CONSTRAINT FK_6F9DB8E7EE45BDBF FOREIGN KEY (picture_id) REFERENCES media_manager (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F9DB8E7EE45BDBF ON banner (picture_id)');
        $this->addSql('ALTER TABLE media_manager DROP FOREIGN KEY FK_9BF3A3F0AA60395A');
        $this->addSql('ALTER TABLE media_manager DROP FOREIGN KEY FK_9BF3A3F0684EC833');
        $this->addSql('DROP INDEX IDX_9BF3A3F0AA60395A ON media_manager');
        $this->addSql('DROP INDEX IDX_9BF3A3F0684EC833 ON media_manager');
        $this->addSql('ALTER TABLE media_manager DROP ordered_id, DROP banner_id');
        $this->addSql('ALTER TABLE seo CHANGE ld_json ld_json LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE banner DROP FOREIGN KEY FK_6F9DB8E7EE45BDBF');
        $this->addSql('DROP INDEX UNIQ_6F9DB8E7EE45BDBF ON banner');
        $this->addSql('ALTER TABLE banner DROP picture_id');
        $this->addSql('ALTER TABLE media_manager ADD ordered_id INT DEFAULT NULL, ADD banner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media_manager ADD CONSTRAINT FK_9BF3A3F0AA60395A FOREIGN KEY (ordered_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE media_manager ADD CONSTRAINT FK_9BF3A3F0684EC833 FOREIGN KEY (banner_id) REFERENCES banner (id)');
        $this->addSql('CREATE INDEX IDX_9BF3A3F0AA60395A ON media_manager (ordered_id)');
        $this->addSql('CREATE INDEX IDX_9BF3A3F0684EC833 ON media_manager (banner_id)');
        $this->addSql('ALTER TABLE seo CHANGE ld_json ld_json LONGTEXT DEFAULT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE `utf8mb4_bin`');
    }
}
