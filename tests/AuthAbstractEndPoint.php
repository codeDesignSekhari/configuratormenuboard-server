<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AuthAbstractEndPoint extends WebTestCase
{
    protected const EMAIL = 'demo-pizza@imenus.fr';
    protected const PASSWORD = '123456789';

    protected array $serverInformation = ['ACCEPT' => 'application/json', 'CONTENT_TYPE' => 'application/json'];
    protected string $tokenNotFound = 'JWT Token not found';
    protected string $notYourResource = "It's not your resource";
    protected string $loginPayload = '{"username": "%s", "password": "%s"}';

    private EntityManagerInterface $entityManager;


    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param string $methods
     * @param string $uri
     * @param string $payload
     * @param array $parameter
     * @param bool $withAuthentication
     * @return Response
     */
    public function getResponseFromRequest(
        string $methods,
        string $uri,
        string $payload = '',
        array  $parameter = [],
        bool   $withAuthentication = true
    ): Response
    {
        $this->createAuthenticationClient($withAuthentication);
        $this->client->request($methods,
            $uri,
            [],
            [],
            $this->serverInformation,
            $payload
        );

        return $this->client->getResponse();
    }

    /**
     * @param bool $withAuthentication
     * @return KernelBrowser
     */
    protected function createAuthenticationClient(bool $withAuthentication): KernelBrowser
    {
        if (!$withAuthentication) {
            return $this->client;
        }

        $this->client->request(
            Request::METHOD_POST,
            '/v1/api/login_check',
            [], [],
            $this->serverInformation,
            sprintf($this->loginPayload, self::EMAIL, self::PASSWORD)
        );

        $data = json_decode($this->client->getResponse()->getContent(), true);

        $this->client
            ->setServerParameter(
                'HTTP_Authorization',
                sprintf('Bearer %s', $data['token'])
            );

        return $this->client;
    }

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        parent::setUp();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        $this->entityManager->close();
        $this->entityManager = null;
        parent::tearDown();
    }
}