<?php

namespace App\Tests;

use App\Entity\MenuTitle;
use App\Entity\Slug;
use App\Entity\Store;
use App\Entity\User;
use App\Service\Constant\MessageResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class MenuConfiguratorTest extends AuthAbstractEndPoint
{
    protected const SLUG_URL = "demo-pizza";
    protected const CATEGORY_PARAMS = "pizza";
    protected const SLUG_PARAMS = "/demo-pizza";
    protected const CITY_PARAMS = "lyon";
    protected const PREFIX_API = "/v1/api/";
    protected const SLUG_RESTAURANT = "restaurants/";
    protected const CATEGORY_NAME_VALUE = "Test unitaire PHP";
    protected const CATEGORY_NAME_VALUE_UPDATE = "Test unitaire PHP UPDATE";


    public function testFetchDataUser()
    {
        $user = $this
            ->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy([
                'email' => AuthAbstractEndPoint::EMAIL
            ]);

        $response = $this->getResponseFromRequest(
            Request::METHOD_GET,
            self::PREFIX_API . 'back/user/' . $user->getId(),
            '',
            [],
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_decode($responseContent, true);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertJson($responseContent);
        self::assertNotEmpty($responseDecoded);

        return $responseDecoded['restaurant']['id'];
    }

//    /**
//     * @return void
//     */
//    public function testUploadImage(): void
//    {
//        $entityManager = $this->getEntityManager();
//
//        $slug = $entityManager->getRepository(Slug::class)->findOneBy(["name" => self::SLUG_URL]);
//        $store = $entityManager->getRepository(Store::class)->findOneBy(["slug" => $slug]);
//
//        $this->getParameter('images_menu_configurator_directory'),
//        $upload = new UploadedFile(
//            dirname(__DIR__, 2) . '\configuratormenuboard-server\src\public\images\menus\orangina.png',
//            'orangina.png',
//            'image/png',
//        );
//        $payload = [
//            "context" => 'PICTURE',
//            "uuid" => $store->getUid(),
//            "upload" => $upload
//        ];
//
//        $response = $this->getResponseFromRequest(
//            Request::METHOD_POST,
//            self::PREFIX_API . 'back/media-manager/upload',
//            json_encode($payload),
//            [],
//        );
//
//        $responseContent = $response->getContent();
//        $responseDecoded = json_encode($responseContent);
//
//        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
//        self::assertJson($responseContent);
//        self::assertNotEmpty($responseDecoded);
//    }


    /**
     * CREATE CATEGORY
     * /back/menu-title/create
     * @depends testFetchDataUser
     */
    public function testCreateMenuCategory(int $storeId)
    {

        $payload = [
            "id" => $storeId,
            "name" => self::CATEGORY_NAME_VALUE
        ];

        $response = $this->getResponseFromRequest(
            Request::METHOD_POST,
            self::PREFIX_API . 'back/menu-title/create',
            json_encode($payload),
            [],
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_decode($responseContent, true);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertStringContainsString($responseDecoded, MessageResponse::CREATE_MESSAGE);
//        self::assertJson($responseContent);
//        self::assertNotEmpty($responseDecoded);
    }

    /**
     * CREATE CATEGORY
     * /back/menu-title/create
     * @depends testFetchDataUser
     */
    public function testUpdateMenuCategory(int $storeId)
    {
        $entityManager = $this->getEntityManager();
        $categoryMenu = $entityManager->getRepository(MenuTitle::class)->findOneBy(["name" => self::CATEGORY_NAME_VALUE]);

        $payload = [
            "id" => $storeId,
            "menuTitleId" => $categoryMenu->getid(),
            "value" => self::CATEGORY_NAME_VALUE_UPDATE
        ];

        $response = $this->getResponseFromRequest(
            Request::METHOD_PATCH,
            self::PREFIX_API . 'back/menu-title',
            json_encode($payload),
            [],
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_decode($responseContent, true);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertStringContainsString($responseDecoded, MessageResponse::CREATE_MESSAGE);
//        self::assertJson($responseContent);
//        self::assertNotEmpty($responseDecoded);
    }


//    /**
//     * CREATE ONE MENU
//     * /back/menu-configurator
//     * @param int $storeId
//     * @return void
//     * @depends testFetchDataUser
//     * @depends testUploadImage
//     */
//    public function testCreateMenuConfigurator(int $storeId): void
//    {
//        $payload =
//            [
//                'form' => [
//                    'id' => 83,
//                    "picture" => [
//                        'id' => 160,
//                        'fileName' => "burger-chickent-61e187dc17115.png",
//                        'size' => 435543
//                    ],
//                    'allergen' => "Test allergènes",
//                    'category' => [
//                        'id' => $categoryMenuId,
//                        'name' => $category->getName()
//                    ],
//                    "description" => "Pain maxi, steak grill, bacon fumé, raclette,\nsauce barbecue, fromage fondu et salade.",
//                    "itemMenus" => [],
//                    "name" => "TEST BURGER CHICKEN",
//                    "price" => 8.99,
//                    "priceMenu" => 11.99,
//                    "subTitle" => "FRESH",
//                ],
//                "idMenuConfig" => 83,
//                "mediaManager" => 160,
//                "storeId" => $storeId
//            ];
//
//        $response = $this->getResponseFromRequest(
//            Request::METHOD_POST,
//            self::PREFIX_API . 'back/menu-configurator',
//            json_encode($payload),
//            [],
//        );
//
//        $responseContent = $response->getContent();
//        $responseDecoded = json_encode($responseContent);
//
//        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
//        self::assertJson($responseContent);
//        self::assertNotEmpty($responseDecoded);
//    }
//
//    /**
//     * @return void
//     */
//    public function testUpdateBackMenuConfigurator() : void
//    {
//        $entityManager = $this->getEntityManager();
//
//        $slug = $entityManager->getRepository(Slug::class)->findOneBy(["name" => $this->slugUrl]);
//        $store = $entityManager->getRepository(Store::class)->findOneBy(["slug" => $slug]);
//
//        $response = $this->getResponseFromRequest(
//            Request::METHOD_POST,
//            $this->prefixApi . 'back/user/' . $store->getId() . '/menus-configurators',
//            '',
//            [],
//        );
//
//        $responseContent = $response->getContent();
//        $responseDecoded = json_encode($responseContent);
//
//        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
//        self::assertJson($responseContent);
//        self::assertNotEmpty($responseDecoded);
//    }

    /**
     * @return void
     */
    public function testGetBackMenuConfigurators(): void
    {
        $entityManager = $this->getEntityManager();

        $slug = $entityManager->getRepository(Slug::class)->findOneBy(["name" => self::SLUG_URL]);
        $store = $entityManager->getRepository(Store::class)->findOneBy(["slug" => $slug]);

        $response = $this->getResponseFromRequest(
            Request::METHOD_GET,
            self::PREFIX_API . 'back/user/' . $store->getId() . '/menus-configurators',
            '',
            [],
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_encode($responseContent);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertJson($responseContent);
        self::assertNotEmpty($responseDecoded);
    }


    /**
     * ex : /lyon/pizza/demo-pizza
     * DISPLAY MENU FRONT CLIENT
     * @return void
     */
    public function testFetchMenuConfiguratorFront(): void
    {
        $response = $this->getResponseFromRequest(
            Request::METHOD_GET,
            self::PREFIX_API . self::SLUG_RESTAURANT . self::CITY_PARAMS . '/' . self::CATEGORY_PARAMS . self::SLUG_PARAMS,
            '',
            [],
            false
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_encode($responseContent);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertJson($responseContent);
        self::assertNotEmpty($responseDecoded);
    }


    /**
     * ex : /menu-title/49/delete
     * REMOVE CATEGORY MENU
     * @return void
     */
    public function testDeleteMenuCategory(): void
    {
        $entityManager = $this->getEntityManager();
        $categoryMenu = $entityManager
            ->getRepository(MenuTitle::class)
            ->findOneBy([
                "name" => self::CATEGORY_NAME_VALUE_UPDATE
            ]);

        $response = $this->getResponseFromRequest(
            Request::METHOD_DELETE,
            self::PREFIX_API . "menu-title/" . $categoryMenu->getId() . "/delete",
            '',
            [],
            false
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_encode($responseContent);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
//        self::assertStringContainsString($responseDecoded, self::MESSAGE_SUCCESS_REMOVE);
    }
}