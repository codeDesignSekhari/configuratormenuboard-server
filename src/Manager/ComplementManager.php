<?php

namespace App\Manager;

use App\Entity\Complement;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ComplementManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, Complement::class);
    }
}