<?php

namespace App\Manager;

use App\Entity\Order;
use App\Entity\OrderLine;
use App\Event\OrderEvent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;

class OrderLineManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface   $entityManager,
        protected LoggerInterface          $logger,
        protected MediaManager             $mediaManager,
        protected ComplementManager        $complementManager,
        protected EventDispatcherInterface $eventDispatcher,
    )
    {
        parent::__construct($entityManager, $logger, OrderLine::class);
    }


    public function generateOrderLine(array $data, Order $order, bool $flush = true): void
    {
        foreach ($data['orders'] as $line) {
            $picture = $this->mediaManager
                ->repo
                ->find($line['picture']['id']);

            $orderLine = (new OrderLine())
                ->setPrice($line['price'])
                ->setName($line['name'])
                ->setQuantity($line['quantity'])
                ->setMediaManager($picture)
                ->setDescription($line['description'])
                ->setOrder($order)
                ->setName($line['name']);

            foreach ($line['complements'] as $complementLine) {
                $complement = $this->complementManager->repo
                    ->find($complementLine['id']);

                if ($complement) {
                    $orderLine->addComplement($complement);
                }
            }

            $order->addOrderLine($orderLine);
        }

        $this->update($order);

        if ($flush) {
            $this->eventDispatcher->dispatch(
                new OrderEvent([$order]),
                OrderEvent::ON_ORDER
            );
        }

    }
}