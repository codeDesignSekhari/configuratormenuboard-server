<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;

abstract class AbstractManager
{
    protected EntityRepository $repo;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected LoggerInterface        $logger,
        protected                        $class
    )
    {
        $this->repo = $entityManager->getRepository($class);
    }

    public function update(
        object $entity,
        bool   $flush = true,
        bool   $enableEvents = true
    ): void
    {
        if ($enableEvents) {
            $this->prePersist($entity);
        }

        $this->entityManager->persist($entity);

        if ($flush) {
            $this->entityManager->flush();
        }

        if ($enableEvents) {
            $this->postFlush($entity);
        }
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }


    public function delete(
        object $entity,
        bool   $flush = true,
        bool   $enableEvents = true
    ): void
    {
        if ($enableEvents) {
            $this->preDelete($entity);
        }

        $this->entityManager->remove($entity);

        if ($flush) {
            $this->entityManager->flush();
        }

        if ($enableEvents) {
            $this->postDelete($entity);
        }
    }

    private function prePersist(object $entity): void
    {
    }

    private function preDelete(object $entity): void
    {
    }

    private function postDelete(object $entity): void
    {
    }

    private function postFlush(object $entity): void
    {
    }

    public function repository(): EntityRepository
    {
        return $this->repo;
    }
}