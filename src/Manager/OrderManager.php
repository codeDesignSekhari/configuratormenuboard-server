<?php

namespace App\Manager;

use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\Store;
use App\Enums\OrderStatus;
use App\Event\OrderEvent;
use App\Exception\StoreNotFoundException;
use App\Service\helperService\helperService;
use App\Service\StripeService\StripeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;

class OrderManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface   $entityManager,
        protected OrderLineManager         $orderLineManager,
        protected CartManager              $cartManager,
        protected StripeService            $stripeService,
        protected EventDispatcherInterface $eventDispatcher,
        protected HelperService            $helperService,
        protected LoggerInterface          $logger,
    )
    {
        parent::__construct($entityManager, $logger, Order::class);
    }

    public function createdOrder(array $data): array
    {
        $res['status'] = 'succeeded';
        $store = $this->entityManager
            ->getRepository(Store::class)
            ->findOneBy(['uid' => $data['storeUid']]);
        if (!$store) {
            throw new StoreNotFoundException();
        }

        if (!$store->getIsShopStore() && !$store->isBooking()) {
            throw new Exception(
                "Unauthorized payment. Please check your payment details or contact customer support if the problem persists.",
                401
            );
        }

        if ($store->getIsShopStore() && !$store->getConfigShop()?->getStripeKey()) {
            throw new Exception(
                "The current store is not configured to support payments. Please contact the site administrator.",
                401
            );
        }

        $cart = $this->cartManager->generateCart($data['cartUid']);
        $order = $this->generateOrder($data, $cart, $store);

        $this->orderLineManager->generateOrderLine($data, $order, false);

        if ($store->getIsShopStore() && !$store->isBooking()) {
            $res = $this->stripeService->createCharge($data, $store);
            if ($res['status'] === OrderStatus::ORDER_FAILED->label()) {
                return [
                    'status' => $res['failure_code'],
                    'message' => $res['failure_message']
                ];
            }
        }

        $this->update($order, false);
        $this->eventDispatcher->dispatch(
            new OrderEvent([$order]),
            OrderEvent::ON_ORDER
        );

        return [
            'status' => $res['status'],
            'message' => "Votre commande a bien été prise en compte, Vous allez recevoir un mail de confirmation."
        ];
    }


    public function generateOrder(array $data, Cart $cart, Store $store): Order
    {
        $reference = $this->helperService->generateStringRandom();
        return (new Order())
            ->setReference($reference)
            ->setStatus($data['status'])
            ->setType($data['type'])
            ->setCart($cart)
            ->setTotalHT($data['totalPrice'])
            ->setEmail($data['email'])
            ->setCellPhone($data['phone'])
            ->setComment($data['comment'])
            ->setModeDelivery($data['modeDelivery'])
            ->setStoreId($store);
    }


    public function orderValidate(Order $order): Order
    {
        $statusSucceeded = OrderStatus::ORDER_SUCCEEDED->label();
        if ($order->getStatus() === $statusSucceeded) {
            throw new Exception("La commande a déjà été Validée", 422);
        }

        $order
            ->setStatus($statusSucceeded)
            ->setIsValidate(1)
            ->setVisited(1);

        $this->update($order, false);

        $this->eventDispatcher->dispatch(
            new OrderEvent([$order]),
            OrderEvent::ON_ORDER_VALIDATE
        );

        return $order;
    }

    public function orderCanceled(Order $order): Order
    {
        $statusCanceled = OrderStatus::ORDER_CANCELED->label();
        if ($order->getStatus() === $statusCanceled) {
            throw new Exception("La commande a déjà été annulée", 422);
        }

        $order
            ->setStatus($statusCanceled)
            ->setIsValidate(1)
            ->setVisited(1);


        $this->update($order, false);
        $this->eventDispatcher->dispatch(
            new OrderEvent([$order]),
            OrderEvent::ON_ORDER_CANCELED
        );

        return $order;
    }


    public function cleanOrderWithPriceZero(): array
    {
        $table = [];
        $orders = $this->repo->findByTotalHtNullOrZero();
        $numOrdersToDelete = count($orders);

        if ($numOrdersToDelete > 0) {
            foreach ($orders as $order) {
                $this->entityManager->remove($order);
                $table[] = [$order->getId(), $order->getReference()];
            }

            $this->entityManager->flush();
            $this->entityManager->clear();

            $outputMessage = "Supprimé $numOrdersToDelete commande(s) avec un prix de 0/null.";
        } else {
            $outputMessage = "Aucune commande avec un prix de 0/null à supprimer.";
        }

        return [
            'outputMessage' => $outputMessage,
            'table' => $table ?? []
        ];
    }
}