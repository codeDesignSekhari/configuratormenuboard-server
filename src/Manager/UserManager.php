<?php

namespace App\Manager;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
        protected UserPasswordHasherInterface $passwordEncoder,
    )
    {
        parent::__construct($entityManager, $logger, User::class);
    }
}