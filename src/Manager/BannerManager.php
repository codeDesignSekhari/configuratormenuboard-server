<?php

namespace App\Manager;

use App\Entity\Banner;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class BannerManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, Banner::class);
    }
}