<?php

namespace App\Manager;

use App\Entity\Cart;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CartManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, Cart::class);
    }

    public function generateCart(string $uid): Cart
    {
        $cart = new Cart();
        $cart->setUid($uid);

        return $cart;
    }

}