<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MediaManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, \App\Entity\MediaManager::class);
    }
}