<?php

namespace App\Manager;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

trait hasTraitName
{
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['name:read'])]
    private ?string $name = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}