<?php

namespace App\Manager;

use App\Entity\MenuTitle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MenuTitleManager extends AbstractManager
{

    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, MenuTitle::class);
    }

}