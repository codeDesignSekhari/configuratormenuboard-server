<?php

namespace App\Manager;

use App\Entity\ItemMenu;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ItemMenuManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, ItemMenu::class);
    }
}