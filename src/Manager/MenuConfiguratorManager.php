<?php

namespace App\Manager;

use App\Entity\MenuConfigurator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MenuConfiguratorManager extends AbstractManager
{
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected LoggerInterface             $logger,
    )
    {
        parent::__construct($entityManager, $logger, MenuConfigurator::class);
    }
}