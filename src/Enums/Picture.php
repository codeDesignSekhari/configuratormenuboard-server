<?php

namespace App\Enums;

enum Picture: string
{
    case PICTURE_COVER = 'COVER';
    case PICTURE_PROFILE = 'PROFILE';
    case PICTURE_ORDER = 'ORDER';
    case PICTURE_TYPE = "PICTURE";
    case STATUS_MEDIA_COVER = "UPLOADING";
    case STATUS_MEDIA_PENDING = "PENDING";
}
