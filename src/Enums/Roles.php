<?php

namespace App\Enums;

enum Roles: string
{
    case ROLE_BACK = "ROLE_BACK";
    case ROLE_ADMIN = "ROLE_ADMIN";
}
