<?php

namespace App\Enums;

enum DayWeekConstant: string
{
    case MONDAY = 'monday';
    case TUESDAY = 'tuesday';
    case WEDNESDAY = 'wednesday';
    case THURSDAY = 'thursday';
    case FRIDAY = 'friday';
    case SATURDAY = 'saturday';
    case SUNDAY = 'sunday';

    case OPEN = 'open';
    case CLOSE = 'close';
}
