<?php

namespace App\Enums;

enum Key: string
{
    case UPDATE = "update";
    case DELETE = "delete";
    case CREATE = "create";
    case DATA = "data";
    case LOGOUT = "logout";
    case ERROR = "error";
    case NOT_FOUND = "notFound";
}
