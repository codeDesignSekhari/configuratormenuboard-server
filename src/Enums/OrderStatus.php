<?php

namespace App\Enums;

enum OrderStatus
{
    case ORDER_WAITING;
    case ORDER_CANCELED;
    case ORDER_FAILED;
    case ORDER_SUCCEEDED;


    public function label(): string
    {
        return match($this)
        {
            OrderStatus::ORDER_WAITING => 'WAITING',
            OrderStatus::ORDER_CANCELED => 'CANCELED',
            OrderStatus::ORDER_SUCCEEDED => 'SUCCEEDED',
            OrderStatus::ORDER_FAILED => 'FAILED',
        };
    }
}