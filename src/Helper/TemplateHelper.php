<?php

namespace App\Helper;

class TemplateHelper
{
    public const TEMPLATE_EMAIL_ORDER = "ORDER";
    public const TEMPLATE_EMAIL_ORDER_VALIDATE = "ORDER_VALIDATE";
    public const TEMPLATE_EMAIL_ORDER_CANCELED = "ORDER_CANCELED";
    public const TEMPLATE_EMAIL_RESET = "RESET";
    public const TEMPLATE_EMAIL_REGISTER = "REGISTER";
    public const TEMPLATE_EMAIL_MUST_RESET_PASSWORD = "MUST_RESET_PASSWORD";
}
