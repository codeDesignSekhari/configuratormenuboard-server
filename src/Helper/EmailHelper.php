<?php

namespace App\Helper;

use Symfony\Component\Mime\Address;

final class EmailHelper
{
    public static function senderAddress()
    {
        $env = strtolower($_ENV['APP_ENV']);

        if ($env === "dev") {
            return new Address('contact@imenus.fr', 'Imenus Developpement');
        }
    }

}
