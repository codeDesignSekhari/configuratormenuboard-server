<?php

namespace App\Security;

use App\Models\RecaptchaApi;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

class RecaptchaTokenSecurity
{
    public function verifyRecaptchaToken(string $token, Request $request): array
    {
        $recaptchaApi = new RecaptchaApi($request);
        $response = $recaptchaApi->responseVerify($token);

        return [
            'data' => [
                'action' => $response['action'],
                'challenge_ts' => $response['action'],
                'hostname' => $response['hostname'],
                'score' => $response['score'],
                'success' => $response['success'],
            ],
            "status" => Response::HTTP_OK
        ];
    }
}
