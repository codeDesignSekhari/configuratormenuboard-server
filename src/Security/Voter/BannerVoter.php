<?php

namespace App\Security\Voter;

use App\Entity\Banner;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BannerVoter extends Voter
{
    const VIEW = 'view_banner';
    const EDIT = 'edit_banner';

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Banner $banner */
        $banner = $subject;

        return match($attribute) {
            self::VIEW => $this->canView($banner, $user),
            self::EDIT => $this->canEdit($banner, $user),
            default => throw new \LogicException('Access denied')
        };
    }

    private function canView(Banner $banner, User $user): bool
    {
        if ($this->canEdit($banner, $user)) {
            return true;
        }

        return false;
    }

    private function canEdit(Banner $banner, User $user): bool
    {
        return $user === $banner->getStore()->getUser();
    }
}
