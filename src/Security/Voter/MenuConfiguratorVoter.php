<?php

namespace App\Security\Voter;

use App\Entity\Banner;
use App\Entity\MenuConfigurator;
use App\Entity\User;
use App\Enums\Roles;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MenuConfiguratorVoter extends Voter
{
    const VIEW = 'view_menu_configurator';
    const EDIT = 'edit_menu_configurator';

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var MenuConfigurator $menuConf */
        $menuConf = $subject;
        dd($subject);

        return match($attribute) {
            self::VIEW => $this->canView($menuConf, $user),
            self::EDIT => $this->canEdit($menuConf, $user),
            default => throw new \LogicException('Access denied')
        };
    }

    private function canView(MenuConfigurator $menuConfigurator, User $user): bool
    {
        if (in_array(Roles::ROLE_ADMIN, $user->getRoles())) {
            return true;
        }

        if ($this->canEdit($menuConfigurator, $user)) {
            return true;
        }

        return false;
    }

    private function canEdit(MenuConfigurator $menuConfigurator, User $user): bool
    {
        return $user === $menuConfigurator->getStore()->getUser();
    }
}
