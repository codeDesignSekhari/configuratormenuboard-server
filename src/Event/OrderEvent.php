<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class OrderEvent extends Event
{
    public const ON_ORDER = "on_order";
    public const ON_ORDER_VALIDATE = "on_order_validate";
    public const ON_ORDER_CANCELED = "on_order_canceled";

    public function __construct(
        private readonly array $orders,
    ) {
    }

    public function getOrders(): array
    {
        return $this->orders;
    }
}
