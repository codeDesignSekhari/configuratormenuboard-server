<?php

namespace App\Repository;

use App\Entity\Store;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Store|null find($id, $lockMode = null, $lockVersion = null)
 * @method Store|null findOneBy(array $criteria, array $orderBy = null)
 * @method Store[]    findAll()
 * @method Store[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoreRepository extends ServiceEntityRepository
{
    /**
     * StoreRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {

        parent::__construct($registry, Store::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findBySlugAndCategory(array $value)
    {
        return $this->createQueryBuilder('s')
            ->Where('s.slug = :slug')
            ->setParameter('slug', $value['slug'])
            ->andWhere('s.category = :category')
            ->setParameter('category', $value['category'])
            ->andWhere('s.city = :city')
            ->setParameter('city', $value['city'])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findStoreByQuery(string $query, array $params): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb->Where('s.name LIKE :name');
        $qb->setParameter('name', '%' . $query . '%');

        if (!empty($params['city'])) {
            $qb->andWhere('s.city = :city')
                ->setParameter('city', $params['city']);
        }

        //        if (!is_null($params['category'])) {
        //            $qb->leftJoin('s.category', 'cat')
        //                ->andWhere('cat.name = :category')
        //                ->setParameter('category', $params['category']);
        //        }

        return $qb
            ->getQuery()
            ->getResult();

    }


    public function findStoreByCategory(array $params): array
    {
        $qb = $this->createQueryBuilder('s');

        if ($params['city']) {
            $qb->andWhere('s.city = :city')
                ->setParameter('city', $params['city']);
        }

        if (isset($params['orderMinimum_10'])) {
            $qb->andWhere('s.price =< :price')
                ->setParameter('price', $params['orderMinimum_10']);
        }

        if (isset($params['orderMinimum_20'])) {
            $qb->andWhere('s.price =< :price')
                ->setParameter('price', $params['orderMinimum_10']);
        }

        if (!is_null($params['category']) && $params['category'] !==  'Tous') {
            $qb->leftJoin('s.category', 'cat')
                ->andWhere('cat.name = :category')
                ->setParameter('category', $params['category']);
        }

        return $qb
            ->getQuery()
            ->getResult();

    }

    /**
     * @throws Exception
     */
    public function findByUserByOffset(array $data)
    {
        $offset = $data['perPage'] * ($data['currentPage'] - 1);

        return $this->createQueryBuilder('s')
            ->orderBy('s.updateAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($data['perPage'])
            ->getQuery()
            ->getResult();
    }

    public function add(Store $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(Store $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /*
    public function findOneBySomeField($value): ?Store
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
