<?php

namespace App\Repository;

use App\Entity\DayWeek;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DayWeek|null find($id, $lockMode = null, $lockVersion = null)
 * @method DayWeek|null findOneBy(array $criteria, array $orderBy = null)
 * @method DayWeek[]    findAll()
 * @method DayWeek[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DayWeekRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DayWeek::class);
    }

    // /**
    //  * @return DayWeek[] Returns an array of DayWeek objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DayWeek
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
