<?php

namespace App\Repository;

use App\Entity\MenuTitleStyle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MenuTitleStyle>
 *
 * @method MenuTitleStyle|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuTitleStyle|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuTitleStyle[]    findAll()
 * @method MenuTitleStyle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuTitleStyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuTitleStyle::class);
    }

//    /**
//     * @return MenuTitleStyle[] Returns an array of MenuTitleStyle objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MenuTitleStyle
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
