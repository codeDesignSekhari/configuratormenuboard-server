<?php

namespace App\Repository;

use App\Entity\MenuConfiguratorStyle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MenuConfiguratorStyle>
 *
 * @method MenuConfiguratorStyle|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuConfiguratorStyle|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuConfiguratorStyle[]    findAll()
 * @method MenuConfiguratorStyle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuConfiguratorStyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuConfiguratorStyle::class);
    }

//    /**
//     * @return MenuConfiguratorStyle[] Returns an array of MenuConfiguratorStyle objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MenuConfiguratorStyle
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
