<?php

namespace App\Repository;

use App\Entity\MenuTitle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuTitle|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuTitle|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuTitle[]    findAll()
 * @method MenuTitle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuTitleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuTitle::class);
    }


    public function findMenuTitlesByStore($store): array
    {

        $query = $this->createQueryBuilder('m')
            ->innerJoin('m.store', 's')
            ->where('s.id = :store_id')
            ->setParameter('store_id', $store);

        return $query
            ->getQuery()
            ->getResult();
    }

    public function add(MenuTitle $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(MenuTitle $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }


    /*
    public function findOneBySomeField($value): ?MenuTitle
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
