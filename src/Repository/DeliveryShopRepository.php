<?php

namespace App\Repository;

use App\Entity\DeliveryShop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DeliveryShop>
 *
 * @method DeliveryShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeliveryShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeliveryShop[]    findAll()
 * @method DeliveryShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeliveryShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeliveryShop::class);
    }

//    /**
//     * @return DeliveryShop[] Returns an array of DeliveryShop objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DeliveryShop
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
