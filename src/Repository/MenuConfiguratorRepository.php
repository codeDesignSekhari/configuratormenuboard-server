<?php

namespace App\Repository;

use App\Entity\MenuConfigurator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuConfigurator|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuConfigurator|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuConfigurator[]    findAll()
 * @method MenuConfigurator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuConfiguratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuConfigurator::class);
    }

    public function add(MenuConfigurator $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(MenuConfigurator $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

//    /**
//     * @return int
//     * @throws NonUniqueResultException
//     */
//    public function countBy (): int
//    {
//        return $this->createQueryBuilder('m')
//            ->select('COUNT(m.id)')
////            ->where('m.store <= :id')
////            ->setParameter('id', $data['id'])
////                ->andWhere('d.active = :active')
////                ->setParameter('active', $data['active'])
////                ->orderBy('d.date_insert', 'DESC')
//            ->getQuery()
//            ->getOneOrNullResult();
//    }

    // /**
    //  * @return MenuConfigurator[] Returns an array of MenuConfigurator objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MenuConfigurator
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
