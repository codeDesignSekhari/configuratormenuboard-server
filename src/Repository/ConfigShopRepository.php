<?php

namespace App\Repository;

use App\Entity\ConfigShop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConfigShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfigShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfigShop[]    findAll()
 * @method ConfigShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfigShop::class);
    }

    // /**
    //  * @return ConfigShop[] Returns an array of ConfigShop objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConfigShop
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
