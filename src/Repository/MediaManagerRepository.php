<?php

namespace App\Repository;

use App\Entity\MediaManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MediaManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method MediaManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method MediaManager[]    findAll()
 * @method MediaManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MediaManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MediaManager::class);
    }


    /**
     * @return MediaManager|null Returns an array of MediaManager objects
     * @throws NonUniqueResultException
     */
    public function isMediaUploaderNull(): ?MediaManager
    {
        return $this->createQueryBuilder('m')
            ->innerJoin('m.menuConfigurators', 'mc')
            ->where('mc is NULL')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findOneBySomeField($value): ?MediaManager
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
