<?php

namespace App\Repository;

use App\Entity\ElementItemMenu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ElementItemMenu|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElementItemMenu|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElementItemMenu[]    findAll()
 * @method ElementItemMenu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElementItemMenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ElementItemMenu::class);
    }

    // /**
    //  * @return ElementItemMenu[] Returns an array of ElementItemMenu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ElementItemMenu
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
