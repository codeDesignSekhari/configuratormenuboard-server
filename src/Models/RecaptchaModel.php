<?php

namespace App\Models;

use ReCaptcha\RequestMethod;
use ReCaptcha\ReCaptcha;

class RecaptchaModel extends ReCaptcha
{
    public function __construct(RequestMethod $requestMethod = null)
    {
        parent::__construct($_ENV['RECAPTCHA_SECRET_KEY'], $requestMethod);
    }
}
