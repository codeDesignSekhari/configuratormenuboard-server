<?php

namespace App\Models\Stripe;

use Psr\Log\LoggerInterface;
use Stripe\Charge;
use Stripe\Exception\ApiErrorException;

class StripeModel extends Stripe
{
    private const CURRENCY_EUR = "eur";
    private const DESCRIPTION_CHARGE = "Customer product payment";

    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function createCharge(array $data): Charge|null
    {
        try {
            return $this->init()->charges->create([
                'currency' => self::CURRENCY_EUR,
                'source' => $data['token'],
                'amount' => $data['amount'],
                'description' => self::DESCRIPTION_CHARGE
            ]);
        } catch (ApiErrorException $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }
}
