<?php

namespace App\Models\Stripe;

use Stripe\StripeClient;

/**
 * https://stripe.com/docs/api/authentication?lang=php
 */
class Stripe
{
    public ?string $apiKey = null;

    private const VERSION_API_STRIPE = "2022-11-15";

    public function init(): StripeClient
    {
        return new StripeClient([
            "api_key" => $this->getApiKey(),
            "stripe_version" => self::VERSION_API_STRIPE
        ]);
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

}
