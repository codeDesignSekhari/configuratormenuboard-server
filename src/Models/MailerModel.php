<?php

namespace App\Models;

use App\Helper\TemplateHelper;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MailerModel
{
    private const NO_REPLY_ADDRESS = "contact@imenus.fr";

    private const TYPE_HTML = "HTML";
    private const TYPE_ATTACHMENT_FROM_PATH = "ATTACH_FROM_PATH";
    private const TYPE_TEXT = "TEXT";
    private const TYPE_EMBED_FROM_PATH = "EMBED_FROM_PATH";
    public const TYPE_HTML_TEMPLATE = "HTML_TEMPLATE";


    private const TYPE_MAP = [
        self::TYPE_HTML => 'html',
        self::TYPE_ATTACHMENT_FROM_PATH => 'attachFromPath',
        self::TYPE_TEXT => 'text',
        self::TYPE_EMBED_FROM_PATH => 'embedFromPath',
        self::TYPE_HTML_TEMPLATE => 'htmlTemplate',
    ];

    private const TEMPLATE_MAP = [
        self::TYPE_MAP[self::TYPE_HTML_TEMPLATE] => [
            TemplateHelper::TEMPLATE_EMAIL_RESET => 'Emails/reset/reset.html.twig',
            TemplateHelper::TEMPLATE_EMAIL_ORDER => 'Email/Order/order.html.twig',
            TemplateHelper::TEMPLATE_EMAIL_REGISTER => 'Email/reset/register.html.twig',
        ],
    ];

    private Address $addressTo;
    private Address $from;
    private string $subject;
    private array $context;
    public string $typeMessageContent = self::TYPE_MAP[self::TYPE_HTML_TEMPLATE];
    public string $template;


    public function __construct(
        private readonly MailerInterface   $mailer,
        protected readonly LoggerInterface $logger,
    ) {
    }

    public function setAddressTo(array $address): MailerModel
    {
        $this->addressTo = $address;

        return $this;
    }

    /**
     * @param string $subject
     * @return MailerModel
     */
    public function setSubject(string $subject): MailerModel
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param string $type
     * @return MailerModel
     */
    public function setTypeMessageContent(string $type): MailerModel
    {
        $this->typeMessageContent = self::TYPE_MAP[$type];

        return $this;
    }

    /**
     * @param string $template
     * @return MailerModel
     */
    public function setTemplate(string $template): MailerModel
    {
        $this->template = self::TEMPLATE_MAP[$this->typeMessageContent][$template];

        return $this;
    }

    public function setContext(array $data): MailerModel
    {
        $this->context = $data;

        return $this;
    }

    public function setFrom(Address $email): MailerModel
    {
        $this->from = $email;

        return $this;
    }



    /**
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function sendEmail(): void
    {
        try {
            $type = $this->typeMessageContent;
            $email = (new TemplatedEmail())
                ->from($this->from)
                ->to($this->addressTo)
                ->subject($this->subject)
                ->$type($this->template)
                ->context($this->context);

            $this->mailer->send($email);

        } catch (LoaderError|RuntimeError|SyntaxError $e) {
            $this->logger->error($e->getMessage());

            throw new Exception(
                "Un problème est survenue à l'envoi de l'email",
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

    }
}
