<?php

namespace App\Models;

use Symfony\Component\HttpFoundation\Request;

class RecaptchaApi
{
    public function __construct(private readonly Request $request)
    {
    }

    public function responseVerify(string $token): array
    {
        $recaptcha = new RecaptchaModel();

        return $recaptcha
            ->verify($token, $this->request->getClientIp())
            ->toArray();
    }
}
