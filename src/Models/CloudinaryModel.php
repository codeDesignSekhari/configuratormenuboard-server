<?php

namespace App\Models;

use App\Exception\FileException;
use App\Service\Constant\MessageResponse;
use Cloudinary\Api\ApiResponse;
use Cloudinary\Api\Exception\ApiError;
use Cloudinary\Api\Exception\GeneralError;
use Cloudinary\Cloudinary;
use Exception;
use Psr\Log\LoggerInterface;

class CloudinaryModel extends Cloudinary
{

    private const OPTION_IMAGE_DEFAULT = "image";

    /**
     * https://cloudinary.com/documentation
     * https://cloudinary.com/documentation/php_integration
     */
    public function __construct(private readonly LoggerInterface $logger)
    {
        parent::__construct($_ENV['CLOUDINARY_URL']);
    }


    /**
     * https://cloudinary.com/documentation/image_upload_api_reference
     */
    public function upload(string $file = null, array $options = []): ApiResponse
    {
        try {
            $params = [
                'resource_type' => $options['resourceType'] ?? self::OPTION_IMAGE_DEFAULT,
            ];

            $optionalParams = [
                'folder' => 'folder',
                'public_id' => 'publicId',
                'asset_id' => 'assetId',
            ];

            foreach ($optionalParams as $key => $value) {
                if (!empty($options[$value])) {
                    $params[$key] = $options[$value];
                }
            }

            return $this->uploadApi()->upload($file, $params);

        } catch (ApiError $apiError) {
            $this->logger->error('Error Cloudinary : ' . $apiError->getMessage());
            throw new FileException("An error occurred while manipulating files : " . $apiError->getMessage());
        }
    }

    public function rename(array $data, array $options = [])
    {
        return
            $this
                ->uploadApi()
                ->rename($data['fromPublicId'], $data['ToPublicId']);
    }

    /**
     * https://cloudinary.com/documentation/image_upload_api_reference#explicit
     */
    public function explicit(string $public_id = null, array $options = []): ApiResponse
    {
        try {
            return
                $this
                    ->uploadApi()
                    ->explicit(
                        $public_id,
                        [
                            'type' => 'upload',
                            'resource_type' => $options['resourceType'] ?? self::OPTION_IMAGE_DEFAULT,
                        ]
                    );
        } catch (Exception $exception) {
            $this->logger->error('Error Cloudinary : ' . $exception->getMessage());
            throw new FileException("An error occurred : " . $exception->getMessage());
        }
    }

    /**
     * https://cloudinary.com/documentation/admin_api
     */
    public function admin(string $public_id, array $options = []): ApiResponse
    {
        return
            $this
                ->adminApi()
                ->asset(
                    $public_id,
                    [
                        'colors' => true,
                        "resource_type" => $options['resourceType'] ?? self::OPTION_IMAGE_DEFAULT
                    ]
                );
    }

    /**
     * https://cloudinary.com/documentation/image_upload_api_reference#destroy
     */
    public function delete(string $public_id = null, array $options = []): ApiResponse
    {
        return
            $this
                ->uploadApi()
                ->destroy(
                    $public_id,
                    [
                        "resource_type" => $options['resourceType'] ?? self::OPTION_IMAGE_DEFAULT
                    ]
                );
    }

    /**
     * https://cloudinary.com/documentation/search_api
     * example :
     *  - $expression = 'resource_type:image AND tags=kitten AND uploaded_at>1d AND bytes>1m'
     * other params:
     *  - next_cursor string
     *  - with_field string
     *  - aggregate string
     */
    public function search(
        string $expression = null,
        string $sortBy = null,
        int    $maxResults = 10
    ): ApiResponse {
        try {
            $results =
                $this
                    ->searchApi()
                    ->expression($expression)
                    ->maxResults($maxResults);

            if (!empty($sortBy)) {
                $results->sortBy($sortBy);
            }

            return $results->execute();
        } catch (Exception $exception) {
            $this->logger->error('Error Cloudinary SEARCH : ' . $exception->getMessage());
            throw new FileException("An error occurred : " . $exception->getMessage());
        }
    }


}
