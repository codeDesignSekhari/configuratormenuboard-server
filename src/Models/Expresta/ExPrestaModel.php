<?php

namespace App\Models\Expresta;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ExPrestaModel extends Expresta
{
    public function __construct(
        protected HttpClientInterface $client,
        protected LoggerInterface     $logger
    )
    {
        parent::__construct($client, $logger);
    }

    public function getProducts(): array
    {
        try {
            $res = $this->client->request('GET', self::EXPRESTA_BASE_URI . '/data/products', [
                'headers' => ['AUTHORIZATION' => $this->getToken()],
            ]);

        } catch (\Exception $e) {
            $this->logger->debug("[DEBUG EXPRESTA GET PRODUCT]", [$e->getMessage()]);
            return [$e];
        }

        return $res?->toArray();
    }

    public function getSizes(): array
    {
        try {
            $res = $this->client->request('GET', self::EXPRESTA_BASE_URI . '/data/sizes', [
                'headers' => ['AUTHORIZATION' => $this->getToken()],
            ]);

        } catch (\Exception $e) {
            $this->logger->debug("[DEBUG EXPRESTA GET PRODUCT]", [$e->getMessage()]);
            return [$e];
        }

        return $res?->toArray();
    }

    public function getDeliveries(): array
    {
        try {
            $res = $this->client->request('GET', self::EXPRESTA_BASE_URI . '/data/deliveries-by-courier/fr', [
                'headers' => ['AUTHORIZATION' => $this->getToken()],
            ]);

        } catch (\Exception $e) {
            $this->logger->debug("[DEBUG EXPRESTA GET PRODUCT]", [$e->getMessage()]);
            return [$e];
        }

        return $res?->toArray();
    }

    public function calculatePrice(): array
    {
        try {
            $res = $this->client->request('POST', self::EXPRESTA_BASE_URI . '/data/calculate-price', [
                'headers' => ['AUTHORIZATION' => $this->getToken()],
                'body' => [
                    'email' => '',
                    'password' => '',
                ],
            ]);

        } catch (\Exception $e) {
            $this->logger->debug("[DEBUG EXPRESTA GET PRODUCT]", [$e->getMessage()]);
            return [$e];
        }

        return $res?->toArray();
    }

}
