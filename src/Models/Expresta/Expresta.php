<?php

namespace App\Models\Expresta;

use http\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Doc
 * https://docs.expresta.com/#8dfb981b-9749-4176-85f8-f178c6c29db5
 * Version 1.5
 * Contact développeurs : api@expresta.com
 * Documentation PDF : https://cdn.expresta.com/common/files/expresta_api.pdf
 */
class Expresta
{
    private const VERSION = 1.5;
    private const URL_LOGIN = "https://api.expresta.com/api/v1/login";
    public const EXPRESTA_BASE_URI = "https://api.expresta.com/api/v1";

    public function __construct(
        protected HttpClientInterface $client,
        protected LoggerInterface     $logger
    )
    {
        $this->login();
    }

    public ?string $email = null;

    public ?string $password = null;

    public ?string $url = null;

    public ?string $body = null;

    public ?string $token = null;

    public function login(): void
    {
        try {
            $res = $this->client->request(
                'POST',
                self::URL_LOGIN,
                [
                    'body' => [
                        'email' => $_ENV['EXPRESTA_EMAIL'],
                        'password' => $_ENV['EXPRESTA_PASSWORD'],
                    ],
                ]
            );

            $content = $res->toArray();
            if ($content['status'] === 'ok') {
                $this->setToken("Bearer " . $content['token']);
            }
        } catch (ClientExceptionInterface $e) {
            $this->logger->debug("[DEBUG EXPRESTA AUTH]", [$e->getMessage()]);
        }
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): void
    {
        $this->body = $body;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): void
    {
        $this->token = $token;
    }


}
