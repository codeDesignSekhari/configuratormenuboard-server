<?php

namespace App\Controller\ApiController;

use App\Entity\Store;
use App\Manager\MenuConfiguratorManager;
use App\Manager\MenuTitleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class fetchMenuConfiguratorFormatted extends AbstractController
{
    public function __construct(
        protected MenuConfiguratorManager $menuConfiguratorManager,
        protected MenuTitleManager        $menuTitleManager,
        protected EntityManagerInterface  $em,
    )
    {
    }

    private const NEWS = "NOUVEAUTÉS";

    public function __invoke(Store $store): JsonResponse
    {
        if ($store->getUser() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }

        $menuConfigurators = $this->menuConfiguratorManager
            ->repository()
            ->findBy(['store' => $store]);
        $categories = $this->menuTitleManager
            ->repository()
            ->findBy(['store' => $store]);
        $menus = [];
        foreach (array_values($categories) as $key => $category) {
            foreach ($menuConfigurators as $item) {
                if ($category?->getName() === $item->getMenuTitle()?->getName()) {
                    $menus[$key]['children'][] = $item;
                }
            }

            $menus[$key] = [
                'children' => $menus[$key]['children'] ?? [],
                'id' => $category->getId(),
                'name' => $category?->getName(),
                'rank' => $category?->getName() === self::NEWS ? 0 : $category->getRanked() + 1
            ];
        }

        return $this->json(
            $menus,
            Response::HTTP_OK,
            [],
            ['groups' => ['menuConfigurator:read']]
        );
    }
}
