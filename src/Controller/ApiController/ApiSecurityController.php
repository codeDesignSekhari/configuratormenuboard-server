<?php

namespace App\Controller\ApiController;

use App\Security\RecaptchaTokenSecurity;
use App\Service\Constant\MessageResponse;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ApiSecurityController extends AbstractController
{
    public function __construct(
        private readonly RecaptchaTokenSecurity $recaptchaTokenSecurity,
        private readonly LoggerInterface        $logger
    ) {
    }

    #[Route(
        path: '/recaptcha-token',
        name: 'app_recaptcha_token',
        methods: ['POST']
    )]
    public function create(Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);
            $response = $this->recaptchaTokenSecurity->verifyRecaptchaToken($data['token'], $request);


            return $this->json($response);
        } catch (
            ClientExceptionInterface|
            ServerExceptionInterface|
            RedirectionExceptionInterface $e
        ) {
            $this->logger->error('Recaptcha error: ' . $e->getMessage());

            return $this->json([
                'status' => $e->getCode(),
                "message" => MessageResponse::ERROR_MESSAGE_OCCURRED
            ]);
        }
    }
}
