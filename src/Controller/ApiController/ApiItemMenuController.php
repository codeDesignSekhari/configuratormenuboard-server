<?php

namespace App\Controller\ApiController;

use App\Service\MenuConfiguratorService\MenuConfiguratorService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiItemMenuController extends AbstractController
{
    public function __construct(
        private readonly MenuConfiguratorService $menuConfiguratorService)
    {
    }

    #[Route(path: '/back/item-menu/create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $this->menuConfiguratorService->createItemMenu($data, null);

        return $this->json([]);
    }
}
