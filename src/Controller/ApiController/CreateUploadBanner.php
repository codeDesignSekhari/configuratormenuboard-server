<?php

namespace App\Controller\ApiController;

use App\Service\BannerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateUploadBanner extends AbstractController
{
    public function __construct(
        protected BannerService $bannerService,
        protected EntityManagerInterface  $em
    ) {
    }


    #[Route(path: '/banners/upload', name: 'create_banner_upload', methods: ['POST'])]
    public function __invoke(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $data['store'] = $this->getUser()->getStore();
        $banner = $this->bannerService->create($data);


        return $this->json(
            $banner,
            Response::HTTP_OK,
        );
    }
}
