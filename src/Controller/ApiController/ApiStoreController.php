<?php

namespace App\Controller\ApiController;

use App\Repository\StoreRepository;
use App\Service\MenuConfiguratorService\MenuConfiguratorService;
use App\Service\StoreService\StoreService;
use App\Service\StripeService\StripeService;
use Exception;
use Stripe\Exception\ApiErrorException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;

class ApiStoreController extends AbstractController
{
    public function __construct() {}
}
