<?php

namespace App\Controller\ApiController;

use App\Entity\Category;
use App\Entity\MenuConfigurator;
use App\Entity\Slug;
use App\Entity\Store;
use App\Importer\MenuConfigurator\MenuConfiguratorImportService;
use Exception;
use App\Importer\MenuConfigurator\MenuConfiguratorExportService;
use App\Models\Expresta\ExPrestaModel;
use App\Service\MenuConfiguratorService\MenuConfiguratorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiMenuConfiguratorController extends AbstractController
{
    public function __construct(
        protected ExPrestaModel                 $exPrestaModel,
        protected MenuConfiguratorService       $menuConfiguratorService,
        protected MenuConfiguratorExportService $menuConfiguratorExportService,
        protected MenuConfiguratorImportService $menuConfiguratorImportService
    )
    {
    }

    #[Route(
        path: '/restaurants/{city}/{category}/{slug}',
        name: 'api_front_menu_configurator_show',
        methods: ['GET']
    )]
    #[ParamConverter('category', options: ['mapping' => ['category' => 'name']])]
    #[ParamConverter('slug', options: ['mapping' => ['slug' => 'name']])]
    public function fetched(string $city, Category $category, Slug $slug): JsonResponse
    {
        return $this->json(
            $this->menuConfiguratorService->fetch($city, $category, $slug),
            Response::HTTP_OK,
            [],
            ['groups' => ['restaurantFront']]
        );
    }

    #[Route(
        path: '/restaurants/{slug}',
        name: 'api_front_menu_configurator_show2',
        methods: ['GET']
    )]
    #[ParamConverter('slug', options: ['mapping' => ['slug' => 'name']])]
    public function fetched2(Slug $slug): JsonResponse
    {
        try {
            return $this->json(
                $this->menuConfiguratorService->fetch2($slug),
                Response::HTTP_OK,
                [],
                ['groups' => ['restaurantFront']]
            );
        } catch (Exception $e) {
            return $this->json($e->getMessage(), 422);
        }
    }

    #[Route(path: '/menu_configurators/{id}/clone', methods: ['POST'])]
    public function cloneMenuProduct(MenuConfigurator $menuConfigurator): JsonResponse
    {
        try {
            $data = $this->menuConfiguratorService->cloneProductMenu($menuConfigurator, $this->getUser());

            return $this->json($data, Response::HTTP_OK, [], ['groups' => ['menuConfigurator:read']]);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }


    #[Route(path: '/files/menu_configurators/store/export/{store}', methods: ['GET'])]
    public function exportMenuExcel(Store $store): Response
    {
        try {
            return $this->menuConfiguratorExportService->export($store);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), 422);
        }
    }

    #[Route(path: '/menu_configurators/store/{store}/import', methods: ['POST'])]
    public function importMenu(Request $request, Store $store): JsonResponse
    {
        try {
            $queries = $request->query->all();

            /** @var UploadedFile $file */
            $file = $request->files->get('file');
            $fileName = $file->getClientOriginalName();

            $fileFolder = __DIR__ . '/../../../public/uploads/menuConfigurators/';

            $filePath = $fileFolder . $fileName;
            $file->move($fileFolder, $fileName);

            $response = $this
                ->menuConfiguratorImportService
                ->doImport(
                    $filePath,
                    $store,
                    $queries,
                    $this->getUser()
                );

            return $this->json($response);
        } catch (Exception $e) {
            return $this->json([
                'message' => "Une erreur s'est produite lors du traitement de la requête.", // feature add key trad
                'error' => $e->getMessage()
            ], 422);
        }
    }
}
