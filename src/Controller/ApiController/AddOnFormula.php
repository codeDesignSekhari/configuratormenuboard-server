<?php

namespace App\Controller\ApiController;

use App\Entity\Complement;
use App\Entity\ComplementGroup;
use App\Entity\MenuConfigurator;
use App\Service\ComplementGroupService;
use App\Service\ComplementService;
use DateTimeImmutable;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class AddOnFormula extends AbstractController
{
    public function __construct(
        protected LoggerInterface        $logger,
        protected ComplementGroupService $complementGroupService,
        protected ComplementService      $complementService,
    ) {
    }

    #[Route(path: '/menu_configurators/{menuConfigurator}/complements/add', name: 'add_on_complement_formula', methods: ['POST', 'PUT'])]
    public function __invoke(Request $request, MenuConfigurator $menuConfigurator): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);
            $complementGroup = new ComplementGroup();
            if ($request->isMethod('put')) {
                $complementGroup = $this->complementGroupService->find($data['complementGroupId']);
            }

            $complementGroup
                ->setName($data['name'])
                ->setMin($data['min'])
                ->setNbField($data['nbField'])
                ->setIsMultiple($data['isMultiple'])
                ->setMax($data['max']);

            if ($request->isMethod('post')) {
                $complementGroup
                    ->setMenuConfigurator($menuConfigurator)
                    ->setCreatedAt(new DateTimeImmutable());
            }

            foreach ($data['complements'] as $complementData) {
                $complement = new Complement();
                if (!empty($complementData['id']) && $complementData['id'] !== 0 && $request->isMethod('put')) {
                    $complement = $this->complementService->find($complementData['id']);
                }

                $complement
                    ->setName($complementData['name'])
                    ->setPrice($complementData['price']);

                if (isset($complementData['id']) && $complementData['id'] === 0 || $request->isMethod('post')) {
                    $complement
                        ->setCreatedAt(new DateTimeImmutable())
                        ->setComplementGroup($complementGroup);
                }

                $this->complementGroupService->persist($complement, false);
            }

            $this->complementGroupService->persist($complementGroup);

        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            return $this->json($e->getMessage(), 422);
        }


        return $this->json(
            'commplement.successful.add',
            Response::HTTP_OK,
            [],
            ['groups' => ['complementGroup:read']]
        );
    }

}
