<?php

namespace App\Controller\ApiController;

use App\Exception\StoreNotFoundException;
use App\Exception\UserNotFoundException;
use App\Models\MailerModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use App\Service\UserService\UserService;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiUserController extends AbstractController
{
    public function __construct(
        protected JWTEncoderInterface $jwtEncoder,
        protected UserService         $userService,
        protected LoggerInterface     $logger,
        protected MailerModel         $mailer,
    )
    {
    }

    #[Route(path: '/back/user_check', name: 'api_user_check', methods: ['GET'])]
    public function checkRequestTokenUser(Request $request): JsonResponse
    {
        $authorizationHeader = $request->headers->get('Authorization');
        $token = (str_contains((string)$authorizationHeader, 'Bearer'))
            ? substr($authorizationHeader, 7)
            : $authorizationHeader;

        if (empty($token)) {
            return $this->json(
                'Token not found'
            );
        }

        $decodeToken = $this->jwtEncoder->decode($token);

        if ($decodeToken === null) {
            throw new Exception('Token is null');
        }

        $data = $this->userService->fetchDataFront($decodeToken['id']);

        return $this->json(
            $data,
            Response::HTTP_OK,
            [],
            ['groups' => 'restaurantBack']
        );

    }


    #[Route(path: '/register', methods: ['POST'])]
    public function register(Request $request): JsonResponse
    {
        if (!in_array('ROLE_ADMIN', $this->getUser()?->getRoles())) {
            throw new AccessDeniedException('Access Denied, you do not have permission');
        }

        try {
            $data = json_decode($request->getContent(), true);
            $this->userService->create($data);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), 422);
        }

        return $this->json(
            'Le compte a bien été créer',
            Response::HTTP_OK,
        );
    }
}
