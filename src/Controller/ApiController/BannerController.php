<?php

namespace App\Controller\ApiController;

use App\Service\BannerService;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BannerController extends AbstractController
{
    public function __construct(
        protected LoggerInterface $logger,
        protected BannerService   $bannerService,
    )
    {
    }

    #[Route(path: '/banner/slider', name: 'get_banner_slider', methods: ['GET'])]
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $queries = $request->query->all();
            $data = $this->bannerService->getImageSlider($queries);
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            return $this->json($e->getMessage(), 422);
        }


        return $this->json(
            $data,
            Response::HTTP_OK,
            [],
            ['groups' => ['banner:read']]
        );
    }

}
