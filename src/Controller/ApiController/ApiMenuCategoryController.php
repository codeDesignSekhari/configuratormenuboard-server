<?php

namespace App\Controller\ApiController;

use App\Entity\Store;
use App\Service\MenuTitleService\MenuTitleService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiMenuCategoryController extends AbstractController
{
    public function __construct(
        protected MenuTitleService $menuTitleService
    )
    {
    }

    #[Route(path: 'stores/{id}/menu_titles', name: 'api_back_by_user_menu_title', methods: ['GET'])]
    public function fetchMenuTitles(Store $store): JsonResponse
    {
        try {
            $menuTitles = $store->getMenuTitles();
            return $this->json($menuTitles,
                Response::HTTP_OK,
                [],
                ['groups' => ['menuTitle:read']]
            );
        } catch (Exception $e) {
            return $this->json([
                'message' => "Une erreur s'est produite lors du traitement de la requête.", // feature add key trad
                'error' => $e->getMessage()
            ], 422);
        }
    }
}
