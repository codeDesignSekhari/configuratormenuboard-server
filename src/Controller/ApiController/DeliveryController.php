<?php

namespace App\Controller\ApiController;

use App\Entity\Delivery;
use App\Service\ComplementService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeliveryController extends AbstractController
{
    public function __construct(
        protected LoggerInterface        $logger,
        protected ComplementService      $complementService,
        protected EntityManagerInterface $em
    )
    {
    }

    #[Route(path: '/deliveries/removes', methods: ['POST'])]
    public function removeDeliveries(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        try {
            foreach ($data['ids'] as $id) {
                $delivery = $this->em->getRepository(Delivery::class)->find($id);
                if ($delivery) {
                    $this->em->remove($delivery);
                }
            }
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            return $this->json($e->getMessage(), 422);
        }


        return $this->json('Deliveries remove');
    }

}
