<?php

namespace App\Controller\ApiController;

use App\Manager\OrderManager;
use App\Service\StripeService\StripeService;
use App\Service\UserService\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiStripeController extends AbstractController
{
    public function __construct(
        protected StripeService $stripeService,
        protected UserService   $userService,
        protected OrderManager  $orderManager,
    )
    {
    }

    #[Route(path: '/payment', name: 'api_payment_product', methods: ['POST'])]
    public function PaymentProduct(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        try {
            $res = $this->orderManager->createdOrder($data);
            return $this->json($res);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), 422);
        }
    }
}
