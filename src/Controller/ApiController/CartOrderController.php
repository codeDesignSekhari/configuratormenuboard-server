<?php

namespace App\Controller\ApiController;

use App\Entity\Order;
use App\Entity\Store;
use App\Manager\CartManager;
use App\Manager\OrderLineManager;
use App\Manager\OrderManager;
use App\Service\CartService\CartService;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class CartOrderController extends AbstractController
{
    public function __construct(
        protected CartService      $cartService,
        protected CartManager      $cartManager,
        protected OrderManager     $orderManager,
        protected orderLineManager $orderLineManager,
        protected LoggerInterface  $logger,
    )
    {
    }

    #[Route(path: '/carts/store/{id}/orders', name: 'post_cart_order_store', methods: ['POST'])]
    #[ParamConverter('store', options: ['mapping' => ['id' => 'uid']])]
    public function __invoke(Request $request, Store $store): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);
            $cart = $this->cartManager->generateCart($data['cartUid']);
            $order = $this->orderManager->generateOrder($data, $cart, $store);

            $this->orderLineManager->generateOrderLine($data, $order);
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            return $this->json($e->getMessage(), 422);
        }


        return $this->json(
            'Votre commande a bien été prise en compte, Vous allez recevoir un mail de confirmation.',
            Response::HTTP_OK,
            [],
            ['groups' => ['cart:read']]
        );
    }

    #[Route(path: '/orders/{id}/validate', name: 'put_order_validate', methods: ['PUT'])]
    public function orderValidate(Order $order): JsonResponse
    {
        try {
            $order = $this->orderManager->orderValidate($order);
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            return $this->json($e->getMessage(), 422);
        }

        return $this->json(
            [
                'response' => 'La commande a bien été validée',
                'order' => $order
            ],
            Response::HTTP_OK,
        );
    }

    #[Route(path: '/orders/{id}/canceled', name: 'put_order_canceled', methods: ['PUT'])]
    public function orderCanceled(Order $order): JsonResponse
    {
        try {
            $order = $this->orderManager->orderCanceled($order);
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            return $this->json($e->getMessage(), 422);
        }

        return $this->json(
            [
                'response' => 'La commande a bien été annulée',
                'order' => $order
            ],
            Response::HTTP_OK,
        );
    }


}
