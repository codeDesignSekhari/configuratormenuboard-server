<?php

namespace App\Controller\ApiController;

use App\Entity\User;
use App\Helper\TemplateHelper;
use App\Repository\UserRepository;
use App\Service\MailerService;
use App\Service\UserService\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

#[AsController]
class forgetPassword extends AbstractController
{
    public function __construct(
        protected UserService             $userService,
        protected LoggerInterface         $logger,
        protected MailerService           $mailerService,
        protected EntityManagerInterface  $em,
        protected TokenGeneratorInterface $tokenGenerator,
    )
    {
    }


    #[Route('/forgot_password', name: 'reset_forget_pass', methods: ['POST'])]
    public function __invoke(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $expiresAt = new \DateTimeImmutable('+3600 seconds', new \DateTimeZone('Europe/Paris'));

        $user = $this->userService->findOneBy(['email' => $data['email']]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        if ($user->isMustChangePassword()) {
            $user->setIsMustChangePassword(0);
        }

        $token = $this->tokenGenerator->generateToken();
        $user
            ->setToken($token)
            ->setTokenExpiredAt($expiresAt);

        $this->em->persist($user);

        $url = $_ENV['URL_FRONT'] . '/reset/' . $token;
        $templateEmail = TemplateHelper::TEMPLATE_EMAIL_RESET;
        $template = "Email/Order/$templateEmail.html.twig";
        $subject = "Vous avez oublié votre mot de passe ?";
        $title = "Réinitialisation de mot de passe : Ne vous inquiétez pas, nous sommes là pour vous aider !";
        $content = "Pour votre demande de réinitialisation de mot de passe, veuillez cliquer sur le lien suivant:";
        $twigData = [
            'url' => $url,
            'user' => $user,
            'title' => $title,
            'content' => $content
        ];

        $this->mailerService->mailerSend($template, $subject, $twigData, [], $user);

        $this->em->flush();

        return $this->json(
            ['response' => 'Email a été envoyé avec succès'],
            Response::HTTP_OK,
        );
    }

    #[Route('/forgot_password/{token}', name: 'reset_pass', methods: ['POST'])]
    public function forgetPasswordReset(
        string                      $token,
        Request                     $request,
        UserRepository              $usersRepository,
        UserPasswordHasherInterface $passwordHasher
    ): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $user = $usersRepository->findOneByToken($token);
        if (!$user) {
            return $this->json(
                ['response' => "Le jeton n'existe pas",],
                Response::HTTP_UNPROCESSABLE_ENTITY,
            );
        }

        $dateNow = new \DateTimeImmutable();
        $expiresAt = $user?->getTokenExpiredAt();

        if (!$expiresAt) {
            return $this->json(
                [
                    'response' => 'Le jeton a expiré',
                    'expired' => true
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
            );
        }

        if (!$dateNow->format('Y-m-d H:i:s') > $expiresAt->format('Y-m-d H:i:s')) {
            $user->setTokenExpiredAt(null);
            $this->em->persist($user);
            $this->em->flush();

            return $this->json(
                [
                    'response' => 'Le jeton a expiré',
                    'expired' => true
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
            );
        }

        $user->setToken(null)
            ->setPassword(
                $passwordHasher->hashPassword(
                    $user,
                    $data['password']
                )
            )
            ->setTokenExpiredAt(null);

        $this->em->persist($user);
        $this->em->flush();

        return $this->json(
            ['response' => 'Votre mot de passe a été modifié avec succès'],
            Response::HTTP_OK,
        );
    }

    #[Route('/update_password', name: 'update_force_forget_pass', methods: ['POST'])]
    public function updatePasswordForce(Request $request): JsonResponse
    {
        $expiresAt = new \DateTimeImmutable('+3600 seconds', new \DateTimeZone('Europe/Paris'));

        $user = $this->userService->findOneBy(['mustChangePassword' => 1]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        if ($user->isMustChangePassword()) {
            return $this->json(
                ["response" => "L'email pour le changement de sécurité du compte a déjà été envoyé"],
                Response::HTTP_OK,
            );
        }

        $token = $this->tokenGenerator->generateToken();
        $user
            ->setToken($token)
            ->setTokenExpiredAt($expiresAt);

        $this->em->persist($user);

        $url = $_ENV['URL_FRONT'] . '/reset/' . $token;
        $templateEmail = TemplateHelper::TEMPLATE_EMAIL_RESET;
        $template = "Email/Order/$templateEmail.html.twig";
        $subject = "Changer votre mot de passe pour sécuriser votre compte";
        $content = "La sécurité de votre compte est une priorité pour nous, c'est pourquoi nous vous encourageons à prendre une mesure importante pour renforcer cette sécurité : changer votre mot de passe:";
        $twigData = [
            'url' => $url,
            'user' => $user,
            'content' => $content,
            'title' => $subject,
        ];

        $this->mailerService->mailerSend($template, $subject, $twigData, [], $user);

        $this->em->flush();

        return $this->json(
            ['changePassword' => true],
            Response::HTTP_OK,
        );
    }
}
