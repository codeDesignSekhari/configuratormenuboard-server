<?php

namespace App\Controller\ApiController;

use App\Entity\MenuConfigurator;
use App\Entity\Store;
use App\Service\ExtractPdfService;
use App\Service\MenuConfiguratorService\MenuConfiguratorService;
use App\Service\PdfService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class GenerateMenuPaper extends AbstractController
{
    public function __construct(
        protected Environment             $twig,
        protected PdfService              $pdfService,
        protected EntityManagerInterface  $em,
        protected MenuConfiguratorService $menuConfiguratorService,
    )
    {
    }

    #[Route(path: 'stores/{store}/menu/pdf', methods: ['GET'])]
    public function generateMenuPdf(Store $store): JsonResponse
    {
        $name = str_replace(' ', '-', $store->getName());
        $menuConfigurators = $this->em->getRepository(MenuConfigurator::class)->findBy([
            'store' => $store,
            'preview' => 1
        ]);

        $groupedMenus = $this->menuConfiguratorService->groupAndSortMenus($menuConfigurators);
        $html = $this->twig->render("PDF/GENERATE_MENU_PAPER.html.twig", [
            'menuConfigurators' => $groupedMenus,
            'store' => $store,
            //'logo' => base64_encode(file_get_contents('images/imenus-logo.png')),
        ]);

        $this->pdfService->generatePdf($html, "menu-$name.pdf");

        return $this->json(['response' => 'Le pdf a correctement été générer']);
    }

    #[Route(path: 'generate/menu', methods: ['POST'])]
    public function generateAutoMenu(Request $request): void
    {
        /**set_time_limit(0);
         * $file = $request->files->get('file');
         * $this->extractPdfService->extractPdf($file);**/
    }
}
