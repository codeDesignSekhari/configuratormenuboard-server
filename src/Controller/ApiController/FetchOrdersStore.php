<?php

namespace App\Controller\ApiController;

use App\Entity\Store;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class FetchOrdersStore extends AbstractController
{
    public function __invoke(Store $store): JsonResponse
    {
        return $this->json(
            $store->getOrders()->getValues(),
            Response::HTTP_OK,
            [],
            ['groups' => ['order:collection']]
        );
    }

}
