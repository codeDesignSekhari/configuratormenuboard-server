<?php

namespace App\Controller\ApiController;

use App\Entity\MediaManager;
use App\Service\MediaManagerService\MediaManagerService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiMediaManagerController extends AbstractController
{
    public function __construct(
        private readonly MediaManagerService $mediaManagerService
    )
    {
    }

    #[Route(path: '/back/media-manager', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);
            $this->mediaManagerService->providerUploader($data);

            return $this->json([]);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], 422);
        }
    }
}
