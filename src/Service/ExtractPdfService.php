<?php

namespace App\Service;

use Smalot\PdfParser\Parser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ExtractPdfService
{
    private Parser $parser;

    public function __construct() {
        $this->parser =  new Parser();
    }


    public function extractPdf(UploadedFile $file): array
    {
        $pdf = $this->parser->parseFile($file);

        $text = $pdf->getText();
        $images = $pdf->getObjectsByType('XObject', 'Image');
        $splitText = explode("€", $text);

        return [];
    }

}