<?php

namespace App\Service;

use App\Entity\ComplementGroup;
use App\Service\DoctrineManager\DoctrineManager;
use Doctrine\ORM\EntityManagerInterface;

use Psr\Log\LoggerInterface;

class ComplementGroupService extends DoctrineManager
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface        $logger,
    ) {
        parent::__construct($em, ComplementGroup::class);
    }
}
