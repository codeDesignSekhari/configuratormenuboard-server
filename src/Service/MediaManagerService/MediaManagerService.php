<?php

namespace App\Service\MediaManagerService;

use App\Entity\MediaManager;
use App\Entity\Store;
use App\Entity\TypeMediaManager;
use App\Enums\Picture;
use App\Exception\MediaManagerNotFoundException;
use App\Exception\ParameterMissingException;
use App\Exception\StoreNotFoundException;
use App\Models\CloudinaryModel;
use App\Service\StoreService\StoreService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MediaManagerService
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface        $logger,
        protected StoreService           $storeService,
        protected CloudinaryModel        $cloudinaryModel,
    ) {
    }

    public function mediaUploader(array $data): ?MediaManager
    {
        $hasContext = in_array($data['context'], [
            Picture::PICTURE_COVER->value,
            Picture::PICTURE_PROFILE->value,
            Picture::PICTURE_TYPE->value
        ]);

        if (!$hasContext || empty($data['context'])) {
            throw new ParameterMissingException("context");
        }

        if (empty($data['file'])) {
            throw new ParameterMissingException("FILE");
        }

        if (
            !empty($data['mediaManager']) &&
            $data['mediaManager'] instanceof MediaManager
        ) {
            $mediaManager = $data['mediaManager'];
            $resCloudinary = $this->cloudinaryModel->upload($data['file'], [
                "publicId" => $mediaManager->getFileName(),
                "assetId" => $mediaManager->getAssetId(),
            ]);

            $mediaManager->setUpdateAt(new \DateTime());
        } else {
            $this->logger->debug("[mediaManager] created");

            $mediaManager = new MediaManager();
            $resCloudinary = $this->cloudinaryModel->upload($data['file'], [
                "folder" => !empty($data['store']) ? $data['store']->getFolderNamePicture() : null
            ]);
        }

        $mediaManager
            ->setFileName($resCloudinary['public_id'])
            ->setAssetId($resCloudinary['asset_id'])
            ->setSize($resCloudinary['bytes'])
            ->setFormat($resCloudinary['format'])
            ->setUrl($resCloudinary['secure_url'])
            ->setStatus(Picture::STATUS_MEDIA_COVER->value);

        $typeMediaManager =
            $this
                ->em
                ->getRepository(TypeMediaManager::class)
                ->findOneBy(['name' => $data['context']]);

        $mediaManager->setType($typeMediaManager);

        if (
            !empty($data['store']) &&
            in_array($data['context'], [
                Picture::PICTURE_COVER->value,
                Picture::PICTURE_PROFILE->value
            ])
        ) {
            $data['store']->addMediaManager($mediaManager);
        }

        $this->em->persist($mediaManager);

        return $mediaManager;
    }


    public function hasPictureFromCloudinary(int $id): bool
    {
        $mediaManager = $this->em->getRepository(MediaManager::class)->find($id);

        if (!$mediaManager) {
            throw new MediaManagerNotFoundException();
        }

        $res = $this
            ->cloudinaryModel
            ->search(`'resource_type:image AND public_id:{$mediaManager->getFileName()}`);

        if ($res['total_count'] > 0) {
            return $res['resources'][0]['asset_id'] !== $mediaManager->getAssetId();
        }

        return false;
    }

    public function providerUploader(array $data): void
    {
        $store =
            $this->em->getRepository(Store::class)->find($data['storeId']);

        if (!$store instanceof Store) {
            throw new StoreNotFoundException();
        }


        $data['store'] = $store;
        $data['mediaManager'] = $store->findMediaManagerByAssetId($data['assetId']);
        $mediaManager = $this->mediaUploader($data);

        if ($data['context'] === Picture::PICTURE_TYPE->value) {
            $store->addMediaManager($mediaManager);
        }
    }

    public function removeAllMediaManagerByStore(int $id): array
    {
        $i = 0;
        $array = [];
        $pictures = [
            Picture::PICTURE_COVER->value,
            Picture::PICTURE_PROFILE->value
        ];

        /** @var Store $store */
        $store = $this->em
            ->getRepository(Store::class)
            ->find($id);

        $mediaManagers = $store->getMediaManagers()->toArray();
        foreach ($mediaManagers as $picture) {
            if (
                in_array($picture->getType()->getName(), $pictures) ||
                empty($picture->getFileName()) ||
                empty($picture->getAssetId()) ||
                $picture->getStatus() === Picture::STATUS_MEDIA_PENDING->value
            ) {
                $array[] = $picture->getId();
                $this->em->remove($picture);
                ++$i;
            }
        }

        return [
            'count' => $i,
            'array' => $array
        ];
    }

    public function delete(MediaManager|int $mediaManager, $flush = false): void
    {
        if (!$mediaManager) {
            $mediaManager = $this->em->getRepository(MediaManager::class)->find($mediaManager);
            if (!$mediaManager) {
                throw new MediaManagerNotFoundException();
            }
        }

        $search = $this
            ->cloudinaryModel
            ->search(`resource_type:image AND public_id:{$mediaManager->getFileName()}`);

        if ($search['total_count'] > 0) {
            $this->cloudinaryModel->delete($search['resources'][0]['public_id']);
            $this->em->remove($mediaManager);
        }
    }
}
