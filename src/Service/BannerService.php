<?php

namespace App\Service;

use App\Entity\Banner;
use App\Manager\BannerManager;
use App\Service\MediaManagerService\MediaManagerService;
use App\Service\RestWebService\RestWebService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class BannerService
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected MediaManagerService    $mediaManagerService,
        protected LoggerInterface        $logger,
        protected BannerManager          $bannerManager,
    )
    {
    }

    public function create(array $data): Banner
    {
        $mediaManager = $this->mediaManagerService
            ->mediaUploader($data);

        if (!$mediaManager) {
            throw new Exception('Picture is null');
        }

        $banner =
            (new Banner())
                ->setPicture($mediaManager)
                ->setName($data['name'])
                ->setStore($data['store'])
                ->setActive(true)
                ->setDevice($data['device'])
                ->setStartTime(new \DateTime($data['startTime']))
                ->setEndTime(new \DateTime($data['endTime']));

        $this->persist($banner);

        return $banner;
    }

    public function getImageSlider(array $queries): array
    {
        return $this->bannerManager->repository()->getImagesByFilter($queries);
    }

}
