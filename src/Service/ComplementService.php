<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class ComplementService
{
    public function __construct(
        protected LoggerInterface $logger,
    )
    {
    }
}
