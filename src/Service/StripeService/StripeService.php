<?php

namespace App\Service\StripeService;

use App\Entity\Store;
use App\Exception\StoreNotFoundException;
use App\Models\Stripe\StripeModel;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Stripe\Charge;

class StripeService
{
    public function __construct(
        public LoggerInterface $logger,
        public StripeModel $stripeModel,
        public EntityManagerInterface $entityManager
    ) {
    }

    public function createCharge(array $data, Store $store): ?Charge
    {
        $env = strtolower($_ENV['APP_ENV']);

        $stripe = new StripeModel($this->logger);

        if ($env === 'dev') {
            $stripe->setApiKey($_ENV['APY_KEY_STRIPE_PRIVATE']);
        } else {
            if (!$store->getStripeKeyConfigShop()) {
                throw new Exception(
                    "The Stripe configuration key for this store is missing. Please check the settings."
                );
            }

            $stripe->setApiKey($store->getStripeKeyConfigShop());
        }

        return $stripe->createCharge($data);
    }
}
