<?php

namespace App\Service\DoctrineManager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class DoctrineManager
{
    private readonly EntityRepository $repo;

    private readonly EntityManagerInterface $em;

    private $class;

    /**
     * @param EntityManagerInterface $em
     * @param $class
     */
    public function __construct(
        EntityManagerInterface $em,
                               $class
    )
    {
        $this->em = $em;
        $this->class = $class;
        $this->repo = $em->getRepository($class);
    }

    /**
     * Returns the collection
     */
    public function findAll(): array
    {
        return $this->repo->findAll();
    }

    public function findBy(array $data): array
    {
        return $this->repo->findBy($data);
    }

    public function find(int $id)
    {
        return $this->repo->find($id);
    }

    public function findOneBy(array $data)
    {
        return $this->repo->findOneBy($data);
    }

    public function persist($entity, $flush = true): void
    {
        $this->em->persist($entity);
        if ($flush) {
            $this->em->flush();
        }
    }

    public function flush(): void
    {
        $this->em->flush();
    }

    public function remove($entity, $flush = true): void
    {
        $this->em->remove($entity);
        if ($flush) {
            $this->em->flush();
        }
    }

    public function getCurrentClass()
    {
        return $this->class;
    }


    public function count(array $data): int
    {
        return $this->repo->count($data);
    }

    public function repository(): EntityRepository
    {
        return $this->repo;
    }

}
