<?php

namespace App\Service\OrderLineService;

use App\Service\ComplementService;
use App\Service\MediaManagerService\MediaManagerService;
use Doctrine\ORM\EntityManagerInterface;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;

class OrderLineService
{
    public function __construct(
        protected EntityManagerInterface   $em,
        protected MediaManagerService      $mediaManagerService,
        protected LoggerInterface          $logger,
        protected EventDispatcherInterface $eventDispatcher,
    ) {
    }
}
