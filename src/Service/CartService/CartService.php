<?php

namespace App\Service\CartService;

use Psr\Log\LoggerInterface;

class CartService
{

    public function __construct(
        protected LoggerInterface        $logger,
    ) {
    }
}
