<?php

namespace App\Service\MenuConfiguratorService;

use _PHPStan_978789531\Nette\Utils\DateTime;
use App\Entity\Category;
use App\Entity\ElementItemMenu;
use App\Entity\ItemMenu;
use App\Entity\MenuConfigurator;
use App\Entity\MenuTitle;
use App\Entity\Slug;
use App\Entity\Store;
use App\Enums\Key;
use App\Exception\ElementItemMenuNotFoundException;
use App\Exception\MenuConfiguratorNotFoundException;
use App\Exception\StoreNotFoundException;
use App\Service\MediaManagerService\MediaManagerService;
use App\Service\MenuTitleService\MenuTitleService;
use App\Service\StoreService\StoreService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class MenuConfiguratorService
{
    private const STATUS_ONLINE = 1;
    private const MENU_TITLE_NEW = "NOUVEAUTÉS";
    private const ALL = 'Tous';

    public function __construct(
        protected LoggerInterface        $logger,
        protected EntityManagerInterface $em,
        protected StoreService           $storeService,
        protected MenuTitleService       $menuTitleService,
        protected MediaManagerService    $mediaManagerService,
    )
    {
    }

    public function fetchMenuConfigAndMenuTitle(Store $store): array
    {
        $menus = [];
        $confMenuTitles = $this->em
            ->getRepository(MenuTitle::class)
            ->findBy(['store' => $store]);

        $menuConfigurators = $this->em
            ->getRepository(MenuConfigurator::class)
            ->findBy([
                'store' => $store,
                'preview' => self::STATUS_ONLINE
            ]);

        $hasMenuTitle = [];
        foreach ($confMenuTitles as $menuTitle) {
            $menuTitleName = $menuTitle->getName();

            if (!in_array($menuTitleName, $hasMenuTitle)) {
                $hasMenuTitle[] = $menuTitleName;
            }

            foreach ($menuConfigurators as $menuConfigurator) {
                if ($menuTitleName === $menuConfigurator->getMenuTitle()->getName()) {
                    $key = array_search($menuTitleName, array_column($menus, 'category'));

                    $rank = ($menuTitleName === self::MENU_TITLE_NEW)
                        ? 0
                        : ($menuTitle->getRanked() + 1);

                    if ($key === false) {
                        $menus[] = [
                            'category' => $menuTitleName,
                            'items' => [$menuConfigurator],
                            'rank' => $rank,
                        ];
                    } else {
                        $menus[$key]['items'][] = $menuConfigurator;
                    }
                }
            }
        }

        array_unshift($menus, ['category' => self::ALL]);

        return $menus;
    }


    public function insertCategoryFromMenuConfig(int $id): int
    {
        $count = 0;
        $store = $this->em
            ->getRepository(Store::class)
            ->find($id);
        $menuConfigurators = $store->getMenuConfigurators()->getValues();

        /** @var MenuConfigurator $menuConfig */
        foreach ($menuConfigurators as $menuConfig) {
            $menuTitleFromMenuConfigId = $menuConfig->getMenuTitle()->getId();
            $menuTitle = $this->em
                ->getRepository(MenuTitle::class)
                ->find($menuTitleFromMenuConfigId);

            if (
                $menuConfig->getMenuTitle()->getId() === $menuTitle->getId()
                && $menuTitle->getStore() === null
            ) {
                ++$count;

                $menuTitle->setStore($store);
                $this->em->persist($menuTitle);
            }
        }

        return $count;
    }

    public function persistMenuConfigurator(
        MenuConfigurator $menuConfig,
        array            $data
    ): MenuConfigurator
    {
        $countMenuConfigurators = $this->em
            ->getRepository(MenuConfigurator::class)
            ->count(['store' => $data['storeId']]);

        if ($countMenuConfigurators === 100) {
            throw new Exception(
                'The maximum number of menu configurators has been reached.'
            );
        }

        $store =
            $this->em
                ->getRepository(Store::class)
                ->find($data['storeId']);

        if (!$store instanceof Store) {
            throw new StoreNotFoundException();
        }

        $isUpdateMenuConfigId = $menuConfig->getId();
        if ($isUpdateMenuConfigId !== 0) {
            $menuConfig->setStore($store);
        }

        foreach ($data['form'] as $key => $field) {
            switch ($key) {
                case 'allergen':
                    $menuConfig->setAllergen(trim($field));
                    break;
                case 'priceMenu':
                    $menuConfig->setPriceMenu(floatval($field));
                    break;
                case 'subTitle':
                    $menuConfig->setSubTitle(trim($field));
                    break;
                case 'description':
                    $menuConfig->setDescription(trim($field));
                    break;
                case 'name':
                    $menuConfig->setName(trim($field));
                    break;
                case 'price':
                    $menuConfig->setPrice(floatval($field));
                    break;
                case 'supplementPrice':
                    $menuConfig->setSupplementPrice(floatval($field));
                    break;
                case 'supplement':
                    $menuConfig->setSupplement($field);
                    break;
            }
        }

        $menuTitle =
            $this->em
                ->getRepository(MenuTitle::class)
                ->find($data['form']['menuTitle']['id']);

        if ($menuTitle instanceof MenuTitle) {
            $menuConfig->setMenuTitle($menuTitle);
        }

        if (!empty($data['file'])) {
            $data['store'] = $store;
            $data['mediaManager'] = $menuConfig->getMediaManager();
            $mediaManager =
                $this
                    ->mediaManagerService
                    ->mediaUploader($data);

            $menuConfig->setMediaManager($mediaManager);
        }

        if (!empty($data['itemMenus'])) {
            $this->createItemMenu($data, $menuConfig);
        }

        $this->em->persist($menuConfig);
        return $menuConfig;
    }

    public function fetch2(Slug $slug): array
    {
        $store = $this->em
            ->getRepository(Store::class)->findOneBy([
                'slug' => $slug,
            ]);

        if (empty($store)) {
            throw new StoreNotFoundException();
        }

        if (!$store->getIsOnline()) {
            throw new Exception('Store is not enabled', Response::HTTP_UNAUTHORIZED);
        }

        $dataMenus = $this->fetchMenuConfigAndMenuTitle($store);

        return [
            'menus' => $dataMenus,
            'store' => $store,
        ];

    }


    public function fetch(string $city, Category $category, Slug $slug): array
    {
        /** @var Store $store */
        $store = $this->em->getRepository(Store::class)->findOneBy([
            'slug' => $slug,
        ]);

        if (empty($store)) {
            throw new StoreNotFoundException();
        }

        if (!$store->getIsOnline()) {
            throw new Exception('Store is not enabled', Response::HTTP_UNAUTHORIZED);
        }

        $dataMenus = $this->fetchMenuConfigAndMenuTitle($store);

        return [
            'menus' => $dataMenus,
            'store' => $store,
        ];

    }

    public function createItemMenu($data, $menuConfigurator): void
    {
        if (isset($data['menuConfiguratorId'])) {
            $menuConfigurator = $this->em
                ->getRepository(MenuConfigurator::class)
                ->find($data['menuConfiguratorId']);
        }

        foreach ($data['itemMenus'] as $itemMenu) {
            $dataItemMenu = $this->persistItemMenu($menuConfigurator, $itemMenu);
            foreach ($itemMenu['values'] as $values) {
                $this->persistElementItemMenu($dataItemMenu, $values);
            }
        }
    }

    private function persistElementItemMenu(ItemMenu $itemMenu, array $data): void
    {
        $elementItemMenu = $data['id'] !== 0
            ? $this->em
                ->getRepository(ElementItemMenu::class)
                ->find($data['id'])
            : new ElementItemMenu();

        $elementItemMenu
            ->setName($data['name'])
            ->setPrice($data['price']);

        $itemMenu->addElementItemMenu($elementItemMenu);

        $this->em->persist($itemMenu);

    }



    private function persistItemMenu(MenuConfigurator $menuConfigurator, array $data): ItemMenu
    {
        $itemMenu = $data['id'] !== 0
            ? $this->em
                ->getRepository(ItemMenu::class)
                ->find($data['id'])
            : new ItemMenu();

        $itemMenu->setTitle($data['title'])
            ->setNbField($data['nbField'])
            ->setNbChoice($data['nbChoice'])
            ->setMenuConfigurator($menuConfigurator);

        $this->em->persist($itemMenu);

        return $itemMenu;
    }


    public function groupAndSortMenus($menuConfigurators): array
    {
        $groupedMenus = [];
        foreach ($menuConfigurators as $menu) {
            $menuTitle = $menu->getMenuTitle()->getName();
            $groupedMenus[$menuTitle][] = $menu;
        }

        uasort($groupedMenus, function ($groupA, $groupB) {
            $rankedA = $groupA[0]->getMenuTitle()->getRanked();
            $rankedB = $groupB[0]->getMenuTitle()->getRanked();

            return $rankedA - $rankedB;
        });

        foreach ($groupedMenus as &$group) {
            usort($group, fn($menuA, $menuB) => $menuA->getPrice() <=> $menuB->getPrice());
        }

        return $groupedMenus ?? [];
    }


    public function cloneProductMenu(MenuConfigurator $menuConfigurator, object $user): MenuConfigurator
    {
        $this->validateStoreIds($user, $menuConfigurator);
        $name = "Produit dupliqué";
        $menuConfigurator = clone $menuConfigurator;

        $menuConfigurator
            ->setName($name)
            ->setMediaManager(null)
            ->setUpdateAt(new \DateTime())
            ->setPreview(0);

        $this->em->persist($menuConfigurator);

        return $menuConfigurator;
    }

    public function validateStoreIds(object $user, MenuConfigurator $menuConfigurator): void
    {
        $storeId = $user?->getStore()?->getId();
        $menuStoreId = $menuConfigurator?->getStore()?->getId();

        if ($storeId !== $menuStoreId) {
            throw new \Exception("Store IDs don't match", 403);
        }
    }
}
