<?php

namespace App\Service\helperService;

use Exception;

class helperService
{

    public function generateStringRandom(): string
    {
        $length = 4;
        $chars = "AZERTYUIOPQSDFGHJKLMWXCVBN1234567890";

        $value = '';
        for ($i = 0; $i < $length; ++$i) {
            $value .= $chars[random_int(0, strlen($chars))];
        }

        return $value;
    }

    /**
     * @throws Exception
     */
    public function generateToken(): string
    {
        $length = 40;
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_-?azertyuiopqsdfghjklmwxcvbn";

        $value = time();
        for ($i = 0; $i < $length; ++$i) {
            $value .= $chars[random_int(0, strlen($chars) - 1)];
        }

        return $value ?? '';
    }


    public function generatePasswordTemporary(int $number): string
    {
        $length = $number;
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZazertyuiopqsdfghjklmwxcvbn";
        $value = '';
        for ($i = 0; $i < $length; ++$i) {
            $value .= $chars[random_int(0, strlen($chars) - 1)];
        }

        return $value ?? '';
    }

    public function parsedFile(
        string $fileName,
        string $projectDir,
        string $extension = "php"
    ): string {
        $data = "";

        $confGenerateFilters = `$projectDir/config/configurator/$fileName.$extension`;
        $file = fopen($confGenerateFilters, 'r');

        // get menu in file config
        while (!feof($file)) {
            $line = fgets($file, 1024);
            $purgeLine = trim(preg_replace('#\s\s+#', ' ', $line));
            if ($purgeLine === "<?php" || $purgeLine === "") {
                continue;
            }

            $data .= $purgeLine;
        }

        fclose($file);

        return $data;
    }

    public function generateDateAsString($timestamp): string
    {

        if ($timestamp !== null) {
            $date = new \DateTime();

            $date->setTimestamp($timestamp);
            return $date->format('Y-m-d H:i:s');
        }

        return '';
    }
}
