<?php

namespace App\Service;

use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class PdfService
{
    private Dompdf $domPdf;

    public function __construct(
        #[Autowire('%kernel.project_dir%')]
        private $dir,
    ) {
        $this->domPdf = new Dompdf();
        $pdfOption = new Options();
        $pdfOption->set('defaultFont', 'Futura');
        $this->domPdf->setOptions($pdfOption);
    }

    public function binaryGeneratePdf(string $path, string $fileName): void
    {
        $output = $this->domPdf->output();
        $publicDirectory = "$this->dir$path";

        $pdfFilepath = "$publicDirectory$fileName";
        file_put_contents($pdfFilepath, $output);
    }


    public function generatePdf(
        $html,
        string $fileName,
        string $orientation = 'portrait',
        string $format = "A4",
    ): void {
        $this->domPdf->loadHtml($html);
        $this->domPdf->setPaper($format, $orientation);

        $this->render($fileName);
    }

    public function render(string $fileNme): void
    {
        $this->domPdf->render();
        $this->domPdf->stream($fileNme);
    }

}
