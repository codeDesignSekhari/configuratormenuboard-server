<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\User;
use App\Models\MailerModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class MailerService
{
    public function __construct(
        protected MailerModel            $mailer,
        protected MailerInterface        $mailerInterface,
        protected EntityManagerInterface $em,
        protected Environment            $twig,
        protected ParameterBagInterface  $parameterBag,
    )
    {
    }


    public function mailerSend(
        string  $template,
        string  $subject,
        ?array  $twigData = null,
        ?array  $attach = null,
        ?object $object = null
    ): void
    {
        $env = strtolower($_ENV['APP_ENV']);
        if ($object instanceof User) {
            $user = $object;
        } else {
            $objectEmail = $object?->getEmail();
            $store = $object?->getStore();
            $user = $store?->getUser();
        }

        $emailBody = $this->twig->render($template, $twigData);
        $email =
            (new Email())
                ->html($emailBody)
                ->subject($subject);


        $fromTo = new Address($_ENV['DEBUG_EMAIL'], "Imenus app");

        $email->from($fromTo);

        if ($env === 'dev' && !empty($_ENV['DEBUG_EMAIL_RECEIVING'])) {
            $to = new Address($_ENV['DEBUG_EMAIL_RECEIVING'], 'Test app');
        } else {
            $to = new Address($objectEmail ?? $user->getEmail());
        }

        $addressTo = $to;

        $email
            ->to($addressTo)
            ->subject($subject)
            ->html($emailBody);

        $this->mailerInterface->send($email);
    }
}
