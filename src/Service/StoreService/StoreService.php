<?php

namespace App\Service\StoreService;


use App\Service\helperService\helperService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class StoreService
{
    public function __construct(
        protected LoggerInterface        $logger,
        protected EntityManagerInterface $em,
        protected HelperService           $helperService,
    )
    {
    }
}
