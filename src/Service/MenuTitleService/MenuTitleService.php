<?php

namespace App\Service\MenuTitleService;

use App\Entity\MenuTitle;
use App\Entity\Store;
use App\Service\StoreService\StoreService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MenuTitleService
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface        $logger,
        protected StoreService           $storeService,
    ) {
    }


    public function checkRankToItemAndPersist(array $data): void
    {
        $currentMenuTitle = $this->em
            ->getRepository(MenuTitle::class)
            ->find($data['id']);

        $store = $this->em->getRepository(Store::class)->find($data['storeId']);

        $this->persistMenuTitleRank($currentMenuTitle, $data['newIndex']);
        $categoriesMenus = $this->em->getRepository(MenuTitle::class)->findMenuTitlesByStore($store);

        $filterHasChildrenCategories =
            array_filter($categoriesMenus, fn ($value) => $this->hasChildrenToCategoryMenu($value));

        /** @var MenuTitle $menuTitle */
        foreach ($filterHasChildrenCategories as $categoryMenu) {
            $currentItemChangedRank = $currentMenuTitle->getId() === $categoryMenu->getId();

            $itemRanked = $categoryMenu->getRanked();

            //value ranked is limit to total categories
            if ($itemRanked <= count($filterHasChildrenCategories)) {
                if ($currentItemChangedRank) {
                    $this->persistMenuTitleRank($categoryMenu, $data['newIndex']);
                    continue;
                }


                if ($itemRanked === $data['newIndex']) {
                    $this->persistMenuTitleRank($categoryMenu, $data['oldIndex']);
                    break;
                }
            }
        }
    }

    private function hasChildrenToCategoryMenu(MenuTitle $menuTitle): bool
    {
        return count($menuTitle->getMenuConfigurators()->toArray()) > 0;
    }


    public function persistMenuTitleRank(MenuTitle $menuTitle, $data): void
    {
        $menuTitle->setRanked($data);
        $this->em->persist($menuTitle);
    }

    public function create($data): void
    {
        $store = $this->em->getRepository(Store::class)->find($data['storeId']);

        $menuTitles =
            $this->em->getRepository(MenuTitle::class)->findMenuTitlesByStore($store);

        $definedNextRanked = ($lastMenuTitle = end($menuTitles))
            ? (int) $lastMenuTitle->getRanked() + 1
            : 1;

        $menuTitle =
            (new MenuTitle())
                ->setStore($store)
                ->setName(trim($data['name']))
                ->setRanked($definedNextRanked);

        $this->em->persist($menuTitle);
    }
}
