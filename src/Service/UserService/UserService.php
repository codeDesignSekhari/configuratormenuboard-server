<?php

namespace App\Service\UserService;

use App\Entity\Category;
use App\Entity\ConfigShop;
use App\Entity\Country;
use App\Entity\Slug;
use App\Entity\Store;
use App\Entity\Template;
use App\Entity\User;
use App\Enums\Roles;
use App\Models\MailerModel;
use App\Service\helperService\helperService;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

class UserService
{
    public function __construct(
        protected EntityManagerInterface      $em,
        protected UserPasswordHasherInterface $passwordEncoder,
        protected helperService               $helperService,
        protected JWTEncoderInterface         $jwtEncoder,
        protected LoggerInterface             $logger,
        protected MailerModel                 $mailer,
    )
    {
    }


    public function create($data): void
    {
        $user = new User();
        $slugger = new AsciiSlugger();
        $slug = new Slug();
        $configShop = new ConfigShop();
        $store = new Store();

        $category = $this->em
            ->getRepository(Category::class)
            ->findOneBy(['name' => $data['category']]);
        $country = $this->em
            ->getRepository(Country::class)
            ->findOneBy(['name' => $data['country']]);
        $template = $this->em
            ->getRepository(Template::class)
            ->findOneBy(['name' => 'premium']);

        // create config
        $configShop->setStripeKey($data['stripeKey']);
        if ($template instanceof Template) {
            $configShop->setTemplate($template);
        }


        // create User
        $plainPassword = $data['password'];
        $user->setEmail($data['email'])
            ->setFirstName($data['firstName'])
            ->setMustChangePassword(0)
            ->setLastName($data['lastName'])
            ->setPassword($this->passwordEncoder->hashPassword($user, $plainPassword))
            ->setRoles([Roles::ROLE_BACK->value]);

        $this->em->persist($user, false);

        //create slug
        $store->setName($data['name']);
        $createSlug = $slugger->slug($data['name'] . ' ' . strtolower($data['city']), '-', 'fr');
        $slug->setName($createSlug);
        $this->em->persist($slug, false);

        // create Store
        $store->setAddress($data['address'])
            ->setUid($this->helperService->guidv4())
            ->setCity($data['city'])
            ->setCompanyNumber($data['companyNumber'] ?? null)
            ->setPhoneNumber($data['phoneNumber'])
            ->setActivePremium(1)
            ->setIsOnline($data['phoneNumber'])
            ->setLatitude($data['latitude'])
            ->setLongitude($data['longitude'])
            ->setRating($data['rating'])
            ->setFolderNamePicture($data['folderName'])
            ->setBillingSubscription($data['subscription']['id'] ?? 0)
            ->setZipCode($data['zipCode'])
            ->setTags('changeTags');

        if ($category !== null) {
            $store->setCategory($category);
        }

        if ($country !== null) {
            $store->setCountry($country);
        }

        $store->setConfigShop($configShop)
            ->setUser($user)
            ->setSlug($slug);

        $this->em->persist($store);
    }
}
