<?php

namespace App\EventSubscriber;

use App\Entity\Order;
use App\Event\OrderEvent;
use App\Helper\TemplateHelper;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Util\Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class CartOrderSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            OrderEvent::ON_ORDER => [
                'onOrder'
            ],
            OrderEvent::ON_ORDER_VALIDATE => [
                'onOrderValidate'
            ],
            OrderEvent::ON_ORDER_CANCELED => [
                'onOrderCanceled'
            ]
        ];
    }

    public function __construct(
        protected MailerService          $mailerService,
        protected EntityManagerInterface $em,
    ) {
    }

    public function definedOrder(OrderEvent $event): Order
    {
        $order = $event->getOrders()[0];

        if (!$order) {
            throw new Exception('Order not defined');
        }

        return $order;
    }


    public function onOrder(OrderEvent $event): void
    {
        $order = $this->definedOrder($event);

        $reference = $order->getReference();

        $templateEmail = TemplateHelper::TEMPLATE_EMAIL_ORDER;
        $template = "Email/Order/$templateEmail.html.twig";
        $subject = "Préparation de votre commande n°$reference";
        $twigData = [
            'order' => $order,
        ];
        $attach = [];

        $this->mailerService->mailerSend($template, $subject, $twigData, $attach, $order);
    }


    public function onOrderValidate(OrderEvent $event): void
    {
        $order = $this->definedOrder($event);
        $reference = $order->getReference();

        $templateEmail = TemplateHelper::TEMPLATE_EMAIL_ORDER_VALIDATE;
        $template = "Email/Order/$templateEmail.html.twig";

        $subject = "Validation de votre commande n°$reference";
        $twigData = [
            'order' => $order,
            'title' => $subject,
        ];

        $attach = [];
        $this->mailerService->mailerSend($template, $subject, $twigData, $attach, $order);
    }

    public function onOrderCanceled(OrderEvent $event): void
    {
        $order = $this->definedOrder($event);
        $reference = $order->getReference();
        $templateEmail = TemplateHelper::TEMPLATE_EMAIL_ORDER_CANCELED;
        $template = "Email/Order/$templateEmail.html.twig";

        $subject = "Annulation de votre commande n°$reference";
        $twigData = [
            'order' => $order,
            'title' => $subject,
        ];

        $attach = [];
        $this->mailerService->mailerSend($template, $subject, $twigData, $attach, $order);
    }
}
