<?php

namespace App\DataFixtures;

use App\Entity\MenuConfigurator;
use Doctrine\Persistence\ObjectManager;

class MenuConfiguratorFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $menu = (new MenuConfigurator())
            ->setPreview(0)
            ->setDescription('Fixture test')
            ->setName('test')
            ->setPrice(12)
            ->setStore(2)
        ;

        $manager->persist($menu);
        $manager->flush();
    }
}
