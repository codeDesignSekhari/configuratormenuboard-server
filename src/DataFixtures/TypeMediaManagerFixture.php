<?php

namespace App\DataFixtures;

use App\Entity\TypeMediaManager;
use Doctrine\Persistence\ObjectManager;

class TypeMediaManagerFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $types = ['PICTURE', 'COVER', 'PROFILE', 'PRINT'];

        foreach ($types as $type) {
            $typeMediaManager = new TypeMediaManager();

            $typeMediaManager->setName($type);
            $manager->persist($typeMediaManager);
        }

        $manager->flush();
    }
}
