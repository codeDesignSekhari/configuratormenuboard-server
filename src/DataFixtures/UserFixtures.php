<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends BaseFixture
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordEncoder
    )
    {
    }

    protected function loadData(ObjectManager $manager): void
    {
        $user = (new User())
            ->setFirstname('youssouf')
            ->setLastName('admin')
            ->setEmail('webmastersekhari@gmail.com')
            ->setRoles(['ROLE_ADMIN'])
            ->setIsVerified(true)
            ->setPassword($this->passwordEncoder->hashPassword($user, 'imenus@2022!'));

        $manager->persist($user);
        $manager->flush();
    }
}
