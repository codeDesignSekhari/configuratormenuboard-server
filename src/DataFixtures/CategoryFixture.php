<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;

class CategoryFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $categories = [
            'BURGER', 'SALADE', 'SUSHI',
            'HALAL', "ASIATIQUE", 'AFRICAIN',
            'CHINOIS', 'TACOS', 'KEBAB',
            'INDIEN-PAKISTANAIS', 'LIBANAIS', "PIZZA",
        ];

        foreach ($categories as $value) {
            $category = (new Category())
                ->setName($value);

            $manager->persist($category);
        }

        $manager->flush();
    }
}
