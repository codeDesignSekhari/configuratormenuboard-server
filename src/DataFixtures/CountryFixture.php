<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Persistence\ObjectManager;

class CountryFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {

        $countries = ['FR', 'DZ'];

        foreach ($countries as $value) {
            $country = (new Country())
                ->setName($value);
            $manager->persist($country);
        }

        $manager->flush();
    }
}
