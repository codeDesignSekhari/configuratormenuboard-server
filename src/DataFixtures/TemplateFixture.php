<?php

namespace App\DataFixtures;

use App\Entity\Template;
use Doctrine\Persistence\ObjectManager;

class TemplateFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $templates = ['premium', 'default'];

        foreach ($templates as $value) {
            $template = new Template();

            $template->setName($value);
            $template->setValueKey($value);
            $manager->persist($template);
        }

        $manager->flush();
    }
}
