<?php

namespace App\EventListener;

use App\Entity\User;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class RequestSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected ParameterBagInterface   $params,
        protected ManagerRegistry         $doctrine,
        protected Security                $security,
        protected TokenGeneratorInterface $tokenGenerator,
        protected MailerService           $mailerService,
        protected EntityManagerInterface  $em,
        protected JWTEncoderInterface     $JWTEncoder,
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
            RequestEvent::class => 'onKernelRequest',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $request = $event->getRequest();
        $path = $request->getPathInfo();
        $allowedPaths = [
            '/v1/api',
            '/docs',
            '/uploads',
            '/images',
            '/_profiler',
        ];

        foreach ($allowedPaths as $allowedPath) {
            if (str_starts_with($path, $allowedPath)) {
                return;
            }
        }

        $response = new JsonResponse([
            'message' => 'Cannot GET ' . $path,
        ]);

        $event->setResponse($response);
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $route = $request->attributes->get('_route');
        $path = $request->getPathInfo();

        if (strpos($path, '/files')) {
            try {
                $token = $request->query->get('authorization');

                $data = $this->JWTEncoder->decode($token);
                $user = $this->em
                    ->getRepository(User::class)
                    ->findOneBy(['id' => $data['id']]);

                if ($user && !$user->hasBackRole()) {
                    throw new AccessDeniedException('Access Denied.');
                }

            } catch (JWTDecodeFailureException $e) {
                throw new AccessDeniedException($e->getMessage());
            }

            return;
        }

        $parameters = json_decode(mb_convert_encoding(
            $request->headers->get('x-custom-auth'), 'UTF-8', 'UTF-8'),
            true
        );

        $routeToAllowed = [
            'app_recaptcha_token',
            'login',
            'api_doc',
            "_profiler",
            "_profiler_home",
            "_profiler_search_results",
            '_profiler_search',
            'api_user_check',
            'api_front_menu_configurator_show2',
            'api_front_menu_configurator_show'
        ];

       if (!$parameters && !in_array($route, $routeToAllowed)) {
            throw new AccessDeniedException('Invalid x-custom-auth');
        }
    }
}
