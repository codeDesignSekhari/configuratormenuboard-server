<?php

namespace App\Importer\MenuConfigurator;

use App\Entity\MenuConfigurator;
use App\Entity\Store;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class MenuConfiguratorExportService
{
    public function __construct()
    {
    }

    public function export(Store $store): StreamedResponse
    {
        $fileName = "Export config menu " . $store->getName() . ' ' . date('d-m-y');

        $columnValues = [];
        $columnNames = [
            "Id \n(autogénérer)",
            "Nom du produit",
            "Description",
            "Sous titre \ndu produit",
            "Prix",
            "Prix du menu",
            'En stock',
            'Allérgen',
            'Référence du style',
            'Couleur de fond',
            "Intitulé du supplément",
            "Prix du Supplément",
            "Etat \n(visible ou non)",
            "Date de mise a jour",

            "Référence catégorie",
            "Nom de la catégorie",

            "Id \n(autogénérer)",
            "Adresse de l'image",
        ];

        /** @var MenuConfigurator $menuProduct */
        foreach ($store->getMenuConfigurators()->toArray() as $menuProduct) {
            $columnValues[] = [
                // product
                $menuProduct->getId(),
                $menuProduct->getName(),
                $menuProduct->getDescription(),
                $menuProduct->getSubTitle(),
                $menuProduct->getPrice(),
                $menuProduct->getPriceMenu(),
                $menuProduct->getInStock() ? 1 : 0,
                $menuProduct->getAllergen(),
                $menuProduct->getStyle()?->getId(),
                $menuProduct->getStyle()?->getBackgroundColor(),
                $menuProduct->getSupplement(),
                $menuProduct->getSupplementPrice(),
                $menuProduct->getPreview() ? 1 : 0,
                $menuProduct->getUpdateAt()->format('Y-m-d H:m:s'),
                // category
                $menuProduct->getMenuTitle()?->getId(),
                $menuProduct->getMenuTitle()?->getName(),
                // media
                $menuProduct->getMediaManager()?->getId(),
                $menuProduct->getMediaManager()?->getUrl(),
            ];
        }

        return $this->exportXLSMenuConfigurator($columnNames, $columnValues, $fileName, true);
    }


    public function exportXLSMenuConfigurator(
        array  $columnNamesSheet,
        array  $columnValuesSheet,
        string $fileName,
        bool   $attachment = false
    ): StreamedResponse
    {
        $titles = ['Produits'];

        $spreadsheet = $this->createSpreadSheet($columnNamesSheet, $columnValuesSheet, $titles);
        $writer = new Xlsx($spreadsheet);

        $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

        $fileName = str_replace(' ', '-', $fileName);
        $fileName = str_replace("/[^A-Za-z0-9\_\-]/", '', $fileName);

        $response = new StreamedResponse();
        $disposition = $response->headers->makeDisposition(
            $attachment ? ResponseHeaderBag::DISPOSITION_ATTACHMENT : ResponseHeaderBag::DISPOSITION_INLINE,
            $fileName . '.xlsx'
        );

        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', $disposition);
        $response->setCallback(function () use ($writer): void {
            $writer->save('php://output');
        });

        return $response;
    }

    private function createSpreadSheet(array $columnNamesSheet, array $columnValuesSheet, array $titles): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($titles[0]);

        $borderColumns = ['N1', 'P1', 'R1'];
        $colorColumns = [''];
        $headerColumns = [
            ['label' => 'Produit', 'first' => 'A1', 'last' => 'N1'],
            ['label' => 'Catégorie', 'first' => 'O1', 'last' => 'P1'],
            ['label' => 'Image', 'first' => 'Q1', 'last' => 'R1'],
        ];

        // header
        foreach ($headerColumns as $headerColumn) {
            $firstAndLast = $headerColumn['first'] . ':' . $headerColumn['last'];
            $sheet->setCellValue($headerColumn['first'], $headerColumn['label']);
            $sheet->mergeCells($firstAndLast);
            $sheet->getStyle($firstAndLast)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle($firstAndLast)->getFont()->setBold(true);
            $sheet
                ->getStyle($firstAndLast)
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_THICK)
                ->setColor(new Color('0000'));
        }

        // name
        $columLetter = 'A';
        $i = 2;
        foreach ($columnNamesSheet as $columnName) {
            $column = $columLetter . $i;
            $sheet->setCellValue($column, $columnName);

            $sheet->getColumnDimension($columLetter)
                ->setAutoSize(true);

            $columLetter++;
        }
        
        // value
        $i = 3;
        foreach ($columnValuesSheet as $columnValues) {
            $columLetter = 'A';
            foreach ($columnValues as $columnValue) {
                $column = $columLetter . $i;
                $sheet->setCellValue($column, $columnValue);

                //$sheet->getColumnDimension($columLetter)->setAutoSize(true);
                $columLetter++;
            }
            $i++;
        }

        $spreadsheet->setActiveSheetIndex(0);

        return $spreadsheet;
    }
}
