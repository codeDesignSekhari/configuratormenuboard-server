<?php

namespace App\Importer\MenuConfigurator;

use App\Entity\MenuConfigurator;
use App\Entity\MenuTitle;
use App\Entity\Store;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PhpOffice\PhpSpreadsheet\Reader\Csv as Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xls as Xsl;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Psr\Log\LoggerInterface;

class MenuConfiguratorImportService
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface        $logger,
    )
    {
    }

    /**
     * @throws Exception
     */
    public function readFile(string $filePath): Spreadsheet
    {
        $extension = pathinfo($filePath, PATHINFO_EXTENSION);

        $reader = match ($extension) {
            'xls' => new Xsl(),
            'xlsx' => new Xlsx(),
            'csv' => new Csv(),
            default => throw new Exception('Invalid extension'),
        };

        $reader->setReadDataOnly(true);

        return $reader->load($filePath);
    }

    public function createDataFromSpreadSheet(Spreadsheet $spreadsheet): array
    {
        $data = [];
        foreach ($spreadsheet->getWorksheetIterator() as $key => $workSheet) {
            $data[$key] = [
                'columnNames' => [],
                'columnValues' => [],
            ];

            foreach ($workSheet->getRowIterator() as $row) {
                $rowIndex = $row->getRowIndex();
                $index = 2;

                if ($rowIndex == 1) {
                    $data[$key]['columnNames'][$rowIndex] = [];
                    $rowName = 'columnNames';
                }

                if ($key > 0) {
                    $index = 3;
                }

                if ($rowIndex > $index) {
                    $data[$key]['columnValues'][$rowIndex] = [];
                    $rowName = 'columnValues';
                }

                $cellIterator = $row->getCellIterator();
                //$cellIterator->getIterateOnlyExistingCells();
                foreach ($cellIterator as $cell) {
                    $data[$key][$rowName][$rowIndex][] = $cell->getFormattedValue();
                }
            }
        }

        return $data;
    }


    public function doImport(string $filePath, Store $store, array $queries, ?User $user): array
    {
        $spreadSheet = $this->readFile($filePath);
        $data = $this->createDataFromSpreadSheet($spreadSheet);
        return $this->createMenuConfiguratorData($data, $store);
    }

    private function createMenuConfiguratorData(array $data, Store $store): array
    {
        try {
            $errors = [];
            $ranked = 1;

            foreach ($data as $values) {
                $menus = [];
                foreach ($values['columnValues'] as $idx => $value) {
                    $error = $this->processMenuData($value, $ranked, $store, $menus);
                    if (!empty($error)) {
                        $errors['Produits'][$idx] = $error;
                    }

                    $ranked++;
                }
            }

            if (empty($errors)) {
                $this->em->flush();
                return [];
            } else {
                return $errors;
            }
        } catch (Exception $e) {
            $errors['exception'] = [
                'message' => "Une erreur s'est produite lors du traitement de la requête.", // feature add key trad
                'error' => $e->getMessage(),
            ];

            return $errors;
        }
    }

    private function processMenuData(
        mixed $value,
        int   $ranked,
        Store $store,
        array &$menus,
    ): array
    {
        $errors = [];

        $menuConfiguratorId = (int)$value[0] ?? null;
        $productName = trim($value[1]) ?? null;
        $description = trim($value[2]) ?? null;
        $subTitle = trim($value[3]) ?? null;
        $price = (int)$value[4] ?? null;
        $priceMenu = (int)$value[5] ?? null;
        $stock = $value[6] ?? 1;
        $allergen = trim($value[7]) ?? null;
        $complementTitle = trim($value[10]) ?? null;
        $complementPrice = (int)$value[11] ?? 0;
        $menuTitleId = (int)$value[14] ?? null;
        $categoryName = trim(strtoupper($value[15])) ?? null;

        if (!$price) {
            $errors[] = "Le prix du produit est requis.";
            return $errors;
        }

        if (!$productName) {
            $errors[] = "Le nom du produit est requis.";
            return $errors;
        }

        $menuConfiguratorRepository = $this->em
            ->getRepository(MenuConfigurator::class);

        $menuConfiguratorExist = null;
        if ($menuConfiguratorId) {
            $menuConfiguratorExist = $menuConfiguratorRepository
                ->findOneBy([
                    'id' => $menuConfiguratorId,
                    'store' => $store
                ]);
        }

        if (!$menuConfiguratorExist) {
            $menuConfiguratorExist = $menuConfiguratorRepository
                ->findOneBy([
                    'store' => $store,
                    'name' => $productName,
                ]);
        }

        if (!$menuConfiguratorExist) {
            $menuConfiguratorExist = new MenuConfigurator();
        }

        $menuConfiguratorExist
            ->setName($productName)
            ->setDescription($description)
            ->setSubTitle($subTitle)
            ->setPrice($price)
            ->setPreview(false)
            ->setSupplement($complementTitle)
            ->setSupplementPrice($complementPrice)
            ->setPriceMenu($priceMenu)
            ->setInStock($stock)
            ->setStore($store)
            ->setAllergen($allergen);

        $menuTitleExist = null;
        if ($menuTitleId) {
            $menuTitleExist = $this->em
                ->getRepository(MenuTitle::class)
                ->findOneBy([
                    'id' => $menuTitleId,
                    'store' => $store
                ]);
        }

        if (!$menuTitleExist && $categoryName) {
            $menuTitleExist = $this->em
                ->getRepository(MenuTitle::class)
                ->findOneBy([
                    'name' => $categoryName,
                    'store' => $store
                ]);
        }

        $menuTitle = $menuTitleExist?->getName() ?? $categoryName;
        if ($menuTitleExist) {
            $menuTitle = $this->createMenuTitle($menuTitleExist, [
                'ranked' => $ranked,
                'categoryName' => $categoryName,
                'store' => $store
            ]);
            $this->em->persist($menuTitle);

        } else if (!isset($menus[$menuTitle])) {
            $menuTitle = $this->createMenuTitle($menuTitleExist, [
                'ranked' => $ranked,
                'categoryName' => $categoryName,
                'store' => $store
            ]);
            $menus[$menuTitle->getName()] = $menuTitle;
            $this->em->persist($menuTitle);
        }

        if (isset($menus[$categoryName])) {
            $menuConfiguratorExist->setMenuTitle($menus[$categoryName]);
        }

        $this->em->persist($menuConfiguratorExist);

        return [];
    }


    private function createMenuTitle(?MenuTitle $menuTitle, array $values): MenuTitle
    {
        if (!$menuTitle) {
            $menuTitle = new MenuTitle();
        }

        $menuTitle
            ->setRanked($values['ranked'])
            ->setName($values['categoryName'])
            ->setStore($values['store']);

        return $menuTitle;
    }
}
