<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TypePaymentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypePaymentRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class TypePayment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $createAt;

    #[ORM\Column(type: 'boolean')]
    private ?bool $cash = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $ticket = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $creditCard = null;

    #[ORM\OneToOne(mappedBy: 'typePayment', targetEntity: ConfigShop::class)]
    private ConfigShop $configShop;

    public function getConfigShop(): ?ConfigShop
    {
        $this->createAt = new \DateTime('now');
        return $this->configShop;
    }

    public function setConfigShop(?ConfigShop $configShop): self
    {
        $this->configShop = $configShop;
        return $this;
    }

    public function __construct()
    {
        $this->createAt = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateAt(): ?\DateTime
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTime $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getCash(): ?bool
    {
        return $this->cash;
    }

    public function setCash(bool $cash): self
    {
        $this->cash = $cash;

        return $this;
    }

    public function getTicket(): ?bool
    {
        return $this->ticket;
    }

    public function setTicket(bool $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getCreditCard(): ?bool
    {
        return $this->creditCard;
    }

    public function setCreditCard(bool $creditCard): self
    {
        $this->creditCard = $creditCard;

        return $this;
    }
}
