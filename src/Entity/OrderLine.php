<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Manager\hasTraitName;
use App\Repository\OrderLineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OrderLineRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['order:read']],
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['order:collection']],
        ),
        new Post(
            denormalizationContext: ['groups' => ['order:post']],
        ),
        new Put(
            denormalizationContext: ['groups' => ['order:put']],
        ),
        new Patch(),
        new Delete(),
    ],
    security: "is_granted('ROLE_BACK')",
)]
class OrderLine
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read', 'order:collection'
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read', 'order:collection'
    ])]
    private ?float $price = null;

    #[ORM\Column(nullable: true)]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read', 'order:collection'
    ])]
    private ?int $quantity = null;

    #[ORM\ManyToOne(inversedBy: 'orderLines')]
    #[JoinTable(name: "order")]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read'
    ])]
    private ?Order $order = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['orderLine:read', 'order:read', 'order:collection'])]
    #[ApiProperty(default:"now")]
    private ?\DateTimeInterface $createdAt;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read',  'order:collection'
    ])]
    private ?string $description = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'orderLines')]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read', 'order:collection'
    ])]
    private ?MediaManager $mediaManager = null;

    #[ORM\ManyToMany(targetEntity: Complement::class, inversedBy: 'orderLines')]
    #[Groups([
        'orderLine:read', 'order:post', 'order:read', 'order:collection'
    ])]
    private Collection $complement;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->complement = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMediaManager(): ?MediaManager
    {
        return $this->mediaManager;
    }

    public function setMediaManager(?MediaManager $mediaManager): self
    {
        $this->mediaManager = $mediaManager;

        return $this;
    }

    /**
     * @return Collection<int, Complement>
     */
    public function getComplement(): Collection
    {
        return $this->complement;
    }

    public function addComplement(Complement $complement): self
    {
        if (!$this->complement->contains($complement)) {
            $this->complement->add($complement);
        }

        return $this;
    }

    public function removeComplement(Complement $complement): self
    {
        $this->complement->removeElement($complement);

        return $this;
    }
}
