<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\ApiController\FetchOrdersStore;
use App\Manager\hasTraitName;
use App\Repository\TypeMediaManagerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TypeMediaManagerRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new GetCollection(),
        new Post(),
        new Put(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['typeMediaManager:read']],
    security: "is_granted('ROLE_BACK')"
)]
class TypeMediaManager
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['typeMediaManager:read', 'restaurantBack'])]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['typeMediaManager:read'])]
    private ?\DateTimeInterface $createAt = null;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: MediaManager::class)]
    private collection $mediaManagers;

    public function __construct()
    {
        $this->createAt = new \DateTime('now');
        $this->mediaManagers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|MediaManager[]
     */
    public function getMediaManagers(): Collection
    {
        return $this->mediaManagers;
    }

    public function addMediaManager(MediaManager $mediaManager): self
    {
        if (!$this->mediaManagers->contains($mediaManager)) {
            $this->mediaManagers[] = $mediaManager;
            $mediaManager->setType($this);
        }

        return $this;
    }

    public function removeMediaManager(MediaManager $mediaManager): self
    {
        if ($this->mediaManagers->contains($mediaManager)) {
            $this->mediaManagers->removeElement($mediaManager);
            // set the owning side to null (unless already changed)
            if ($mediaManager->getType() === $this) {
                $mediaManager->setType(null);
            }
        }

        return $this;
    }
}
