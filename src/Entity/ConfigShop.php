<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ConfigShopRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ConfigShopRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['configShop:read']],
    security: "is_granted('ROLE_BACK')"
)]
class ConfigShop
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['configShop:read', 'restaurantFront', 'restaurantBack'])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Template::class, inversedBy: 'configShops')]
    private ?Template $template = null;

    #[Groups('configShop:read')]
    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $customerStripe = null;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    #[Groups('configShop:read')]
    private ?string $subscriptionStripe = null;

    #[ORM\Column(type: 'string', length: 150, nullable: true)]
    #[Groups(['restaurantBack', 'configShop:read'])]
    private ?string $stripeKey = null;

    #[ORM\OneToOne(
        inversedBy: 'configShop',
        targetEntity: TypePayment::class,
        cascade: ['persist', 'remove']
    )]
    #[Groups('configShop:read')]
    private ?TypePayment $typePayment = null;

    #[ORM\Column(type: 'float')]
    #[Groups(['restaurantFront'])]
    private ?float $deliveryCost = 0;

    #[ORM\Column(type: 'float')]
    #[Groups(['restaurantFront'])]
    private ?float $minimumOrder = 0;

    #[ORM\OneToMany(mappedBy: 'configShop', targetEntity: DayWeek::class)]
    private collection $dayWeeks;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['restaurantFront', 'restaurantBack'])]
    private ?string $colorBase = null;

    public function __construct()
    {
        $this->dayWeeks = new ArrayCollection();
    }


    public function __toString()
    {
        return $this->getTemplate()->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemplate(): ?Template
    {
        return $this->template;
    }

    public function setTemplate(?Template $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getCustomerStripe(): ?string
    {
        return $this->customerStripe;
    }

    public function setCustomerStripe(string $customerStripe): self
    {
        $this->customerStripe = $customerStripe;

        return $this;
    }

    public function getSubscriptionStripe(): ?string
    {
        return $this->subscriptionStripe;
    }

    public function setSubscriptionStripe(?string $subscriptionStripe): self
    {
        $this->subscriptionStripe = $subscriptionStripe;

        return $this;
    }

    public function getStripeKey(): ?string
    {
        return $this->stripeKey;
    }

    public function setStripeKey(string $stripeKey): self
    {
        $this->stripeKey = $stripeKey;

        return $this;
    }

    public function getTypePayment(): ?TypePayment
    {
        return $this->typePayment;
    }

    public function setTypePayment(?TypePayment $typePayment): self
    {
        $this->typePayment = $typePayment;

        return $this;
    }

    public function getDeliveryCost(): ?float
    {
        return $this->deliveryCost;
    }

    public function setDeliveryCost(float $deliveryCost): self
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    public function getMinimumOrder(): ?float
    {
        return $this->minimumOrder;
    }

    public function setMinimumOrder(float $minimumOrder): self
    {
        $this->minimumOrder = $minimumOrder;

        return $this;
    }

    public function getDayWeeks(): Collection
    {
        return $this->dayWeeks;
    }

    public function addDayWeek(DayWeek $dayWeek): self
    {
        if (!$this->dayWeeks->contains($dayWeek)) {
            $this->dayWeeks[] = $dayWeek;
            $dayWeek->setConfigShop($this);
        }

        return $this;
    }

    public function removeDayWeek(DayWeek $dayWeek): self
    {
        // set the owning side to null (unless already changed)
        if ($this->dayWeeks->removeElement($dayWeek) && $dayWeek->getConfigShop() === $this) {
            $dayWeek->setConfigShop(null);
        }

        return $this;
    }

    public function getColorBase(): ?string
    {
        return $this->colorBase;
    }

    public function setColorBase(?string $colorBase): static
    {
        $this->colorBase = $colorBase;

        return $this;
    }
}
