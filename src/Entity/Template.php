<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TemplateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TemplateRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class Template
{
    #[Groups('template:read')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('template:read')]
    #[ORM\Column(type: 'string', length: 50)]
    private $name;

    #[Groups('template:read')]
    #[ORM\Column(type: 'string', length: 50)]
    private $valueKey;

    #[ORM\OneToMany(mappedBy: 'template', targetEntity: ConfigShop::class)]
    private $configShops;

    public function __construct()
    {
        $this->configShops = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValueKey(): ?string
    {
        return $this->valueKey;
    }

    public function setValueKey(string $valueKey): self
    {
        $this->valueKey = $valueKey;

        return $this;
    }

    /**
     * @return Collection|ConfigShop[]
     */
    public function getConfigShops(): Collection
    {
        return $this->configShops;
    }

    public function addConfigShop(ConfigShop $configShop): self
    {
        if (!$this->configShops->contains($configShop)) {
            $this->configShops[] = $configShop;
            $configShop->setTemplate($this);
        }

        return $this;
    }

    public function removeConfigShop(ConfigShop $configShop): self
    {
        if ($this->configShops->contains($configShop)) {
            $this->configShops->removeElement($configShop);
            // set the owning side to null (unless already changed)
            if ($configShop->getTemplate() === $this) {
                $configShop->setTemplate(null);
            }
        }

        return $this;
    }
}
