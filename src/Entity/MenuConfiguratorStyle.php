<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\MenuConfiguratorStyleRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MenuConfiguratorStyleRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class MenuConfiguratorStyle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $colorTitle = null;

    #[ORM\Column(length: 255)]
    private ?string $fontTitle = null;

    #[ORM\Column(length: 255)]
    private ?string $size = null;

    #[ORM\Column(length: 255)]
    private ?string $backgroundColor = null;

    #[ORM\Column(length: 255)]
    private ?string $colorText = null;

    #[ORM\OneToOne(mappedBy: 'style', cascade: ['persist', 'remove'])]
    private ?MenuConfigurator $menuConfigurator = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColorTitle(): ?string
    {
        return $this->colorTitle;
    }

    public function setColorTitle(string $colorTitle): static
    {
        $this->colorTitle = $colorTitle;

        return $this;
    }

    public function getFontTitle(): ?string
    {
        return $this->fontTitle;
    }

    public function setFontTitle(string $fontTitle): static
    {
        $this->fontTitle = $fontTitle;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): static
    {
        $this->size = $size;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(string $backgroundColor): static
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getColorText(): ?string
    {
        return $this->colorText;
    }

    public function setColorText(string $colorText): static
    {
        $this->colorText = $colorText;

        return $this;
    }

    public function getMenuConfigurator(): ?MenuConfigurator
    {
        return $this->menuConfigurator;
    }

    public function setMenuConfigurator(?MenuConfigurator $menuConfigurator): static
    {
        // unset the owning side of the relation if necessary
        if ($menuConfigurator === null && $this->menuConfigurator !== null) {
            $this->menuConfigurator->setStyle(null);
        }

        // set the owning side of the relation if necessary
        if ($menuConfigurator !== null && $menuConfigurator->getStyle() !== $this) {
            $menuConfigurator->setStyle($this);
        }

        $this->menuConfigurator = $menuConfigurator;

        return $this;
    }
}
