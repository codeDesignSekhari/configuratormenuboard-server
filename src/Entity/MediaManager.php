<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\MediaManagerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: MediaManagerRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['mediaManager:read']],
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['mediaManager:collection']],
        ),
        new Post(
            denormalizationContext: ['groups' => ['mediaManager:post']],
        ),
        new Put(),
        new Delete(),
    ],
    security: "is_granted('ROLE_BACK')"
)]
class MediaManager
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups([
        'mediaManager:read', 'banner:read',
        'cart:post', 'order:read',
        'cart:read', 'menuConfigurator:read',
        'restaurantFront', 'restaurantBack'
    ])]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['mediaManager:read', 'order:read', 'cart:read', 'menuConfigurator:read', 'banner:read'])]
    private ?\DateTime $createAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['mediaManager:read', 'mediaManager:post', 'order:read', 'cart:read'])]
    private ?\DateTimeInterface $updateAt = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([
        'mediaManager:read',
        'mediaManager:post',
        'cart:read',
        'order:read',
        'cart:read',
        'menuConfigurator:read',
        'restaurantFront',
        'order:collection',
        'banner:read',
        'restaurantBack'
    ])]
    private ?string $fileName = null;


    #[ORM\Column(type: 'string', length: 100)]
    #[Groups([
        'mediaManager:read', 'mediaManager:post',
        'order:read', 'cart:read', 'menuConfigurator:read',
        'banner:read', 'restaurantBack'
    ])]
    private ?string $assetId = null;

    #[ORM\OneToOne(
        mappedBy: 'mediaManager',
        targetEntity: MenuConfigurator::class
    )
    ]
    #[Groups(['mediaManager:read'])]
    private ?MenuConfigurator $menuConfigurators = null;

    #[ORM\ManyToMany(
        targetEntity: Store::class,
        inversedBy: 'mediaManagers'
    )
    ]
    #[Groups(['mediaManager:read', 'mediaManager:post'])]
    private collection $store;

    #[ORM\ManyToOne(
        targetEntity: TypeMediaManager::class,
        inversedBy: 'mediaManagers'
    )
    ]
    #[Groups([
        'mediaManager:read', 'mediaManager:post', 'cart:read', 'menuConfigurator:read',
        'banner:read', 'restaurantBack'
    ])]
    private ?TypeMediaManager $type = null;

    #[ORM\Column(type: 'string', length: 20)]
    #[Groups([
        'mediaManager:read', 'mediaManager:post',
        'order:read', 'cart:read', 'menuConfigurator:read', 'restaurantBack'
    ])]
    private ?string $status = null;

    #[ORM\Column(type: 'integer')]
    #[Groups([
        'mediaManager:read', 'mediaManager:post', 'order:read', 'cart:read',
        'menuConfigurator:read', 'banner:read', 'restaurantBack'
    ])]
    private ?int $size = null;

    #[ORM\Column(type: "string", length: 150)]
    #[Groups([
        'mediaManager:read', 'mediaManager:post', 'order:read', 'cart:read',
        'menuConfigurator:read', 'banner:read', 'restaurantBack'
    ])]
    private ?string $url = null;

    #[ORM\Column(type: "string", length: 20)]
    #[Groups([
        'mediaManager:read', 'mediaManager:post', 'order:read', 'cart:read',
        'menuConfigurator:read', 'banner:read', 'restaurantBack'
    ])]
    private ?string $format = null;

    #[ORM\OneToMany(mappedBy: 'mediaManager', targetEntity: OrderLine::class)]
    private Collection $orderLines;


    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->store = new ArrayCollection();
        $this->orderLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTime $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getMenuConfigurators(): ?MenuConfigurator
    {
        return $this->menuConfigurators;
    }

    public function setMenuConfigurator(?MenuConfigurator $menuConfigurator): self
    {
        $this->menuConfigurators = $menuConfigurator;
        return $this;
    }

    public function getStore(): Collection
    {
        return $this->store;
    }

    public function addStore(Store $store): self
    {
        if (!$this->store->contains($store)) {
            $this->store[] = $store;
        }

        return $this;
    }

    public function removeStore(Store $store): self
    {
        if ($this->store->contains($store)) {
            $this->store->removeElement($store);
        }

        return $this;
    }

    public function getType(): ?TypeMediaManager
    {
        return $this->type;
    }

    public function setType(?TypeMediaManager $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function setMenuConfigurators(?MenuConfigurator $menuConfigurators): self
    {
        $this->menuConfigurators = $menuConfigurators;

        // set (or unset) the owning side of the relation if necessary
        $newMediaManager = null === $menuConfigurators ? null : $this;
        if ($menuConfigurators->getMediaManager() !== $newMediaManager) {
            $menuConfigurators->setMediaManager($newMediaManager);
        }

        return $this;
    }

    public function getAssetId(): ?string
    {
        return $this->assetId;
    }


    public function setAssetId(?string $assetId): self
    {
        $this->assetId = $assetId;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return Collection<int, OrderLine>
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function addOrderLine(OrderLine $orderLine): self
    {
        if (!$this->orderLines->contains($orderLine)) {
            $this->orderLines->add($orderLine);
            $orderLine->setMediaManager($this);
        }

        return $this;
    }

    public function removeOrderLine(OrderLine $orderLine): self
    {
        if ($this->orderLines->removeElement($orderLine)) {
            // set the owning side to null (unless already changed)
            if ($orderLine->getMediaManager() === $this) {
                $orderLine->setMediaManager(null);
            }
        }

        return $this;
    }
}
