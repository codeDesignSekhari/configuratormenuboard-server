<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['order:read']],
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['order:collection']],
        ),
        new Post(
            denormalizationContext: ['groups' => ['order:post']],
        ),
        new Put(
            denormalizationContext: ['groups' => ['order:put']],
        ),
        new Patch(),
        new Delete(),
    ],
    security: "is_granted('ROLE_BACK')",
)]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read'])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 20)]
    #[Groups(['order:read', 'order:post', 'cart:read', 'store:read'])]
    private ?string $type = null;

    #[ORM\Column(type: 'datetime')]
    #[ApiProperty(default:"now")]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read'])]
    private ?\DateTimeInterface $createAt;

    #[ORM\Column(type: 'string', length: 50, unique: true)]
    #[Groups(['order:read', 'order:post', 'cart:read', 'order:collection', 'store:read'])]
    private ?string $reference = null;

    #[ORM\ManyToOne(targetEntity: Store::class, inversedBy: 'orders')]
    #[Groups(['order:read', 'order:post'])]
    private ?Store $store;

    #[ORM\Column(type: 'string', length: 20)]
    #[Groups(['order:read', 'order:post', 'cart:read', 'order:collection', 'store:read'])]
    private ?string $status = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['order:read', 'cart:read', 'store:read'])]
    private bool $visited = false;

    #[ORM\ManyToOne(targetEntity: Cart::class, cascade: ['persist', 'remove'], inversedBy: 'orders')]
    #[Groups(['order:read', 'order:post'])]
    private ?Cart $cart;


    #[ORM\Column]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read', 'cart:read'])]
    private bool $isValidate = false;

    #[ORM\OneToMany(mappedBy: 'order', targetEntity: OrderLine::class, cascade: ['persist', 'remove'])]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read',])]
    private Collection $orderLines;

    #[ORM\Column(length: 255)]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read', 'order:post'])]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read', 'order:post'])]
    private ?string $cellPhone = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read', 'order:post'])]
    private ?string $comment = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read', 'order:post'])]
    private ?float $totalHt = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['order:read', 'cart:read', 'order:collection', 'store:read', 'order:post'])]
    private ?string $modeDelivery = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $commentCancellation = null;

    #[ORM\OneToOne(mappedBy: 'orderShop', cascade: ['persist', 'remove'])]
    private ?DeliveryShop $deliveryShop = null;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->orderLines = new ArrayCollection();
    }

    public function getUidStore(): ?string
    {
        return $this->store->getUid();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getStoreId(): ?Store
    {
        return $this->store;
    }

    public function setStoreId(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setVisited(bool $visited): self
    {
        $this->visited = $visited;

        return $this;
    }

    public function getVisited(): bool
    {
        return $this->visited;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function isIsValidate(): bool
    {
        return $this->isValidate;
    }

    public function setIsValidate(bool $isValidate): self
    {
        $this->isValidate = $isValidate;

        return $this;
    }

    /**
     * @return Collection<int, OrderLine>
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function addOrderLine(OrderLine $orderLine): self
    {
        if (!$this->orderLines->contains($orderLine)) {
            $this->orderLines->add($orderLine);
            $orderLine->setOrder($this);
        }

        return $this;
    }

    public function removeOrderLine(OrderLine $orderLine): self
    {
        if ($this->orderLines->removeElement($orderLine)) {
            // set the owning side to null (unless already changed)
            if ($orderLine->getOrder() === $this) {
                $orderLine->setOrder(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCellPhone(): ?string
    {
        return $this->cellPhone;
    }

    public function setCellPhone(string $cellPhone): self
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getTotalHt(): ?float
    {
        return $this->totalHt;
    }

    public function setTotalHt(?float $totalHt): self
    {
        $this->totalHt = $totalHt;

        return $this;
    }

    public function getModeDelivery(): ?string
    {
        return $this->modeDelivery;
    }

    public function setModeDelivery(?string $modeDelivery): self
    {
        $this->modeDelivery = $modeDelivery;

        return $this;
    }

    public function getCommentCancellation(): ?string
    {
        return $this->commentCancellation;
    }

    public function setCommentCancellation(?string $commentCancellation): self
    {
        $this->commentCancellation = $commentCancellation;

        return $this;
    }

    public function getDeliveryShop(): ?DeliveryShop
    {
        return $this->deliveryShop;
    }

    public function setDeliveryShop(?DeliveryShop $deliveryShop): static
    {
        // unset the owning side of the relation if necessary
        if ($deliveryShop === null && $this->deliveryShop !== null) {
            $this->deliveryShop->setOrderShop(null);
        }

        // set the owning side of the relation if necessary
        if ($deliveryShop !== null && $deliveryShop->getOrderShop() !== $this) {
            $deliveryShop->setOrderShop($this);
        }

        $this->deliveryShop = $deliveryShop;

        return $this;
    }
}
