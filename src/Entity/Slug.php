<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\SlugRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SlugRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['slug:read']],
    denormalizationContext: ['groups' => ['slug:post']],
    security: "is_granted('ROLE_BACK')"
)]
class Slug
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 100, unique: true)]
    #[Groups(['slug:read', 'restaurantFront', 'store:read', 'restaurantBack'])]
    private ?string $name;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $createAt;

    /**
     * Slug constructor.
     */
    public function __construct()
    {
        $this->setCreateAt(new \DateTime('now'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }
}
