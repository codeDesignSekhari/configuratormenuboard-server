<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\DeliveryShopRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DeliveryShopRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class DeliveryShop
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $dateDelivery = null;

    #[ORM\OneToOne(inversedBy: 'deliveryShop', cascade: ['persist', 'remove'])]
    private ?Order $orderShop = null;

    #[ORM\ManyToOne(inversedBy: 'deliveryShops')]
    private ?Store $store = null;

    #[ORM\ManyToOne(inversedBy: 'deliveryShops')]
    private ?Delivery $delivery = null;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDelivery(): ?\DateTimeImmutable
    {
        return $this->dateDelivery;
    }

    public function setDateDelivery(\DateTimeImmutable $dateDelivery): static
    {
        $this->dateDelivery = $dateDelivery;

        return $this;
    }

    public function getOrderShop(): ?Order
    {
        return $this->orderShop;
    }

    public function setOrderShop(?Order $orderShop): static
    {
        $this->orderShop = $orderShop;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): static
    {
        $this->store = $store;

        return $this;
    }

    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    public function setDelivery(?Delivery $delivery): static
    {
        $this->delivery = $delivery;

        return $this;
    }
}
