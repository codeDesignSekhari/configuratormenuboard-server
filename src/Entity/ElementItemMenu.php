<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Manager\hasTraitName;
use App\Repository\ElementItemMenuRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ElementItemMenuRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class ElementItemMenu
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $createAt;

    #[ORM\Column(type: 'float')]
    private ?float $price = null;

    #[ORM\ManyToOne(targetEntity: ItemMenu::class, inversedBy: 'elementItemMenus')]
    private ?ItemMenu $itemMenu;


    public function __construct()
    {
        $this->createAt = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getItemMenu(): ?ItemMenu
    {
        return $this->itemMenu;
    }

    public function setItemMenu(?ItemMenu $itemMenu): self
    {
        $this->itemMenu = $itemMenu;

        return $this;
    }
}
