<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ItemMenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ItemMenuRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class ItemMenu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 50)]
    private ?string $title;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $createAt;

    #[ORM\ManyToOne(targetEntity: MenuConfigurator::class, inversedBy: 'itemMenu')]
    private ?MenuConfigurator $menuConfigurator = null;

    #[ORM\OneToMany(mappedBy: 'itemMenu', targetEntity: ElementItemMenu::class, cascade: ['persist', 'remove'])]
    private collection $elementItemMenus;

    #[ORM\Column(type: 'integer')]
    private ?int $nbField = 0;

    #[ORM\Column(type: 'integer')]
    private ?int $nbChoice = 1;

    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->elementItemMenus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreateAt(): ?\DateTime
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTime $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getMenuConfigurator(): ?MenuConfigurator
    {
        return $this->menuConfigurator;
    }

    public function setMenuConfigurator(?MenuConfigurator $menuConfigurator): self
    {
        $this->menuConfigurator = $menuConfigurator;

        return $this;
    }

    public function getElementItemMenus(): Collection
    {
        return $this->elementItemMenus;
    }

    public function addElementItemMenu(ElementItemMenu $elementItemMenu): self
    {
        if (!$this->elementItemMenus->contains($elementItemMenu)) {
            $this->elementItemMenus[] = $elementItemMenu;
            $elementItemMenu->setItemMenu($this);
        }

        return $this;
    }

    public function removeElementItemMenu(ElementItemMenu $elementItemMenu): self
    {
        // set the owning side to null (unless already changed)
        if ($this->elementItemMenus->removeElement($elementItemMenu) && $elementItemMenu->getItemMenu() === $this) {
            $elementItemMenu->setItemMenu(null);
        }

        return $this;
    }

    public function getNbField(): ?int
    {
        return $this->nbField;
    }

    public function setNbField(int $nbField): self
    {
        $this->nbField = $nbField;

        return $this;
    }

    public function getNbChoice(): ?int
    {
        return $this->nbChoice;
    }

    public function setNbChoice(int $nbChoice): self
    {
        $this->nbField = $nbChoice;

        return $this;
    }
}
