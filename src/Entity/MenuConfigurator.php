<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\ApiController\fetchMenuConfiguratorFormatted;
use App\Manager\hasTraitName;
use App\Repository\MenuConfiguratorRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MenuConfiguratorRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(
            uriTemplate: '/menu_configurators/store/{id}',
            controller: fetchMenuConfiguratorFormatted::class,
            name: 'fetch_menu_configurator_formatted_collection'
        ),
        new Post(
            routeName: 'api_back_create_menu_configurator',
            security: "is_granted('ROLE_BACK') and is_granted('edit_menu_configurator', object)",
            name: 'publication_menu_configurator',
        ),
        new Put(),
        new Patch(
            routeName: 'api_back_update_menu_configurator',
            security: "is_granted('ROLE_BACK') and is_granted('edit_menu_configurator', object)",
            name: 'update_menu_configurator'
        ),
        new Delete(
            security: "is_granted('ROLE_BACK') and is_granted('edit_menu_configurator', object)",
        ),
    ],
    normalizationContext: ['groups' => ['menuConfigurator:read']],
    security: "is_granted('ROLE_BACK')"
)]
#[ApiFilter(SearchFilter::class, properties: ['store.id'])]
class MenuConfigurator
{
    use hasTraitName;
    private const DEFAULT_NO_IMAGE = 'default_no_image_r5jf8s';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?int $id = null;

    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    #[ORM\Column(type: 'float')]
    private ?float $price = null;

    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    #[ORM\Column(type: 'text', length: 700, nullable: true)]
    private ?string $description = null;

    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    #[ORM\Column(type: 'boolean')]
    private ?bool $preview = false;

    #[Groups('menuConfigurator:read')]
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updateAt;

    #[ORM\ManyToOne(targetEntity: Store::class, inversedBy: 'menuConfigurators')]
    private ?Store $store = null;

    #[ORM\ManyToOne(targetEntity: MenuTitle::class)]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?MenuTitle $menuTitle = null;

    #[ORM\OneToOne(
        inversedBy: 'menuConfigurators',
        targetEntity: MediaManager::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true
    )]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?MediaManager $mediaManager = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?bool $inStock = true;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?string $subTitle = null;

    #[ORM\Column(type: 'string', length: 600, nullable: true)]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?string $allergen = null;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?float $priceMenu = null;

    #[ORM\OneToMany(
        mappedBy: 'menuConfigurator',
        targetEntity: ComplementGroup::class,
        cascade: ['persist', 'remove'],
    )]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private Collection $complementGroups;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?string $supplement = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['menuConfigurator:read', 'restaurantFront'])]
    private ?float $supplementPrice = null;

    #[ORM\OneToOne(inversedBy: 'menuConfigurator', cascade: ['persist', 'remove'])]
    #[Groups(['menuConfigurator:read'])]
    private ?MenuConfiguratorStyle $style = null;


    public function __construct()
    {
        $this->updateAt = new \DateTime();
        $this->complementGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPreview(): ?bool
    {
        return $this->preview;
    }

    public function setPreview(bool $preview): self
    {
        $this->preview = $preview;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTime $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getMenuTitle(): ?MenuTitle
    {
        return $this->menuTitle;
    }

    public function setMenuTitle(?MenuTitle $menuTitle): self
    {
        $this->menuTitle = $menuTitle;

        return $this;
    }

    public function getMediaManager(): ?MediaManager
    {
        return $this->mediaManager;
    }

    public function setMediaManager(?MediaManager $mediaManager): self
    {
        $this->mediaManager = $mediaManager;

        return $this;
    }

    public function getInStock(): ?bool
    {
        return $this->inStock;
    }

    public function setInStock(bool $inStock): self
    {
        $this->inStock = $inStock;

        return $this;
    }

    public function getSubTitle(): ?string
    {
        return $this->subTitle;
    }

    public function setSubTitle(?string $subtitle): self
    {
        $this->subTitle = $subtitle;

        return $this;
    }

    public function getAllergen(): ?string
    {
        return $this->allergen;
    }

    public function setAllergen(?string $allergen): self
    {
        $this->allergen = $allergen;

        return $this;
    }

    public function getPriceMenu(): ?float
    {
        return $this->priceMenu;
    }

    public function setPriceMenu(?float $priceMenu): self
    {
        $this->priceMenu = $priceMenu;

        return $this;
    }

    /**
     * @return Collection<int, ComplementGroup>
     */
    public function getComplementGroups(): Collection
    {
        return $this->complementGroups;
    }

    public function addComplementGroup(ComplementGroup $complementGroup): self
    {
        if (!$this->complementGroups->contains($complementGroup)) {
            $this->complementGroups->add($complementGroup);
            $complementGroup->setMenuConfigurator($this);
        }

        return $this;
    }

    public function removeComplementGroup(ComplementGroup $complementGroup): self
    {
        if ($this->complementGroups->removeElement($complementGroup)) {
            // set the owning side to null (unless already changed)
            if ($complementGroup->getMenuConfigurator() === $this) {
                $complementGroup->setMenuConfigurator(null);
            }
        }

        return $this;
    }

    public function getSupplement(): ?string
    {
        return $this->supplement;
    }

    public function setSupplement(?string $supplement): static
    {
        $this->supplement = $supplement;

        return $this;
    }

    public function getSupplementPrice(): ?float
    {
        return $this->supplementPrice;
    }

    public function setSupplementPrice(?float $supplementPrice): static
    {
        $this->supplementPrice = $supplementPrice;

        return $this;
    }

    public function getStyle(): ?MenuConfiguratorStyle
    {
        return $this->style;
    }

    public function setStyle(?MenuConfiguratorStyle $style): static
    {
        $this->style = $style;

        return $this;
    }
}
