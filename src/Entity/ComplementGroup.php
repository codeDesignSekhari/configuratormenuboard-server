<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Manager\hasTraitName;
use App\Repository\ComplementGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ComplementGroupRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(denormalizationContext: ['groups' => ['complementGroup:post']]),
        new Put(normalizationContext: ['groups' => ['complementGroup:put']]),
        new Patch(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['complementGroup:read']],
    security: "is_granted('ROLE_BACK')"
)]
#[ApiFilter(SearchFilter::class, properties: ['menuConfigurator.id'])]
class ComplementGroup
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["complementGroup:read", 'menuConfigurator:read', 'menuConfigurator:read', 'restaurantFront'])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['complementGroup:post', 'complementGroup:read', 'menuConfigurator:read', 'complementGroup:put', 'restaurantFront'])]
    private ?int $min = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['complementGroup:post', 'complementGroup:read', 'menuConfigurator:read', 'complementGroup:put', 'restaurantFront'])]
    private ?int $max = null;

    #[ORM\Column]
    #[Groups(['complementGroup:post', 'complementGroup:read', 'menuConfigurator:read', 'complementGroup:put', 'restaurantFront'])]
    private ?bool $isMultiple = null;

    #[ORM\Column]
    #[ApiProperty(default: "now")]
    private ?\DateTimeImmutable $createdAt;

    #[ORM\Column]
    #[ApiProperty(default: "now")]
    private ?\DateTimeImmutable $updateAt;

    #[ORM\OneToMany(
        mappedBy: 'complementGroup',
        targetEntity: Complement::class,
        cascade: ['persist', 'remove'],
    )]
    #[Groups(['complementGroup:read', 'menuConfigurator:read', 'complementGroup:put', 'restaurantFront'])]
    private Collection $complements;

    #[ORM\ManyToOne(targetEntity: MenuConfigurator::class, inversedBy: 'complementGroups')]
    #[Groups(['complementGroup:post'])]
    private ?MenuConfigurator $menuConfigurator = null;

    #[ORM\Column]
    #[Groups(['complementGroup:post', 'menuConfigurator:read', 'complementGroup:read', 'complementGroup:put'])]
    private ?int $nbField = null;

    public function __construct()
    {
        $this->updateAt = new \DateTimeImmutable();
        $this->createdAt = new \DateTimeImmutable();
        $this->complements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function isIsMultiple(): ?bool
    {
        return $this->isMultiple;
    }

    public function setIsMultiple(bool $isMultiple): self
    {
        $this->isMultiple = $isMultiple;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection<int, Complement>
     */
    public function getComplements(): Collection
    {
        return $this->complements;
    }

    public function addComplement(Complement $complement): self
    {
        if (!$this->complements->contains($complement)) {
            $this->complements->add($complement);
            $complement->setComplementGroup($this);
        }

        return $this;
    }

    public function removeComplement(Complement $complement): self
    {
        if ($this->complements->removeElement($complement)) {
            // set the owning side to null (unless already changed)
            if ($complement->getComplementGroup() === $this) {
                $complement->setComplementGroup(null);
            }
        }

        return $this;
    }

    public function getMenuConfigurator(): ?MenuConfigurator
    {
        return $this->menuConfigurator;
    }

    public function setMenuConfigurator(?MenuConfigurator $menuConfigurator): self
    {
        $this->menuConfigurator = $menuConfigurator;

        return $this;
    }

    public function getNbField(): ?int
    {
        return $this->nbField;
    }

    public function setNbField(int $nbField): self
    {
        $this->nbField = $nbField;

        return $this;
    }
}
