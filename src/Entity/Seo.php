<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\SeoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SeoRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class Seo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $title;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $metaDescription;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $metaKeyWords;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $metaRobots;

    #[ORM\Column(type: 'json', nullable: true)]
    private array $ldJson = [];

    #[ORM\OneToOne(targetEntity: Slug::class, cascade: ['persist', 'remove'])]
    private ?Slug $slug;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getMetaKeyWords(): ?string
    {
        return $this->metaKeyWords;
    }

    public function setMetaKeyWords(string $metaKeyWords): self
    {
        $this->metaKeyWords = $metaKeyWords;

        return $this;
    }

    public function getMetaRobots(): ?string
    {
        return $this->metaRobots;
    }

    public function setMetaRobots(string $metaRobots): self
    {
        $this->metaRobots = $metaRobots;

        return $this;
    }

    public function getLdJson(): ?array
    {
        return $this->ldJson;
    }

    public function setLdJson(?array $ldJson): self
    {
        $this->ldJson = $ldJson;

        return $this;
    }

    public function getSlug(): ?Slug
    {
        return $this->slug;
    }

    public function setSlug(?Slug $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
