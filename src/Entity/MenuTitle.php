<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Manager\hasTraitName;
use App\Repository\MenuTitleRepository;
use App\State\MenuTitleStateProcessor;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(),
        new Put(),
        new Patch(),
        new Delete(processor: MenuTitleStateProcessor::class),
    ],
    normalizationContext: ['groups' => ['menuTitle:read']],
    security: "is_granted('ROLE_BACK')"
)]
#[ORM\Entity(repositoryClass: MenuTitleRepository::class)]
class MenuTitle
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['menuTitle:read', 'menuConfigurator:read'])]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['menuTitle:read', 'menuConfigurator:read'])]
    private \DateTime $createAt;

    #[ORM\ManyToOne(targetEntity: Store::class, inversedBy: 'menuTitles')]
    #[Groups(['menuTitle:read'])]
    private ?Store $store = null;

    #[ORM\Column(type: 'integer')]
    #[Groups(['menuTitle:read', 'menuConfigurator:read'])]
    private ?int $ranked = null;


    public function __construct()
    {
        $this->createAt = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateAt(): ?\DateTime
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTime $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getRanked(): ?int
    {
        return $this->ranked;
    }

    public function setRanked(int $ranked): self
    {
        $this->ranked = $ranked;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    /**
     * @return $this
     */
    public function setStore(Store $store): self
    {
        $this->store = $store;

        return $this;
    }


    #[Groups(['countMenuConfigurator'])]
    public function getCountMenuConfigurator(): int
    {
        $menus = $this->getStore()?->getMenuConfigurators()->toArray();

        $id = $this->getId();
        $filteredMenus = array_filter($menus, function ($menu) use ($id) {
            return $menu?->getMenutitle()?->getId() === $id;
        });

        if (!empty($filteredMenus)) {
            return count($filteredMenus);
        }

        return 0;
    }
}
