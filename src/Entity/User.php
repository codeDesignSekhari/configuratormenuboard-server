<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Enums\Roles;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    operations: [
        new Get(security: "is_granted('ROLE_BACK')"),
        new GetCollection(security: "is_granted('ROLE_ADMIN')"),
        new Patch(security: "is_granted('ROLE_ADMIN')"),
        new Delete(security: "is_granted('ROLE_ADMIN')"),
        new Post(security: "is_granted('ROLE_ADMIN')")
    ],
    normalizationContext: ['groups' => ['user:read', 'restaurantBack']],
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['restaurantFront', 'user:read', 'restaurantBack'])]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(['user:post', 'order:read', 'store:read', 'user:read', 'restaurantBack'])]
    private ?string $email = null;

    #[ORM\Column(type: 'json')]
    #[Groups(['user:read', 'restaurantBack'])]
    private array $roles = ['ROLE_BACK'];

    #[Assert\NotBlank]
    #[Assert\Regex(
        pattern: '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/',
        message: 'Use 1 upper case letter, 1 lower case letter, and 1 number'
    )
    ]
    #[ORM\Column(type: 'string')]
    private string $password;

    #[ORM\Column(type: 'string', length: 40)]
    #[Groups(['user:read', 'restaurantBack'])]
    private ?string $firstName = null;

    #[ORM\Column(type: 'string', length: 40)]
    #[Groups(['user:read', 'restaurantBack'])]
    private ?string $lastName = null;

    #[ORM\OneToOne(
        mappedBy: 'user',
        targetEntity: Store::class,
        cascade: ['persist', 'remove']
    )]
    #[Groups(['user:read', 'restaurantBack'])]
    private Store $store;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['restaurantBack'])]
    private bool $isVerified = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['store:read', 'user:read', 'restaurantBack'])]
    private bool $hasAcceptedTermCondition = false;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $token = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $tokenExpiredAt = null;

    #[ORM\Column]
    #[Groups(['user:read', 'restaurantBack'])]
    private ?bool $mustChangePassword = null;

    public function getStore(): Store
    {
        return $this->store;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->email;
    }

    public function getUsername(): string
    {
        return (string)$this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = Roles::ROLE_BACK->value;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {

    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $store ? null : $this;
        if ($store->getUser() !== $newUser) {
            $store->setUser($newUser);
        }

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function getHasAcceptedTermCondition(): ?bool
    {
        return $this->hasAcceptedTermCondition;
    }

    public function setHasAcceptedTermCondition(bool $hasAcceptedTermCondition): self
    {
        $this->hasAcceptedTermCondition = $hasAcceptedTermCondition;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getTokenExpiredAt(): ?\DateTimeImmutable
    {
        return $this->tokenExpiredAt;
    }

    public function setTokenExpiredAt(?\DateTimeImmutable $tokenExpiredAt): self
    {
        $this->tokenExpiredAt = $tokenExpiredAt;

        return $this;
    }

    public function isMustChangePassword(): ?bool
    {
        return $this->mustChangePassword;
    }

    public function setMustChangePassword(bool $mustChangePassword): static
    {
        $this->mustChangePassword = $mustChangePassword;

        return $this;
    }

    public function hasBackRole(): bool
    {
        return in_array(Roles::ROLE_BACK->value, $this->roles);
    }
}
