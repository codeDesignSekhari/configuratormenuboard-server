<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Manager\hasTraitName;
use App\Repository\DeliveryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DeliveryRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
#[ApiFilter(SearchFilter::class, properties: ['store.uid'])]
class Delivery
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $zipCode = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\OneToMany(mappedBy: 'delivery', targetEntity: DeliveryShop::class)]
    private Collection $deliveryShops;

    #[ORM\ManyToOne(inversedBy: 'deliveries')]
    private ?Store $store = null;

    public function __construct()
    {
        $this->deliveryShops = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getZipCode(): ?int
    {
        return $this->zipCode;
    }

    public function setZipCode(int $zipCode): static
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, DeliveryShop>
     */
    public function getDeliveryShops(): Collection
    {
        return $this->deliveryShops;
    }

    public function addDeliveryShop(DeliveryShop $deliveryShop): static
    {
        if (!$this->deliveryShops->contains($deliveryShop)) {
            $this->deliveryShops->add($deliveryShop);
            $deliveryShop->setDelivery($this);
        }

        return $this;
    }

    public function removeDeliveryShop(DeliveryShop $deliveryShop): static
    {
        if ($this->deliveryShops->removeElement($deliveryShop)) {
            // set the owning side to null (unless already changed)
            if ($deliveryShop->getDelivery() === $this) {
                $deliveryShop->setDelivery(null);
            }
        }

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): static
    {
        $this->store = $store;

        return $this;
    }
}
