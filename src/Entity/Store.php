<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\ApiController\FetchOrdersStore;
use App\Enums\Picture;
use App\Manager\hasTraitName;
use App\Repository\StoreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: StoreRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(
            uriTemplate: '/stores/{uid}/orders',
            controller: FetchOrdersStore::class,
            security: "is_granted('ROLE_BACK') or object.user == user",
            name: 'ordersByStore'
        ),
        new GetCollection(),
        new Post(),
        new Put(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['store:read']],
    denormalizationContext: ['groups' => ['store:post']],
    security: "is_granted('ROLE_BACK')"
)]
class Store
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: false)]
    #[Groups(['store:read', 'user:read', 'restaurantFront', 'restaurantBack'])]
    private ?int $id = null;

    #[Groups(['store:read', 'store:post', 'restaurantFront', 'restaurantBack'])]
    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[ApiProperty(identifier: true)]
    private ?string $uid;

    #[Groups(['store:post'])]
    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $createAt = null;

    #[Groups(['store:post'])]
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updateAt = null;

    #[Groups(['store:read', 'store:post', 'restaurantBack'])]
    #[ORM\Column(type: 'boolean')]
    private ?bool $activePremium = null;

    #[ORM\OneToOne(
        inversedBy: 'store',
        targetEntity: User::class,
        cascade: ['persist', 'remove']
    )]
    #[Groups(['store:read', 'restaurantFront', 'order:read'])]
    private ?User $user = null;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'store')]
    #[Groups(['store:read', 'restaurantFront', 'restaurantBack'])]
    private ?Category $category = null;

    #[ORM\OneToMany(mappedBy: 'store', targetEntity: MenuConfigurator::class)]
    #[Groups(['withMenuConfigurators'])]
    private Collection $menuConfigurators;

    #[Groups(['store:read', 'store:post', 'restaurantFront', 'restaurantBack'])]
    #[ORM\OneToOne(targetEntity: Slug::class, cascade: ['persist', 'remove'])]
    private ?Slug $slug = null;

    #[Groups(['store:read', 'store:post', 'restaurantBack', 'restaurantFront'])]
    #[ORM\Column(type: 'integer')]
    private ?int $zipCode = null;

    #[Groups(['store:read', 'store:post', 'restaurantBack', 'restaurantFront'])]
    #[ORM\Column(type: 'string', length: 100)]
    private ?string $address = null;

    #[Groups(['store:post'])]
    #[ORM\Column(type: 'string', length: 14)]
    private ?string $companyNumber = null;

    #[Groups(['store:read', 'store:post', 'restaurantFront', 'restaurantBack'])]
    #[ORM\Column(type: 'string', length: 20)]
    private ?string $phoneNumber = null;

    #[Groups(['store:read', 'store:post', 'restaurantFront', 'restaurantBack'])]
    #[ORM\Column(type: 'string', length: 40)]
    private ?string $city = null;

    #[Groups(['store:post'])]
    #[ORM\ManyToOne(targetEntity: Country::class, inversedBy: 'stores')]
    private ?Country $country = null;

    #[ORM\OneToOne(targetEntity: ConfigShop::class, cascade: ['persist', 'remove'])]
    #[Groups(['restaurantFront', 'restaurantBack'])]
    private ?ConfigShop $configShop = null;

    #[Groups(['store:read', 'store:post'])]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $tags = null;

    #[Groups(['store:read', 'store:post', 'restaurantBack', 'restaurantFront'])]
    #[ORM\Column(type: 'boolean')]
    private int $isOnline = 0;

    #[Groups(['store:read', 'restaurantBack'])]
    #[ORM\ManyToMany(targetEntity: MediaManager::class, mappedBy: 'store')]
    private Collection $mediaManagers;

    #[Groups(['store:read', 'restaurantBack'])]
    #[ORM\Column(type: 'integer')]
    private ?int $billingSubscription = null;


    #[ORM\OneToMany(mappedBy: 'store', targetEntity: MenuTitle::class)]
    private Collection $menuTitles;

    #[Groups(['store:read', 'store:post', 'restaurantBack', 'restaurantFront'])]
    #[ORM\Column(type: 'boolean')]
    private int $isOpen = 0;

    #[Groups(['store:read', 'store:collection', 'order:collection'])]
    #[ORM\OneToMany(mappedBy: 'store', targetEntity: Order::class)]
    private Collection $orders;

    #[Groups(['store:read', 'store:post', 'restaurantFront', 'restaurantBack'])]
    #[ORM\Column(type: 'boolean')]
    private bool $isShopStore = false;

    #[Groups(['store:read', 'restaurantFront', 'restaurantBack', 'store:post'])]
    #[ORM\Column(type: 'boolean')]
    private bool $disabledShopStore = false;

    #[Groups(['store:read', 'store:post', 'restaurantFront'])]
    #[ORM\Column(type: 'float')]
    private ?float $rating;

    #[Groups(['store:read', 'store:post', 'restaurantFront'])]
    #[ORM\Column(nullable: true)]
    private ?float $latitude = null;

    #[Groups(['store:read', 'store:post', 'restaurantFront'])]
    #[ORM\Column(nullable: true)]
    private ?float $longitude = null;

    #[ORM\OneToMany(mappedBy: 'store', targetEntity: Banner::class)]
    private Collection $banners;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['store:read', 'restaurantFront', 'restaurantBack'])]
    private ?string $folderNamePicture = null;

    #[ORM\OneToMany(mappedBy: 'store', targetEntity: DeliveryShop::class)]
    private Collection $deliveryShops;

    #[ORM\OneToMany(mappedBy: 'store', targetEntity: Delivery::class)]
    private Collection $deliveries;

    #[ORM\Column]
    #[Groups(['store:read', 'restaurantFront', 'order:read', 'store:post', 'restaurantBack'])]
    private ?bool $actifDelivery = null;

    #[ORM\Column]
    #[Groups(['store:read', 'restaurantFront', 'store:post', 'restaurantBack'])]
    private ?bool $actifInSite = null;

    #[ORM\Column]
    #[Groups(['store:read', 'restaurantFront', 'store:post', 'restaurantBack'])]
    private ?bool $actifTakeAway = null;

    #[ORM\Column]
    #[Groups(['store:read', 'restaurantFront', 'store:post', 'restaurantBack'])]
    private ?bool $booking = null;

    /**
     * Store constructor.
     */
    public function __construct()
    {
        $this->setCreateAt(new \DateTime());
        $this->menuConfigurators = new ArrayCollection();
        $this->mediaManagers = new ArrayCollection();
        $this->menuTitles = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->banners = new ArrayCollection();
        $this->deliveryShops = new ArrayCollection();
        $this->deliveries = new ArrayCollection();
    }

    #[Groups(['restaurantFront'])]
    public function getCardPictures(): array
    {
        $pictures = [];
        foreach ($this->getMediaManagers()->getValues() as $picture) {
            if (
                in_array($picture->getType()->getName(), [
                    Picture::PICTURE_PROFILE->value,
                    Picture::PICTURE_COVER->value,
                ])
            ) {
                $pictures[$picture->getType()->getName()] = [
                    'id' => $picture->getId(),
                    'fileName' => $picture->getFileName(),
                ];
            }
        }

        return $pictures;
    }

    public function getStripeKeyConfigShop(): ?string
    {
        return $this->configShop->getStripeKey();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;
        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;
        return $this;
    }

    public function getActivePremium(): ?bool
    {
        return $this->activePremium;
    }

    public function setActivePremium(bool $activePremium): self
    {
        $this->activePremium = $activePremium;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getMenuConfigurators(): Collection
    {
        return $this->menuConfigurators;
    }

    public function addMenuConfigurator(MenuConfigurator $menuConfigurator): self
    {
        if (!$this->menuConfigurators->contains($menuConfigurator)) {
            $this->menuConfigurators[] = $menuConfigurator;
            $menuConfigurator->setStore($this);
        }

        return $this;
    }

    public function removeMenuConfigurator(MenuConfigurator $menuConfigurator): self
    {
        if ($this->menuConfigurators->contains($menuConfigurator)) {
            $this->menuConfigurators->removeElement($menuConfigurator);
            // set the owning side to  (unless already changed)
            if ($menuConfigurator->getStore() === $this) {
                $menuConfigurator->setStore(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?Slug
    {
        return $this->slug;
    }

    public function setSlug(?Slug $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getZipCode(): ?int
    {
        return $this->zipCode;
    }

    public function setZipCode(int $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCompanyNumber(): ?string
    {
        return $this->companyNumber;
    }

    public function setCompanyNumber(string $companyNumber): self
    {
        $this->companyNumber = $companyNumber;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getConfigShop(): ?ConfigShop
    {
        return $this->configShop;
    }

    public function setConfigShop(?ConfigShop $configShop): self
    {
        $this->configShop = $configShop;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getIsOnline(): ?bool
    {
        return $this->isOnline;
    }

    public function setIsOnline(bool $isOnline): self
    {
        $this->isOnline = $isOnline;

        return $this;
    }

    public function getMediaManagers(): Collection
    {
        return $this->mediaManagers;
    }

    public function addMediaManager(MediaManager $mediaManager): self
    {
        if (!$this->mediaManagers->contains($mediaManager)) {
            $this->mediaManagers[] = $mediaManager;
            $mediaManager->addStore($this);
        }

        return $this;
    }

    public function removeMediaManager(MediaManager $mediaManager): self
    {
        if ($this->mediaManagers->contains($mediaManager)) {
            $this->mediaManagers->removeElement($mediaManager);
            $mediaManager->removeStore($this);
        }

        return $this;
    }

    public function getBillingSubscription(): ?int
    {
        return $this->billingSubscription;
    }

    public function setBillingSubscription(int $billingSubscription): self
    {
        $this->billingSubscription = $billingSubscription;

        return $this;
    }

    public function getMenuTitles(): Collection
    {
        return $this->menuTitles;
    }

    public function getIsOpen(): ?bool
    {
        return $this->isOpen;
    }

    public function setIsOpen(bool $isOpen): self
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setStoreId($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        // set the owning side to null (unless already changed)
        if ($this->orders->removeElement($order) && $order->getStoreId() === $this) {
            $order->setStoreId(null);
        }

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getIsShopStore(): ?bool
    {
        return $this->isShopStore;
    }

    public function setIsShopStore(bool $isShopStore): self
    {
        $this->isShopStore = $isShopStore;

        return $this;
    }

    public function getDisabledShopStore(): ?bool
    {
        return $this->disabledShopStore;
    }

    public function setDisabledShopStore(bool $disabledShopStore): self
    {
        $this->disabledShopStore = $disabledShopStore;

        return $this;
    }

    public function getRating(): ?float
    {
        return $this->rating;
    }

    public function setRating(float $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection<int, Banner>
     */
    public function getBanners(): Collection
    {
        return $this->banners;
    }

    public function addBanner(Banner $banner): self
    {
        if (!$this->banners->contains($banner)) {
            $this->banners->add($banner);
            $banner->setStore($this);
        }

        return $this;
    }

    public function removeBanner(Banner $banner): self
    {
        if ($this->banners->removeElement($banner)) {
            // set the owning side to null (unless already changed)
            if ($banner->getStore() === $this) {
                $banner->setStore(null);
            }
        }

        return $this;
    }

    public function getFolderNamePicture(): ?string
    {
        return $this->folderNamePicture;
    }

    public function setFolderNamePicture(?string $folderNamePicture): self
    {
        $this->folderNamePicture = $folderNamePicture;

        return $this;
    }

    public function findMediaManagerByAssetId(string $assetId): MediaManager|bool
    {
        return $this->mediaManagers->filter(fn($m) => $m->getAssetId() === $assetId)->first();
    }

    /**
     * @return Collection<int, DeliveryShop>
     */
    public function getDeliveryShops(): Collection
    {
        return $this->deliveryShops;
    }

    public function addDeliveryShop(DeliveryShop $deliveryShop): static
    {
        if (!$this->deliveryShops->contains($deliveryShop)) {
            $this->deliveryShops->add($deliveryShop);
            $deliveryShop->setStore($this);
        }

        return $this;
    }

    public function removeDeliveryShop(DeliveryShop $deliveryShop): static
    {
        if ($this->deliveryShops->removeElement($deliveryShop)) {
            // set the owning side to null (unless already changed)
            if ($deliveryShop->getStore() === $this) {
                $deliveryShop->setStore(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Delivery>
     */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    public function addDelivery(Delivery $delivery): static
    {
        if (!$this->deliveries->contains($delivery)) {
            $this->deliveries->add($delivery);
            $delivery->setStore($this);
        }

        return $this;
    }

    public function removeDelivery(Delivery $delivery): static
    {
        if ($this->deliveries->removeElement($delivery)) {
            // set the owning side to null (unless already changed)
            if ($delivery->getStore() === $this) {
                $delivery->setStore(null);
            }
        }

        return $this;
    }

    #[Groups(['restaurantBack'])]
    public function getPictureCover(): ?MediaManager
    {
        $pictures = $this->getMediaManagers()->toArray();
        $firstPicture = null;

        foreach ($pictures as $media) {
            if ($media->getType() === Picture::PICTURE_COVER->value) {
                $firstPicture = $media;
                break;
            }
        }

        return $firstPicture;
    }

    #[Groups(['restaurantBack'])]
    public function getPictureProfile(): ?MediaManager
    {
        $pictures = $this->getMediaManagers()->toArray();
        $firstPicture = null;

        foreach ($pictures as $media) {
            if ($media->getType() === Picture::PICTURE_PROFILE->value) {
                $firstPicture = $media;
                break;
            }
        }

        return $firstPicture;
    }

    #[Groups(['restaurantBack'])]
    public function getPicture(): ?MediaManager
    {
        $pictures = array_filter(
            $this->getMediaManagers()->toArray(),
            fn($media) => $media->getType() === Picture::PICTURE_TYPE->value
        );

        return count($pictures) > 0 ?  $pictures[0] : null;
    }

    public function isActifDelivery(): ?bool
    {
        return $this->actifDelivery;
    }

    public function setActifDelivery(bool $actifDelivery): static
    {
        $this->actifDelivery = $actifDelivery;

        return $this;
    }

    public function isActifInSite(): ?bool
    {
        return $this->actifInSite;
    }

    public function setActifInSite(bool $actifInSite): static
    {
        $this->actifInSite = $actifInSite;

        return $this;
    }

    public function isActifTakeAway(): ?bool
    {
        return $this->actifTakeAway;
    }

    public function setActifTakeAway(bool $actifTakeAway): static
    {
        $this->actifTakeAway = $actifTakeAway;

        return $this;
    }

    public function isBooking(): ?bool
    {
        return $this->booking;
    }

    public function setBooking(bool $booking): static
    {
        $this->booking = $booking;

        return $this;
    }
}
