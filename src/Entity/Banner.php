<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Manager\hasTraitName;
use App\Repository\BannerRepository;
use App\State\BannerStateProcessor;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

#[ORM\Entity(repositoryClass: BannerRepository::class)]
#[ApiResource(
    operations: [
        new Post(security: "is_granted('edit_banner', object)"),
        new Get(),
        new GetCollection(security: "is_granted('ROLE_ADMIN')"),
        new Put(security: "is_granted('ROLE_BACK') and is_granted('edit_banner', object)"),
        new Patch(security: "is_granted('ROLE_BACK') and is_granted('edit_banner', object)"),
        new Delete(security: "is_granted('ROLE_BACK') and is_granted('edit_banner', object)"),
    ],
    normalizationContext: ['groups' => ['banner:read']],
    forceEager: false,
)]
#[ApiFilter(SearchFilter::class, properties: ['store.uid', 'device'])]
#[ApiFilter(DateFilter::class, properties: ['startTime', 'endTime'])]
#[ApiFilter(BooleanFilter::class, properties: ['active'])]
class Banner
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['banner:read', 'banner:item'])]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['banner:read', 'banner:item'])]
    #[Context(normalizationContext: [DateTimeNormalizer::FORMAT_KEY => 'd-m-Y'])]
    private ?\DateTimeInterface $updateAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['banner:read', 'banner:item'])]
    #[Context(normalizationContext: [DateTimeNormalizer::FORMAT_KEY => 'd-m-Y'])]
    private ?\DateTimeInterface $createdAt;


    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(['banner:read', 'banner:item'])]
    private ?MediaManager $picture = null;

    #[ORM\ManyToOne(inversedBy: 'banners')]
    #[Groups(['banner:read', 'banner:item'])]
    private ?Store $store = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['banner:read', 'banner:item'])]
    private ?\DateTimeInterface $startTime = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['banner:read', 'banner:item'])]
    private ?\DateTimeInterface $endTime = null;

    #[ORM\Column]
    #[Groups(['banner:read', 'banner:item'])]
    private ?bool $active = null;

    #[ORM\Column(length: 255)]
    #[Groups(['banner:read', 'banner:item'])]
    private ?string $device = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updateAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPicture(): ?MediaManager
    {
        return $this->picture;
    }

    public function setPicture(?MediaManager $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDevice(): ?string
    {
        return $this->device;
    }

    public function setDevice(string $device): self
    {
        $this->device = $device;

        return $this;
    }
}
