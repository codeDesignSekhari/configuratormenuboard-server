<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\DayWeekRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DayWeekRepository::class)]
#[ApiResource(
    security: "is_granted('ROLE_BACK')"
)]
class DayWeek
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $mondayOpen = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $createAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $mondayClose;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $tuesdayOpen;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $tuesdayClose;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $wednesdayOpen;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $wednesdayClose;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $thursdayOpen;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $thursdayClose;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $fridayOpen;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $fridayClose;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $saturdayOpen;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $saturdayClose;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $sundayOpen = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $sundayClose = null;

    #[ORM\ManyToOne(targetEntity: ConfigShop::class, inversedBy: 'dayWeeks')]
    private ?ConfigShop $configShop;

    public function __construct()
    {
        $this->createAt = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMondayOpen(): ?\DateTimeInterface
    {
        return $this->mondayOpen;
    }

    public function setMondayOpen(?\DateTimeInterface $mondayOpen): self
    {
        $this->mondayOpen = $mondayOpen;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(?\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getMondayClose(): ?\DateTimeInterface
    {
        return $this->mondayClose;
    }

    public function setMondayClose(?\DateTimeInterface $mondayClose): self
    {
        $this->mondayClose = $mondayClose;

        return $this;
    }

    public function getTuesdayOpen(): ?\DateTimeInterface
    {
        return $this->tuesdayOpen;
    }

    public function setTuesdayOpen(?\DateTimeInterface $tuesdayOpen): self
    {
        $this->tuesdayOpen = $tuesdayOpen;

        return $this;
    }

    public function getTuesdayClose(): ?\DateTimeInterface
    {
        return $this->tuesdayClose;
    }

    public function setTuesdayClose(?\DateTimeInterface $tuesdayClose): self
    {
        $this->tuesdayClose = $tuesdayClose;

        return $this;
    }

    public function getWednesdayOpen(): ?\DateTimeInterface
    {
        return $this->wednesdayOpen;
    }

    public function setWednesdayOpen(?\DateTimeInterface $wednesdayOpen): self
    {
        $this->wednesdayOpen = $wednesdayOpen;

        return $this;
    }

    public function getWednesdayClose(): ?\DateTimeInterface
    {
        return $this->wednesdayClose;
    }

    public function setWednesdayClose(?\DateTimeInterface $wednesdayClose): self
    {
        $this->wednesdayClose = $wednesdayClose;

        return $this;
    }

    public function getThursdayOpen(): ?\DateTimeInterface
    {
        return $this->thursdayOpen;
    }

    public function setThursdayOpen(?\DateTimeInterface $thursdayOpen): self
    {
        $this->thursdayOpen = $thursdayOpen;

        return $this;
    }

    public function getThursdayClose(): ?\DateTimeInterface
    {
        return $this->thursdayClose;
    }

    public function setThursdayClose(?\DateTimeInterface $thursdayClose): self
    {
        $this->thursdayClose = $thursdayClose;

        return $this;
    }

    public function getFridayOpen(): ?\DateTimeInterface
    {
        return $this->fridayOpen;
    }

    public function setFridayOpen(?\DateTimeInterface $fridayOpen): self
    {
        $this->fridayOpen = $fridayOpen;

        return $this;
    }

    public function getFridayClose(): ?\DateTimeInterface
    {
        return $this->fridayClose;
    }

    public function setFridayClose(?\DateTimeInterface $fridayClose): self
    {
        $this->fridayClose = $fridayClose;

        return $this;
    }

    public function getSaturdayOpen(): ?\DateTimeInterface
    {
        return $this->saturdayOpen;
    }

    public function setSaturdayOpen(?\DateTimeInterface $saturdayOpen): self
    {
        $this->saturdayOpen = $saturdayOpen;

        return $this;
    }

    public function getSaturdayClose(): ?\DateTimeInterface
    {
        return $this->saturdayClose;
    }

    public function setSaturdayClose(?\DateTimeInterface $saturdayClose): self
    {
        $this->saturdayClose = $saturdayClose;

        return $this;
    }

    public function getSundayOpen(): ?\DateTimeInterface
    {
        return $this->sundayOpen;
    }

    public function setSundayOpen(?\DateTimeInterface $sundayOpen): self
    {
        $this->sundayOpen = $sundayOpen;

        return $this;
    }

    public function getSundayClose(): ?\DateTimeInterface
    {
        return $this->sundayClose;
    }

    public function setSundayClose(?\DateTimeInterface $sundayClose): self
    {
        $this->sundayClose = $sundayClose;

        return $this;
    }

    public function getConfigShop(): ?ConfigShop
    {
        return $this->configShop;
    }

    public function setConfigShop(?ConfigShop $configShop): self
    {
        $this->configShop = $configShop;

        return $this;
    }
}
