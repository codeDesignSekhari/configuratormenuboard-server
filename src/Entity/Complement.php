<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Manager\hasTraitName;
use App\Repository\ComplementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ComplementRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(denormalizationContext: ['groups' => ['complement:post']]),
        new Put(denormalizationContext: ['groups' => ['complement:put']]),
        new Patch(),
        new Delete(),
    ],
    security: "is_granted('ROLE_BACK')"
)]
class Complement
{
    use hasTraitName;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['complementGroup:read', 'complement:read', 'menuConfigurator:read', 'restaurantFront', 'order:collection'])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['complement:post', 'menuConfigurator:read', 'complementGroup:read', 'complement:put', 'restaurantFront', 'order:collection'])]
    private ?float $price = null;

    #[ORM\Column]
    #[ApiProperty(default:"now")]
    private ?\DateTimeImmutable $createdAt;

    #[ORM\Column]
    #[ApiProperty(default:"now")]
    private ?\DateTimeImmutable $updateAt;

    #[ORM\ManyToOne(inversedBy: 'complements')]
    #[Groups(['complement:post', 'order:collection'])]
    private ?ComplementGroup $complementGroup = null;

    #[ORM\ManyToMany(targetEntity: OrderLine::class, mappedBy: 'complement')]
    private Collection $orderLines;

    public function __construct()
    {
        $this->updateAt = new \DateTimeImmutable();
        $this->createdAt = new \DateTimeImmutable();
        $this->orderLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getComplementGroup(): ?ComplementGroup
    {
        return $this->complementGroup;
    }

    public function setComplementGroup(?ComplementGroup $complementGroup): self
    {
        $this->complementGroup = $complementGroup;

        return $this;
    }

    /**
     * @return Collection<int, OrderLine>
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function addOrderLine(OrderLine $orderLine): self
    {
        if (!$this->orderLines->contains($orderLine)) {
            $this->orderLines->add($orderLine);
            $orderLine->addComplement($this);
        }

        return $this;
    }

    public function removeOrderLine(OrderLine $orderLine): self
    {
        if ($this->orderLines->removeElement($orderLine)) {
            $orderLine->removeComplement($this);
        }

        return $this;
    }
}
