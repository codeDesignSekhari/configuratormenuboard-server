<?php

namespace App\Command;

use App\Service\MenuConfiguratorService\MenuConfiguratorService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:insert-categoryMenu-from-menuConfig')]
class InsertCategoryMenuFromMenuConfiguratorCommand extends Command
{
    private readonly LoggerInterface $logger;

    private readonly MenuConfiguratorService $menuConfiguratorService;

    public function __construct(
        LoggerInterface         $logger,
        MenuConfiguratorService $menuConfiguratorService
    ) {
        parent::__construct();
        $this->logger = $logger;
        $this->menuConfiguratorService = $menuConfiguratorService;
    }

    protected function configure(): void
    {
        $this->setDescription('Insert Category menu..')
            ->addArgument('id_store', InputArgument::REQUIRED, 'required id store');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id_store');

        $output->writeln('start insert category menu');
        $this->logger->info('start insert category menu.');

        $response = $this->menuConfiguratorService->insertCategoryFromMenuConfig($id);
        $output->writeln('Done - id:'
            . $id . '  Number inserted ' . $response);

        return Command::SUCCESS;
    }
}
