<?php

namespace App\Command;

use App\Entity\MediaManager;
use App\Service\MediaManagerService\MediaManagerService;
use App\Service\helperService\helperService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;

#[AsCommand('app:remove-images')]
class RemoveImageCommand extends Command
{
    public function __construct(
        private readonly LoggerInterface     $logger,
        private readonly MediaManagerService $mediaManagerService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Remove images.')
            ->addArgument('id_store', InputArgument::REQUIRED, 'required id store')
            ->setHelp('This command allows you to remove images orphan...');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info('run remove images');
        $output->writeln([
            'Remove images orphan',
        ]);

        $id = $input->getArgument('id_store');
        $output->writeln('start remove images orphan');
        $this->mediaManagerService->removeAllMediaManagerByStore($id);

        $output->writeln('ID selected' . $id);

        return Command::SUCCESS;
    }
}
