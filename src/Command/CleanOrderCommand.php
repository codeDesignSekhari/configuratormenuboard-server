<?php

namespace App\Command;

use App\Manager\OrderManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;

#[AsCommand('app:clean-order')]
class CleanOrderCommand extends Command
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly OrderManager    $orderManager,
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Clean orders with a price of 0');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $response = $this->orderManager->cleanOrderWithPriceZero();
        $this->renderTable($output, ['ID', 'Numéro de commande'], $response['table']);
        $output->writeln($response['outputMessage']);

        return Command::SUCCESS;
    }

    private function renderTable(
        OutputInterface $output,
        array           $headers,
        array           $rows
    ): void
    {
        $table = new Table($output);
        $table->setHeaders($headers)
            ->setRows($rows);
        $table->render();
    }
}
