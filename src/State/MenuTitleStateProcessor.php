<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\MenuConfigurator;
use App\Models\CloudinaryModel;
use App\Service\MediaManagerService\MediaManagerService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use function PHPUnit\Framework\throwException;

class MenuTitleStateProcessor implements ProcessorInterface
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected MediaManagerService    $mediaManagerService,
        protected CloudinaryModel        $cloudinaryModel,
    )
    {

    }

    public function process(
        mixed     $data,
        Operation $operation,
        array     $uriVariables = [],
        array     $context = []
    ): void
    {

        $menuConfigurators = $this->em
            ->getRepository(MenuConfigurator::class)
            ->findBy(['menuTitle' => $data]);

        foreach ($menuConfigurators as $menuConfigurator) {
            if ($mediaManager = $menuConfigurator->getMediaManager()) {
                if (count($mediaManager->getOrderLines()->toArray()) > 0) {
                    throw new Exception(
                        "Impossible de supprimer cette catégorie, car elle est liée à une/plusieurs commande(s)",
                        422
                    );
                }

                $this->mediaManagerService->delete($mediaManager);
            }

            $this->em->remove($menuConfigurator);
            $this->em->remove($data);
        }

        $this->em->flush();
    }
}
