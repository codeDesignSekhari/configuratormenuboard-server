<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class MenuTitleNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('MenuTitle was not found', 404);
    }
}
