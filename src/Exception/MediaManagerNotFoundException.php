<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class MediaManagerNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Media was not found', 404);
    }
}
