<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class ParameterMissingException extends \Exception
{
    public function __construct($parameter)
    {
        parent::__construct("Missing required parameter '$parameter' or the parameter is empty", Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
