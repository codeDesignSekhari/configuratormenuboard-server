<?php

namespace App\Exception;

class ItemMenuNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('ItemMenu was not found', 404);
    }
}
