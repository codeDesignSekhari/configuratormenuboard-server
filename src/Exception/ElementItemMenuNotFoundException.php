<?php

namespace App\Exception;

class ElementItemMenuNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('ElementItemMenu was not found', 404);
    }
}
