<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class CategoryNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Category was not found', 404);
    }
}
