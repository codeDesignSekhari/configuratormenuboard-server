<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class StoreNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Store was not found', 404);
    }
}
