<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class SlugNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Slug was not found', 404);
    }
}
