<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class CartNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Cart was not found', 404);
    }
}
