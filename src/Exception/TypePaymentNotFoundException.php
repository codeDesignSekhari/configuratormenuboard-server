<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class TypePaymentNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('TypePayment was not found', 404);
    }
}
