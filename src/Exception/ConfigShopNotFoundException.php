<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class ConfigShopNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('ConfigShop was not found', 404);
    }
}
