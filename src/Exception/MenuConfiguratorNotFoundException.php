<?php

namespace App\Exception;

class MenuConfiguratorNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('MenuConfigurator was not found', 404);
    }
}
