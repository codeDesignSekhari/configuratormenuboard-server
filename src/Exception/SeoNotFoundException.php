<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class SeoNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Seo was not found', 404);
    }
}
