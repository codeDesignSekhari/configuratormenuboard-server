<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class OrderNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Order was not found', 404);
    }
}
