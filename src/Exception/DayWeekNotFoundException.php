<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class DayWeekNotFoundException extends \Exception
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('DayWeek was not found', 404);
    }
}
